<?php
namespace Rvs\General\Block; 

class CustomerInfo extends \Magento\Framework\View\Element\Template
{
    protected $_customerSession;
         
	private $wishlist;
	
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\SessionFactory $customerSession,
		 \Magento\Wishlist\Model\Wishlist $wishlist,
        array $data = []
    ) {
		$this->wishlist = $wishlist;
        $this->_customerSession = $customerSession->create();
        parent::__construct($context, $data);
    }
     
    public function getLoggedinCustomerId() {
        if ($this->_customerSession->isLoggedIn()) {
            return $this->_customerSession->getId();
        }
        return false;
    }
 
    public function getCustomerData() {
        if ($this->_customerSession->isLoggedIn()) {
            return $this->_customerSession->getCustomerData();
        }
        return false;
    }
	
	public function getCustomerWishlist($customer_id){
		$wishlist_collection = $this->wishlist->loadByCustomerId($customer_id, true)->getItemCollection();
		return $wishlist_collection;
	
	}
}