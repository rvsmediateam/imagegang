<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Shopby
 */


namespace Amasty\Shopby\Plugin\CatalogSearch\Model\Search\FilterMapper;

use Magento\CatalogSearch\Model\Search\FilterMapper\FilterContext as MagentoFilterContext;

/**
 * Class FilterContext
 * @package Amasty\Shopby\Plugin\CatalogSearch\Model\Search\FilterMapper
 */
class FilterContext
{
    /**
     * @var CustomExclusionStrategy
     */
    private $customExclusionStrategy;

    /**
     * FilterContext constructor.
     * @param CustomExclusionStrategy $customExclusionStrategy
     */
    public function __construct(
        CustomExclusionStrategy $customExclusionStrategy
    ) {
        $this->customExclusionStrategy = $customExclusionStrategy;
    }

    /**
     * Method for apply custom filters(on_sale, is_new and rating summary)
     *
     * @param MagentoFilterContext $subject
     * @param callable $proceed
     * @param \Magento\Framework\Search\Request\FilterInterface $filter
     * @param \Magento\Framework\DB\Select $select
     * @return mixed
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundApply(
        MagentoFilterContext $subject,
        callable $proceed,
        \Magento\Framework\Search\Request\FilterInterface $filter,
        \Magento\Framework\DB\Select $select
    ) {
        return $this->customExclusionStrategy->apply($filter, $select) ?: $proceed($filter, $select);
    }
}
