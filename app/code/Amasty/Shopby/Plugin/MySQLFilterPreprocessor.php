<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Shopby
 */


namespace Amasty\Shopby\Plugin;

use Amasty\Shopby\Helper\Category;

class MySQLFilterPreprocessor
{
    const INVALID_FILTERS = ['on_sale', 'is_new'];

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    private $connection;

    /**
     * MySQLFilterPreprocessor constructor.
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     */
    public function __construct(\Magento\Framework\App\ResourceConnection $resourceConnection)
    {
        $this->connection = $resourceConnection->getConnection();
    }

    /**
     * @param \Magento\CatalogSearch\Model\Adapter\Mysql\Filter\Preprocessor $subject
     * @param \Closure $closure
     * @param \Magento\Framework\Search\Request\FilterInterface $filter
     * @param bool $isNegation
     * @param string $query
     * @return string
     */
    public function aroundProcess(
        \Magento\CatalogSearch\Model\Adapter\Mysql\Filter\Preprocessor $subject,
        \Closure $closure,
        \Magento\Framework\Search\Request\FilterInterface $filter,
        $isNegation,
        $query
    ) {

        if (in_array($filter->getField(), self::INVALID_FILTERS)) {
            return '';
        }elseif ($filter->getField() === Category::STORE_CODE) {
            return $this->processQuery($query, 'store_id', 'store_id');
        } elseif ($filter->getField() === Category::ATTRIBUTE_CODE && is_array($filter->getValue())) {
            return $this->processQuery($query, 'category_ids', 'category_id');
        }

        return $closure($filter, $isNegation, $query);
    }

    /**
     * @param string $query
     * @param string $column1
     * @param string $column2
     * @return string
     */
    private function processQuery($query, $column1, $column2)
    {
        return str_replace(
            $this->connection->quoteIdentifier($column1),
            $this->connection->quoteIdentifier('category_ids_index.' . $column2),
            $query
        );
    }
}
