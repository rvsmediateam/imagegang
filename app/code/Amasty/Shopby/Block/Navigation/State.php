<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Shopby
 */


namespace Amasty\Shopby\Block\Navigation;

use Amasty\Shopby\Model\Layer\Filter\Item;
use Amasty\Shopby\Helper\Data as ShopbyHelper;

class State extends \Magento\LayeredNavigation\Block\Navigation\State
{
    /**
     * @var string
     */
    protected $_template = 'layer/state.phtml';

    /**
     * @var \Amasty\Shopby\Helper\FilterSetting
     */
    protected $filterSettingHelper;

    protected $managerInterface;

    protected $priceCurrency;

    /**
     * @var ShopbyHelper
     */
    protected $helper;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        \Amasty\Shopby\Helper\FilterSetting $filterSettingHelper,
        ShopbyHelper $helper,
        array $data = []
    ) {
        $this->filterSettingHelper = $filterSettingHelper;
        $this->managerInterface = $context->getStoreManager();
        $this->helper = $helper;
        parent::__construct($context, $layerResolver, $data);
    }

    /**
     * @param \Magento\Catalog\Model\Layer\Filter\FilterInterface $filter
     * @return \Amasty\ShopbyBase\Api\Data\FilterSettingInterface
     */
    public function getFilterSetting(\Magento\Catalog\Model\Layer\Filter\FilterInterface $filter)
    {
        return $this->filterSettingHelper->getSettingByLayerFilter($filter);
    }

    /**
     * @param Item $filter
     * @param bool $showLabels
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getSwatchHtml(\Amasty\Shopby\Model\Layer\Filter\Item $filter, $showLabels = false)
    {
        return $this->getLayout()->createBlock(\Amasty\Shopby\Block\Navigation\State\Swatch::class)
            ->setFilter($filter)
            ->showLabels($showLabels)
            ->toHtml();
    }

    /**
     * @return string
     */
    public function collectFilters()
    {
        return $this->helper->getSubmitFiltersMode() ===
        \Amasty\Shopby\Model\Source\SubmitMode::BY_BUTTON_CLICK ? '1' : '0';
    }

    /**
     * @param Item $filter
     * @return string
     */
    public function viewLabel($filter)
    {
        $filterSetting = $this->getFilterSetting($filter->getFilter());

        switch ($filterSetting->getDisplayMode()) {
            case \Amasty\Shopby\Model\Source\DisplayMode::MODE_IMAGES:
                $value =  $this->getSwatchHtml($filter);
                break;
            case \Amasty\Shopby\Model\Source\DisplayMode::MODE_IMAGES_LABELS:
                $value =  $this->getSwatchHtml($filter, true);
                break;
            default:
                $value = $this->viewExtendedLabel($filter);
                break;
        }

        return $value;
    }

    /**
     * @param Item $filter
     * @return string
     */
    protected function viewExtendedLabel($filter)
    {
        if ($filter->getFilter()->getRequestVar() == \Amasty\Shopby\Model\Source\DisplayMode::ATTRUBUTE_PRICE) {
            $currencyRate = (float)$filter->getFilter()->getCurrencyRate();

            if ($currencyRate !== 1) {
                $value = $this->genetateValueLabel($filter, $currencyRate);
            } else {
                $value = $filter->getLabel();
            }
        } else {
            $value = $this->stripTags($filter->getLabel());
        }

        return $value;
    }

    private function genetateValueLabel($filter, $currencyRate)
    {
        $arguments = $filter->getLabel()->getArguments();
        $stepSlider = $this->filterSettingHelper->getSettingByLayerFilter($filter->getFilter())->getSliderStep();

        $arguments[0] = preg_replace("/[^,.0-9]/", '', $arguments[0]);
        $arguments[1] = preg_replace("/[^,.0-9]/", '', $arguments[1]);

        if (strpos($arguments[0], '.') !== false) {
            $arguments[0] = preg_replace("/[,]/", '', $arguments[0]);
        }
        if (strpos($arguments[1], '.') !== false) {
            $arguments[1] = preg_replace("/[,]/", '', $arguments[1]);
        }

        $arguments[0] = preg_replace("/[']/", '', $arguments[0]);
        $arguments[1] = preg_replace("/[']/", '', $arguments[1]);

        $value = __(
            '%1 - %2',
            $this->generateSpanPrice((float)$arguments[0], $currencyRate, $stepSlider),
            $this->generateSpanPrice((float)$arguments[1], $currencyRate, $stepSlider)
        );

        return $value;
    }

    private function generateSpanPrice($value, $currencyRate, $stepSlider)
    {
        return $stepSlider == 1
            ? '<span class="price">'. round($value * $currencyRate) .'</span>'
            : '<span class="price">'. round($value * $currencyRate, 2) .'</span>';
    }
}
