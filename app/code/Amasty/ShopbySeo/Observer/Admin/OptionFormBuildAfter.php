<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_ShopbySeo
 */


namespace Amasty\ShopbySeo\Observer\Admin;

use Amasty\ShopbyBase\Api\Data\OptionSettingInterface;
use Amasty\ShopbyBase\Model\FilterSettingFactory;
use Magento\Catalog\Model\Category\Attribute\Source\Page;
use Magento\Framework\Data\Form;
use Magento\Framework\Event\ObserverInterface;

class OptionFormBuildAfter implements ObserverInterface
{
    /**
     * @var Page
     */
    protected $page;

    /**
     * @var  FilterSettingFactory
     */
    protected $filterSettingFactory;

    /**
     * @var  OptionSettingInterface
     */
    protected $model;

    public function __construct(Page $page, FilterSettingFactory $filterSettingFactory)
    {
        $this->page = $page;
        $this->filterSettingFactory = $filterSettingFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var Form $form */
        $form = $observer->getData('form');

        /** @var OptionSettingInterface $setting */
        $this->model = $observer->getData('setting');

        if ($this->isSeoURLEnabled()) {
            $form->getElement('url_alias')->setData(
                'note',
                __('Enable SEO URL for the attribute in order to use URL Aliases')
            );
        }
    }

    protected function isSeoURLEnabled()
    {
        $filterSetting = $this->filterSettingFactory->create();
        $filterSetting->load($this->model->getFilterCode(), 'filter_code');
        if (!$filterSetting->getId()) {
            return false;
        }

        return $filterSetting->isSeoSignificant();
    }
}
