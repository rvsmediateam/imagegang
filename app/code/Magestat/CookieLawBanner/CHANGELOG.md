# Changelog
All notable changes to this project will be documented in this file.

## [1.0.0] - 2018-07-26
### Added
- CMS module settings (Enable/Disable)
- Initial feature