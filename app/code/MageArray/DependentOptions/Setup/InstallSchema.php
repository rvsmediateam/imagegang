<?php
/**
 * Copyright � 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace MageArray\DependentOptions\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();
		
		/**
         * Update table 'catalog_product_option_type_value'
         */
		$eavTable = $setup->getTable('catalog_product_option_type_value');
		$columns = [
			'dependent_options' => [
				'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
				'nullable' => false,
				'comment' => 'Dependent Options',
			],
			'row_id' => [
				'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
				'nullable' => true,
				'comment' => 'Row ID',
			]
		];

		$connection = $setup->getConnection();
		foreach ($columns as $name => $definition) {
			$connection->addColumn($eavTable, $name, $definition);
		}
		
		/**
         * Update table 'catalog_product_option'
         */
		$eavTable = $setup->getTable('catalog_product_option');
		$columns = [
			'row_id' => [
				'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
				'nullable' => true,
				'comment' => 'Row ID',
			]
		];

		$connection = $setup->getConnection();
		foreach ($columns as $name => $definition) {
			$connection->addColumn($eavTable, $name, $definition);
		}
	}
}
