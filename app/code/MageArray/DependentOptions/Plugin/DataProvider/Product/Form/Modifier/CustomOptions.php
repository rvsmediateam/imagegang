<?php
/**
 * Copyright � 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace MageArray\DependentOptions\Plugin\DataProvider\Product\Form\Modifier;

use Magento\Ui\Component\Container;
use Magento\Ui\Component\Form\Element\Input;
use Magento\Ui\Component\Form\Element\Hidden;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Ui\Component\Form\Element\ActionDelete;

/**
 * Class CustomOptions
 * @package MageArray\DependentOptions\Plugin\DataProvider\Product\Form\Modifier
 */
class CustomOptions
{
    /**
     *
     */
    const FIELD_DEPENDENT_OPTIONS_NAME = 'dependent_options';
    /**
     *
     */
    const FIELD_IS_DELETE = 'is_delete';
    /**
     *
     */
    const FIELD_ROW_ID = 'row_id';
    /**
     *
     */
    const FIELD_DEPENDENT_ID_NAME = 'dependent_id';

    /**
     * CustomOptions constructor.
     * @param \MageArray\DependentOptions\Helper\Data $helper
     */
    public function __construct(
        \MageArray\DependentOptions\Helper\Data $helper
    ) {
        $this->helper = $helper;
    }

    /**
     * @param $subject
     * @param $meta
     * @return mixed
     */
    public function aftermodifyMeta($subject, $meta)
    {
        if (!$this->helper->isEnabled()) {
            return $meta;
        }

        $containerTypeStatic = $meta['custom_options']['children']['options']['children']['record']['children']
        ['container_option']['children']['container_type_static']['children'];
        $containerTypeStatic = array_merge(
            [
                static::FIELD_ROW_ID => $this->getOptionIdFieldConfig(0)
            ] + $containerTypeStatic);
        $meta['custom_options']['children']['options']['children']['record']['children']['container_option']
        ['children']['container_type_static']['children'] = $containerTypeStatic;

        $record = $meta['custom_options']['children']['options']['children']['record']['children']['container_option']
        ['children']['values']['children']['record']['children'];
        unset($record[static::FIELD_IS_DELETE]);
        $preSortOrder = count($record) * 10;

        $record[static::FIELD_DEPENDENT_OPTIONS_NAME] = $this->getDependentOptionsFieldConfig($preSortOrder + 20);
        $record[static::FIELD_IS_DELETE] = $this->getIsDeleteFieldConfig($preSortOrder + 30);

        $record = array_merge([static::FIELD_ROW_ID => $this->getOptionIdFieldConfig(0)] + $record);

        $meta['custom_options']['children']['options']['children']['record']['children']['container_option']
        ['children']['values']['children']['record']['children'] = $record;
        return $meta;
    }

    /**
     * @param $sortOrder
     * @return array
     */
    protected function getOptionIdFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('ID'),
                        'fit' => true,
                        'additionalClasses' => 'optionids',
                        'component' => 'MageArray_DependentOptions/js/rowids',
                        'componentType' => Field::NAME,
                        'formElement' => Input::NAME,
                        'dataScope' => static::FIELD_ROW_ID,
                        'dataType' => Text::NAME,
                        'sortOrder' => $sortOrder,
                        'validation' => [
                            'required-entry' => false
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $sortOrder
     * @return array
     */
    protected function getDependentOptionsFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Dependent Options'),
                        'fit' => true,
                        'additionalClasses' => 'dependentoptionfield',
                        'component' => 'MageArray_DependentOptions/js/dependentfield',
                        'componentType' => Field::NAME,
                        'formElement' => Input::NAME,
                        'dataScope' => static::FIELD_DEPENDENT_OPTIONS_NAME,
                        'dataType' => Text::NAME,
                        'sortOrder' => $sortOrder,
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $sortOrder
     * @return array
     */
    protected function getIsDeleteFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => ActionDelete::NAME,
                        'fit' => true,
                        'sortOrder' => $sortOrder
                    ],
                ],
            ],
        ];
    }
}
