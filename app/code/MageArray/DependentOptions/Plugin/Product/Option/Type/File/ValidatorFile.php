<?php
namespace MageArray\DependentOptions\Plugin\Product\Option\Type\File;

/**
 * Class ValidatorFile
 * @package MageArray\DependentOptions\Plugin\Product\Option\Type\File
 */
class ValidatorFile
{
	/**
     * CustomOptions constructor.
     * @param \MageArray\DependentOptions\Helper\Data $helper
     */
    public function __construct(
        \MageArray\DependentOptions\Helper\Data $helper
    ) {
        $this->helper = $helper;
    }
	
    /**
     * @param $subject
     * @param $processingParams
     * @param $option
     * @return mixed
     */
    public function beforevalidate($subject, $processingParams, $option)
    {
		if ($this->helper->isEnabled()) {
            $option->setIsRequire(0);
        }
        return $option;
    }
}
