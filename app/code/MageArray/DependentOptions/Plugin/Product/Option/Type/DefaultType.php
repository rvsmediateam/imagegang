<?php
namespace MageArray\DependentOptions\Plugin\Product\Option\Type;

/**
 * Class DefaultType
 * @package MageArray\DependentOptions\Plugin\Product\Option\Type
 */
class DefaultType
{
	/**
     * CustomOptions constructor.
     * @param \MageArray\DependentOptions\Helper\Data $helper
     */
    public function __construct(
        \MageArray\DependentOptions\Helper\Data $helper
    ) {
        $this->helper = $helper;
    }
	
    /**
     * @return bool
     */
    public function aftergetSkipCheckRequiredOption($subject, $params)
    {
		if ($this->helper->isEnabled()) {
            return true;
        }
        return $params;
    }
}
