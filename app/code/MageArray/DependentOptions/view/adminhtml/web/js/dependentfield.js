define(['jquery',
        'underscore',
        'mageUtils',
        'uiLayout',
        'uiElement',
        'Magento_Ui/js/lib/validation/validator'],
    function ($, _, utils, layout, Element, validator) {

        'use strict';
        return Element.extend({
            defaults: {
                visible: true,
                preview: '',
                focused: false,
                required: false,
                disabled: false,
                valueChangedByUser: false,
                elementTmpl: 'MageArray_DependentOptions/form/element/dependentfield',
                tooltipTpl: 'ui/form/element/helper/tooltip',
                fallbackResetTpl: 'ui/form/element/helper/fallback-reset',
                'input_type': 'input',
                placeholder: '',
                description: '',
                labelVisible: true,
                label: '',
                error: '',
                warn: '',
                notice: '',
                customScope: '',
                default: '',
                isDifferedFromDefault: false,
                showFallbackReset: false,
                additionalClasses: {},
                isUseDefault: '',
                valueUpdate: false,
                switcherConfig: {
                    component: 'Magento_Ui/js/form/switcher',
                    name: '${ $.name }_switcher',
                    target: '${ $.name }',
                    property: 'value'
                },
                listens: {
                    visible: 'setPreview',
                    value: 'setDifferedFromDefault',
                    '${ $.provider }:data.reset': 'reset',
                    '${ $.provider }:data.overload': 'overload',
                    '${ $.provider }:${ $.customScope ? $.customScope + "." : ""}data.validate': 'validate',
                    'isUseDefault': 'toggleUseDefault'
                },
                links: {
                    value: '${ $.provider }:${ $.dataScope }'
                }
            },
            initialize: function () {
                _.bindAll(this, 'reset');
                this._super().setInitialValue()._setClasses().initSwitcher();

                return this;
            },
            initObservable: function () {
                var rules = this.validation = this.validation || {};
                this._super();
                this.observe('error disabled focused preview visible value warn isDifferedFromDefault').observe('isUseDefault').observe({
                    'required': !!rules['required-entry']
                });
                return this;
            },
            initConfig: function () {
                var uid = utils.uniqueid(),
                    optid, storeurl, name, valueUpdate, scope;
                this._super();
                scope = this.dataScope;
                name = scope.split('.').slice(1);
                valueUpdate = this.valueUpdate;

                _.extend(this, {
                    uid: uid,
                    optid: uid,
                    noticeId: 'notice-' + uid,
                    inputName: utils.serializeName(name.join('.')),
                    valueUpdate: valueUpdate
                });
                return this;
            },
            getOptionList: function (elementParent, optionParent, elmt) {
                var record_id = elementParent.data._latestValue.record_id;
                var optionArr = optionParent.relatedData;
                this.reloadOptions(record_id, optionArr, elmt);
            },
            reloadOptions: function (record_id, optionArr, elmt) {

                var dependentdiv = $('#' + elmt.id).closest('.dependentdiv');
                var input = dependentdiv.find('input');
                var optionsbox = dependentdiv.find('.optionsbox'); // multiselect parent div
                var multiselect = optionsbox.find('ul.optionselect');

                var oldValue = input.val();
                var oldValueArr = oldValue.split(',').map(Number);

                var types = ["drop_down", "radio", "checkbox", "multiple"];
                var select = '';

                for (var i = 0; i < optionArr.length; i++) {
                    var selectBox = optionArr[i];
                    if (selectBox.record_id != record_id) {
                        if ($.inArray(selectBox.type, types) !== -1) {
                            select += '<li><ul class="ma-group">';
                            select += '<li class="label"><hr /> <b>' + selectBox.title + '</b></li>';
                            var option = selectBox.values;

                            for (var j = 0; j < option.length; j++) {


                                var setId1 = parseInt(option[j].row_id);
                                if (!setId1) {
                                    setId1 = parseInt($('[name="product[options][' + selectBox.record_id + '][values][' + j + '][row_id]"]').val());
                                }

                                var selected = '';
                                if ($.inArray(setId1, oldValueArr) !== -1) {
                                    selected = 'checked';
                                } else {
                                    selected = '';
                                }

                                var moreCls = '';
                                var moreBtn = '';
                                if (j >= 3) {
                                    moreCls = 'class="viewmore"';
                                    moreBtn = '<span class="viewbtn">View more</span>';
                                }

                                select += '<li ' + moreCls + '> -- <input type="checkbox" id="ma-' + setId1 + '" value="' + setId1 + '" ' + selected + ' /> <label for="ma-' + setId1 + '">' + option[j].title + '</label></li>';
                            }
                            select += '</ul>' + moreBtn + '</li>';
                        } else {

                            var setId = parseInt(selectBox.row_id);
                            if (!setId) {
                                setId = parseInt($('[name="product[options][' + selectBox.record_id + '][row_id]"]').val());
                            }

                            var selected = '';
                            if ($.inArray(setId, oldValueArr) !== -1) {
                                selected = 'checked';
                            } else {
                                selected = '';
                            }
                            select += '<li><ul class="ma-group">';
                            select += '<li class="field" ><hr /> - <input type="checkbox" id="ma-' + setId + '" value="' + setId + '" ' + selected + ' /> <label for="ma-' + setId + '"><b>' + selectBox.title + '</b></label></li>';
                            select += '</ul></li>';
                        }
                    }
                }


                $('.optionsbox').fadeOut(); // multiselect parent div's


                multiselect.html(select);

                optionsbox.fadeIn(); // multiselect parent div

                dependentdiv.find('.okselect').click(function () {
                    var vles = [];
                    multiselect.find(':checkbox').each(function () {
                        if ($(this).is(':checked')) {
                            vles.push($(this).val());
                        }
                    });
                    var inptvle = vles.join(',');
                    input.val(inptvle);
                    $(this).parent().fadeOut(function () {
                        dependentdiv.find('.optionselect > li').remove();
                    });
                });

                dependentdiv.find('.cancelselect').click(function () {
                    input.val(oldValue);
                    $(this).parent().fadeOut(function () {
                        dependentdiv.find('.optionselect > li').remove();
                    });
                });

                dependentdiv.find('.viewbtn').click(function () {
                    $(this).hide();
                    $(this).parent().find('.viewmore').toggle();
                });

                dependentdiv.find('.ma-group .viewmore').hide();

            },
            initSwitcher: function () {
                if (this.switcherConfig.enabled) {
                    layout([this.switcherConfig]);
                }
                return this;
            },
            setInitialValue: function () {
                this.initialValue = this.getInitialValue();
                if (this.value.peek() !== this.initialValue) {
                    this.value(this.initialValue);
                }
                this.on('value', this.onUpdate.bind(this));
                this.isUseDefault(this.disabled());
                return this;
            },
            _setClasses: function () {
                var additional = this.additionalClasses,
                    classes;
                if (_.isString(additional) && additional.trim().length) {
                    additional = this.additionalClasses.trim().split(' ');
                    classes = this.additionalClasses = {};
                    additional.forEach(function (name) {
                        classes[name] = true;
                    }, this);
                }
                _.extend(this.additionalClasses, {
                    _required: this.required,
                    _error: this.error,
                    _warn: this.warn,
                    _disabled: this.disabled
                });
                return this;
            },
            getInitialValue: function () {
                var values = [this.value(), this.default],
                    value;
                values.some(function (v) {
                    if (v !== null && v !== undefined) {
                        value = v;
                        return true;
                    }
                    return false;
                });
                return this.normalizeData(value);
            },
            setVisible: function (isVisible) {
                this.visible(isVisible);
                return this;
            },
            show: function () {
                this.visible(true);
                return this;
            },
            hide: function () {
                this.visible(false);
                return this;
            },
            disable: function () {
                this.disabled(true);
                return this;
            },
            enable: function () {
                this.disabled(false);
                return this;
            },
            setValidation: function (rule, options) {
                var rules = utils.copy(this.validation),
                    changed;
                if (_.isObject(rule)) {
                    _.extend(this.validation, rule);
                } else {
                    this.validation[rule] = options;
                }
                changed = utils.compare(rules, this.validation).equal;
                if (changed) {
                    this.required(!!rules['required-entry']);
                    this.validate();
                }
                return this;
            },
            getPreview: function () {
                return this.value();
            },
            hasAddons: function () {
                return this.addbefore || this.addafter;
            },
            hasService: function () {
                return this.service && this.service.template;
            },
            hasChanged: function () {
                var notEqual = this.value() !== this.initialValue;
                return !this.visible() ? false : notEqual;
            },
            hasData: function () {
                return !utils.isEmpty(this.value());
            },
            reset: function () {
                this.value(this.initialValue);
                this.error(false);
                return this;
            },
            overload: function () {
                this.setInitialValue();
                this.bubble('update', this.hasChanged());
            },
            clear: function () {
                this.value('');
                return this;
            },
            normalizeData: function (value) {
                return utils.isEmpty(value) ? '' : value;
            },
            validate: function () {
                var value = this.value(),
                    result = validator(this.validation, value, this.validationParams),
                    message = !this.disabled() && this.visible() ? result.message : '',
                    isValid = this.disabled() || !this.visible() || result.passed;
                this.error(message);
                this.bubble('error', message);
                if (!isValid) {
                    this.source.set('params.invalid', true);
                }
                return {
                    valid: isValid,
                    target: this
                };
            },
            onUpdate: function () {
                this.bubble('update', this.hasChanged());
                this.validate();
            },
            restoreToDefault: function () {
                this.value(this.default);
            },
            setDifferedFromDefault: function () {
                var value = typeof this.value() != 'undefined' && this.value() !== null ? this.value() : '',
                    defaultValue = typeof this.default != 'undefined' && this.default !== null ? this.default : '';
                this.isDifferedFromDefault(value !== defaultValue);
            },
            toggleUseDefault: function (state) {
                this.disabled(state);
            },
            userChanges: function () {
                this.valueChangedByUser = true;
            }
        });
    });