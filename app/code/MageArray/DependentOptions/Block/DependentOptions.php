<?php
namespace MageArray\DependentOptions\Block;

/**
 * Class DependentOptions
 * @package MageArray\DependentOptions\Block
 */
class DependentOptions extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productLoader;
    /**
     * @var
     */
    protected $scopeConfig;

    /**
     * DependentOptions constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Catalog\Model\ProductFactory $productLoader
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\ProductFactory $productLoader,
        \Magento\Framework\App\Request\Http $request,
        array $data = []
    ) {
        $this->storeManager = $context->getStoreManager();
        $this->registry = $registry;
        $this->productLoader = $productLoader;
        $this->request = $request;
        $this->scopeConfig = $context->getScopeConfig();
        parent::__construct($context, $data);
    }

    /**
     * @return mixed
     */
    public function getConfigureMode()
    {
        return $this->getRequest()->getActionName();
    }

    /**
     * @return mixed
     */
    public function getIndependent()
    {
        return $this->scopeConfig->getValue('dependentoptions/general/independent');
    }

    /**
     * @return mixed
     */
    public function getProductOption()
    {
        $product = $this->getProduct();
        $valueArr = [];
        $optionArr = [];
        $types = ['drop_down', 'radio', 'checkbox', 'multiple'];
        foreach ($product->getOptions() as $o) {
            if (in_array($o->getType(), $types)) {
                $opt = [];
                foreach ($o->getValues() as $value) {
                    $opt[$value->getOptionTypeId()][$value->getRowId()] = $value->getDependentOptions();
                }
                $valueArr[$o->getId()] = $opt;
            }

            if (!in_array($o->getType(), $types)) {
                $optionArr[$o->getId()] = $o->getRowId();
            }
        }

        $main = [
            'value_id' => $valueArr,
            'option_id' => $optionArr,
            'configureMode' => $this->getConfigureMode(),
            'independent' => $this->getIndependent()
        ];

        return json_encode($main, true);
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        $product = $this->registry->registry('current_product');
        return $this->productLoader->create()->load($product->getId());
    }

    /**
     * @return string
     */
    public function getMediaUrl()
    {
        return $this->storeManager
                ->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product';
    }
}
