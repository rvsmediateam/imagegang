<?php
namespace MageArray\DependentOptions\Observer;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class GetOptions
 * @package MageArray\DependentOptions\Observer
 */
class GetOptions implements ObserverInterface
{
	/**
     * CustomOptions constructor.
     * @param \MageArray\DependentOptions\Helper\Data $helper
     */
    public function __construct(
        \MageArray\DependentOptions\Helper\Data $helper
    ) {
        $this->helper = $helper;
    }
	
    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
		if ($this->helper->isEnabled()) {
            $productCollection = $observer->getEvent()->getCollection();
			foreach ($productCollection as $product) {				
				/*foreach ($product->getOptions() as $option) {
					$option->setIsRequire(0);
				}*/
				$product->setSkipCheckRequiredOption(true);
			}
        }
    }
}
