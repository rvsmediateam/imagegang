<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace MageArray\CustomOptions\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {

            /* ma_custom_template */
            $template = $installer->getConnection()
                ->newTable($installer->getTable('ma_custom_template'))
                ->addColumn(
                    'template_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'Template id'
                )
                ->addColumn(
                    'title',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Title'
                )
                ->addColumn(
                    'can_option_delete_from_product',
                    Table::TYPE_SMALLINT,
                    null,
                    ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                    'Product option delete from product'
                )
                ->addColumn(
                    'is_active',
                    Table::TYPE_SMALLINT,
                    null,
                    ['default' => '1'],
                    'Active Template'
                )
                ->setComment(
                    'Custom option template master'
                );
            $installer->getConnection()->createTable($template);

            /* ma_custom_template_product */
            $templateProduct = $installer->getConnection()
                ->newTable($installer->getTable('ma_custom_template_product'))
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'ID'
                )
                ->addColumn(
                    'template_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'unsigned' => true,
                        'nullable' => false
                    ],
                    'Template ID'
                )
                ->addColumn(
                    'product_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'unsigned' => true,
                        'nullable' => false
                    ],
                    'Product ID'
                )
                ->addForeignKey(
                    $installer->getFkName(
                        'ma_custom_template',
                        'template_id',
                        'id',
                        'id'
                    ),
                    'template_id',
                    $installer->getTable('ma_custom_template'),
                    'template_id',
                    Table::ACTION_CASCADE,
                    Table::ACTION_CASCADE
                )
                ->addForeignKey(
                    $installer->getFkName(
                        'ma_custom_template',
                        'product_id',
                        'catalog_product_entity',
                        'entity_id'
                    ),
                    'product_id',
                    $installer->getTable('catalog_product_entity'),
                    'entity_id',
                    Table::ACTION_CASCADE,
                    Table::ACTION_CASCADE
                )
                ->setComment('Template products relation');
            $installer->getConnection()->createTable($templateProduct);

            /* ma_custom_template_option */
            $templateOption = $installer->getConnection()
                ->newTable($installer->getTable('ma_custom_template_option'))
                ->addColumn(
                    'option_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'Option id'
                )
                ->addColumn(
                    'template_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'unsigned' => true,
                        'nullable' => false
                    ],
                    'Template ID'
                )
                ->addColumn(
                    'title',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Title'
                )
                ->addColumn(
                    'type',
                    Table::TYPE_TEXT,
                    50,
                    ['nullable' => true, 'default' => null],
                    'Type'
                )
                ->addColumn(
                    'is_require',
                    Table::TYPE_SMALLINT,
                    null,
                    ['nullable' => false, 'default' => '1'],
                    'Is Required'
                )
                ->addColumn(
                    'sku',
                    Table::TYPE_TEXT,
                    64,
                    [],
                    'SKU'
                )
                ->addColumn(
                    'max_characters',
                    Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true],
                    'Max Characters'
                )
                ->addColumn(
                    'file_extension',
                    Table::TYPE_TEXT,
                    50,
                    [],
                    'File Extension'
                )
                ->addColumn(
                    'image_size_x',
                    Table::TYPE_SMALLINT,
                    null,
                    ['unsigned' => true],
                    'Image Size X'
                )
                ->addColumn(
                    'image_size_y',
                    Table::TYPE_SMALLINT,
                    null,
                    ['unsigned' => true],
                    'Image Size Y'
                )
                ->addColumn(
                    'sort_order',
                    Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                    'Sort Order'
                )
                ->addColumn(
                    'price',
                    Table::TYPE_DECIMAL,
                    '12,4',
                    ['nullable' => false, 'default' => '0.0000'],
                    'Price'
                )
                ->addColumn(
                    'price_type',
                    Table::TYPE_TEXT,
                    7,
                    ['nullable' => false, 'default' => 'fixed'],
                    'Price Type'
                )
                ->addColumn(
                    'layout',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'Option Layout'
                )
                ->addColumn(
                    'description',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => true],
                    'Description'
                )
                ->addColumn(
                    'css_class',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'Css Class'
                )
                ->addColumn(
                    'image_height',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'Image Height'
                )
                ->addColumn(
                    'image_width',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'Image Width'
                )
                ->addColumn(
                    'row_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => true],
                    'Row ID'
                )
                ->addForeignKey(
                    $installer->getFkName(
                        'ma_custom_template_option',
                        'template_id',
                        'id',
                        'id'
                    ),
                    'template_id',
                    $installer->getTable('ma_custom_template'),
                    'template_id',
                    Table::ACTION_CASCADE,
                    Table::ACTION_CASCADE
                )
                ->setComment(
                    'Custom template options'
                );
            $installer->getConnection()->createTable($templateOption);

            /* ma_custom_template_option_type */
            $templateOptionType = $installer->getConnection()
                ->newTable($installer->getTable('ma_custom_template_option_type'))
                ->addColumn(
                    'option_type_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'Option type id'
                )
                ->addColumn(
                    'option_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'unsigned' => true,
                        'nullable' => false
                    ],
                    'option ID'
                )
                ->addColumn(
                    'title',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Title'
                )
                ->addColumn(
                    'sku',
                    Table::TYPE_TEXT,
                    64,
                    [],
                    'SKU'
                )
                ->addColumn(
                    'sort_order',
                    Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                    'Sort Order'
                )
                ->addColumn(
                    'price',
                    Table::TYPE_DECIMAL,
                    '12,4',
                    ['nullable' => false, 'default' => '0.0000'],
                    'Price'
                )
                ->addColumn(
                    'price_type',
                    Table::TYPE_TEXT,
                    7,
                    ['nullable' => false, 'default' => 'fixed'],
                    'Price Type'
                )
                ->addColumn(
                    'store_image',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Store Image'
                )
                ->addColumn(
                    'description',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Description'
                )
                ->addColumn(
                    'dependent_options',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => true],
                    'Dependent Options'
                )
                ->addColumn(
                    'row_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => true],
                    'Row ID'
                )
                ->addForeignKey(
                    $installer->getFkName(
                        'ma_custom_template_option_type',
                        'option_id',
                        'id',
                        'entity_id'
                    ),
                    'option_id',
                    $installer->getTable('ma_custom_template_option'),
                    'option_id',
                    Table::ACTION_CASCADE,
                    Table::ACTION_CASCADE
                )
                ->setComment(
                    'Custom template option type'
                );
            $installer->getConnection()->createTable($templateOptionType);

            /* catalog_product_option_type_value */
            $eavTable = $installer->getTable('catalog_product_option_type_value');
            $columns = [
                'store_image' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => false,
                    'comment' => 'Store Image',
                ],
                'description' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => false,
                    'comment' => 'Description',
                ],
            ];

            $connection = $installer->getConnection();
            foreach ($columns as $name => $definition) {
                $connection->addColumn($eavTable, $name, $definition);
            }
            /* catalog_product_option */
            $eavTable = $installer->getTable('catalog_product_option');
            $columns = [
                'layout' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => false,
                    'comment' => 'Option Layout',
                ],
                'description' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => false,
                    'comment' => 'Description',
                ],
                'css_class' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => false,
                    'comment' => 'Css Class',
                ],
                'image_height' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => false,
                    'comment' => 'Image Height',
                ],
                'image_width' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => false,
                    'comment' => 'Image Width',
                ],
            ];

            $connection = $installer->getConnection();
            foreach ($columns as $name => $definition) {
                $connection->addColumn($eavTable, $name, $definition);
            }
        } // For Version 1.0.1

        $installer->endSetup();
    }
}
