<?php

namespace MageArray\CustomOptions\Ui\DataProvider\Product\Form\Modifier;

use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Form\Element\Input;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Ui\Component\Form\Element\Textarea;
use Magento\Ui\Component\Form\Element\Select;
use MageArray\CustomOptions\Model\Template\Option as TemplateOption;
use Magento\Framework\App\ObjectManager;

/**
 * Class CustomOptions
 * @package MageArray\CustomOptions\Ui\DataProvider\Product\Form\Modifier
 */
class CustomOptions extends
    \Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\CustomOptions
{
    /**
     *
     */
    const FIELD_MA_CUSTOM_OPTION = 'ma_custom_option';
    /**
     *
     */
    const FIELD_LAYOUT_NAME = 'layout';
    /**
     *
     */
    const FIELD_DESCRIPTION_NAME = 'description';
    /**
     *
     */
    const FIELD_CSS_CLASS_NAME = 'css_class';
    /**
     *
     */
    const FIELD_IMAGE_HEIGHT_NAME = 'image_height';
    /**
     *
     */
    const FIELD_IMAGE_WIDTH_NAME = 'image_width';
    /**
     *
     */
    const FIELD_IMAGE_UPLOAD_NAME = 'image';
    /**
     *
     */
    const FIELD_IMAGE_DISPLAY_NAME = 'store_image';

    /**
     * @var
     */
    protected $_dataHelper;

    /**
     * @return mixed
     */
    public function isEnable()
    {
        if (!$this->_dataHelper) {
            $objectManager = ObjectManager::getInstance();
            $this->_dataHelper = $objectManager->get(
                '\MageArray\CustomOptions\Helper\Data');
        }

        return $this->_dataHelper->enableExtraElements();
    }

    /**
     * Get config for container with fields for all types except "select"
     *
     * @param int $sortOrder
     * @return array
     */
    protected function getStaticTypeContainerConfig($sortOrder)
    {
        $config = parent::getStaticTypeContainerConfig($sortOrder);
        $config['children'][static::FIELD_MA_CUSTOM_OPTION] =
            $this->getCustomOptionsFieldConfig(200);
        return $config;
    }

    /**
     * Get config for grid for "select" types
     *
     * @param int $sortOrder
     * @return array
     */
    protected function getSelectTypeGridConfig($sortOrder)
    {
        $config = parent::getSelectTypeGridConfig($sortOrder);

        $config['children']['record']['children']
        [static::FIELD_MA_CUSTOM_OPTION] =
            $this->getCustomOptionsFieldConfig(200);

        if ($this->isEnable()) {
            $config['arguments']['data']['config']['additionalClasses'] =
                'admin__field-wide magearrayoptions';

            $config['children']['record']['children']
            [static::FIELD_IMAGE_DISPLAY_NAME] = $this->getImgDisplayConfig(55);
            $config['children']['record']['children']
            [static::FIELD_IMAGE_UPLOAD_NAME] = $this->getImgConfig(56);
            $config['children']['record']['children']
            [static::FIELD_DESCRIPTION_NAME] =
                $this->getDescriptionFieldConfig(57);

            if (isset($config['children']['record']['children']
                [static::FIELD_IS_DELETE])) {
                unset($config['children']['record']['children']
                    [static::FIELD_IS_DELETE]);
            }
            $config['children']['record']['children']
            [static::FIELD_IS_DELETE] = $this->getIsDeleteFieldConfig(90);
        }

        return $config;
    }

    /**
     * @param int $sortOrder
     * @return array
     */
    protected function getCommonContainerConfig($sortOrder)
    {
        $commonContainer = parent::getCommonContainerConfig($sortOrder);

        if ($this->isEnable()) {
            $commonContainer['children']
            [static::FIELD_LAYOUT_NAME] = $this->getLayoutFieldConfig(50);
            $commonContainer['children']
            [static::FIELD_DESCRIPTION_NAME] =
                $this->getDescriptionFieldConfig(60);
            $commonContainer['children']
            [static::FIELD_CSS_CLASS_NAME] = $this->getCSSClassFieldConfig(70);
            $commonContainer['children']
            [static::FIELD_IMAGE_HEIGHT_NAME] =
                $this->getImageHeightFieldConfig(80);
            $commonContainer['children']
            [static::FIELD_IMAGE_WIDTH_NAME] =
                $this->getImageWidthFieldConfig(90);
        }

        return $commonContainer;
    }

    /**
     * @param $sortOrder
     * @return array
     */
    protected function getImgDisplayConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Display Image'),
                        'fit' => true,
                        'additionalClasses' => 'image_path',
                        'component' =>
                            'MageArray_CustomOptions/js/view-option-image',
                        'componentType' => Field::NAME,
                        'formElement' => Input::NAME,
                        'dataScope' => static::FIELD_IMAGE_DISPLAY_NAME,
                        'dataType' => Text::NAME,
                        'sortOrder' => $sortOrder,
                        'validation' => [
                            'required-entry' => false
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $sortOrder
     * @return array
     */
    protected function getImgConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Image Upload'),
                        'fit' => true,
                        'additionalClasses' => 'image_upload',
                        'component' =>
                            'MageArray_CustomOptions/js/upload-option-image',
                        'componentType' => Field::NAME,
                        'formElement' => Input::NAME,
                        'dataScope' => static::FIELD_IMAGE_UPLOAD_NAME,
                        'dataType' => Text::NAME,
                        'sortOrder' => $sortOrder,
                        'validation' => [
                            'required-entry' => false
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $sortOrder
     * @param array $options
     * @return array
     */
    protected function getDescriptionFieldConfig($sortOrder, array $options = [])
    {
        return array_replace_recursive(
            [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'label' => __('Description'),
                            'additionalClasses' => 'option_description',
                            'componentType' => Textarea::NAME,
                            'formElement' => Input::NAME,
                            'dataScope' => static::FIELD_DESCRIPTION_NAME,
                            'dataType' => Text::NAME,
                            'sortOrder' => $sortOrder,
                        ],
                    ],
                ],
            ],
            $options
        );
    }

    /**
     * @return array
     */
    protected function getLayoutOptions()
    {
        $options = TemplateOption::getLayoutOptions();

        foreach ($options as $key => $label) {
            $group = [
                'value' => $key,
                'label' => $label,
                'optgroup' => []
            ];
            $options[] = $group;
        }

        return $options;
    }

    /**
     * @param $sortOrder
     * @return array
     */
    protected function getLayoutFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Option Layout'),
                        'additionalClasses' => 'option_image_layout',
                        'componentType' => Field::NAME,
                        'formElement' => Select::NAME,
                        'dataScope' => static::FIELD_LAYOUT_NAME,
                        'dataType' => Text::NAME,
                        'sortOrder' => $sortOrder,
                        'options' => $this->getLayoutOptions(),
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $sortOrder
     * @return array
     */
    protected function getCSSClassFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('CSS Class'),
                        'additionalClasses' => 'css_class',
                        'componentType' => Field::NAME,
                        'formElement' => Input::NAME,
                        'dataScope' => static::FIELD_CSS_CLASS_NAME,
                        'dataType' => Text::NAME,
                        'sortOrder' => $sortOrder,
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $sortOrder
     * @return array
     */
    protected function getImageHeightFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Image Height'),
                        'additionalClasses' => 'image_height',
                        'addafter' => __('px.'),
                        'componentType' => Field::NAME,
                        'formElement' => Input::NAME,
                        'dataScope' => static::FIELD_IMAGE_HEIGHT_NAME,
                        'dataType' => Text::NAME,
                        'sortOrder' => $sortOrder,
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $sortOrder
     * @return array
     */
    protected function getImageWidthFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Image Width'),
                        'additionalClasses' => 'image_width',
                        'addafter' => __('px.'),
                        'componentType' => Field::NAME,
                        'formElement' => Input::NAME,
                        'dataScope' => static::FIELD_IMAGE_WIDTH_NAME,
                        'dataType' => Text::NAME,
                        'sortOrder' => $sortOrder,
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $sortOrder
     * @return array
     */
    protected function getCustomOptionsFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Field::NAME,
                        'formElement' => Input::NAME,
                        'dataScope' => static::FIELD_MA_CUSTOM_OPTION,
                        'dataType' => Text::NAME,
                        'sortOrder' => $sortOrder,
                        'visible' => false,
                    ],
                ],
            ],
        ];
    }

    /**
     * @param int $sortOrder
     * @return array
     */
    protected function getIsRequireFieldConfig($sortOrder)
    {
        $config = parent::getIsRequireFieldConfig($sortOrder);

        $config['arguments']['data']['config']['additionalClasses'] =
            'nb-required';

        return $config;
    }

    /**
     * @param int $sortOrder
     * @return array
     */
    protected function getTypeFieldConfig($sortOrder)
    {
        $config = parent::getTypeFieldConfig($sortOrder);

        $config['arguments']['data']['config']['additionalClasses'] =
            'image_type_select';
        $config['arguments']['data']['config']['component'] =
            'MageArray_CustomOptions/js/custom-options-type';
        $config['arguments']['data']['config']['elementTmpl'] =
            'MageArray_CustomOptions/form/element/ui-select';

        return $config;
    }
}
