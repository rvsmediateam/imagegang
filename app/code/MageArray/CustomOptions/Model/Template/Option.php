<?php
namespace MageArray\CustomOptions\Model\Template;

use Magento\Framework\Model\AbstractModel;
use MageArray\CustomOptions\Helper\Data as DataHelper;
use Magento\Framework\App\ObjectManager;

/**
 * Class Option
 * @package MageArray\CustomOptions\Model\Template
 */
class Option extends AbstractModel
{
    /**
     *
     */
    const OPTION_GROUP_TEXT = 'text';

    /**
     *
     */
    const OPTION_GROUP_FILE = 'file';

    /**
     *
     */
    const OPTION_GROUP_SELECT = 'select';

    /**
     *
     */
    const OPTION_GROUP_DATE = 'date';

    /**
     *
     */
    const OPTION_TYPE_FIELD = 'field';

    /**
     *
     */
    const OPTION_TYPE_AREA = 'area';

    /**
     *
     */
    const OPTION_TYPE_FILE = 'file';

    /**
     *
     */
    const OPTION_TYPE_DROP_DOWN = 'drop_down';

    /**
     *
     */
    const OPTION_TYPE_RADIO = 'radio';

    /**
     *
     */
    const OPTION_TYPE_CHECKBOX = 'checkbox';

    /**
     *
     */
    const OPTION_TYPE_MULTIPLE = 'multiple';

    /**
     *
     */
    const OPTION_TYPE_DATE = 'date';

    /**
     *
     */
    const OPTION_TYPE_DATE_TIME = 'date_time';

    /**
     *
     */
    const OPTION_TYPE_TIME = 'time';

    /**
     *
     */
    const LAYOUT_ABOVE_OPTION = 'above_option';
    /**
     *
     */
    const LAYOUT_ABOVE_OPTION_TEXT = 'Above Option';
    /**
     *
     */
    const LAYOUT_BEFORE_OPTION = 'before_option';
    /**
     *
     */
    const LAYOUT_BEFORE_OPTION_TEXT = 'Before Option';
    /**
     *
     */
    const LAYOUT_BELOW_OPTION = 'below_option';
    /**
     *
     */
    const LAYOUT_BELOW_OPTION_TEXT = 'Below Option';
    /**
     *
     */
    const LAYOUT_COLOR_PICKER = 'color_picker';
    /**
     *
     */
    const LAYOUT_COLOR_PICKER_TEXT = 'Color picker';
    /**
     *
     */
    const LAYOUT_COLOR_PICKER_MAIN = 'picker_main';
    /**
     *
     */
    const LAYOUT_COLOR_PICKER_MAIN_TEXT = 'Color picker & Main image';
    /**
     *
     */
    const LAYOUT_GRID = 'grid';
    /**
     *
     */
    const LAYOUT_GRID_TEXT = 'Grid';
    /**
     *
     */
    const LAYOUT_LIST = 'list';
    /**
     *
     */
    const LAYOUT_LIST_TEXT = 'List';

    /**
     * @var null
     */
    protected $_values = null;

    /**
     *
     */
    protected function _construct()
    {
        $this->_init(
            'MageArray\CustomOptions\Model\ResourceModel\Template\Option'
        );
    }

    /**
     * Get group name of option by given option type
     *
     * @param string $type
     * @return string
     */
    public function getGroupByType($type = null)
    {
        if ($type === null) {
            $type = $this->getType();
        }
        $optionGroupsToTypes = [
            self::OPTION_TYPE_FIELD => self::OPTION_GROUP_TEXT,
            self::OPTION_TYPE_AREA => self::OPTION_GROUP_TEXT,
            self::OPTION_TYPE_FILE => self::OPTION_GROUP_FILE,
            self::OPTION_TYPE_DROP_DOWN => self::OPTION_GROUP_SELECT,
            self::OPTION_TYPE_RADIO => self::OPTION_GROUP_SELECT,
            self::OPTION_TYPE_CHECKBOX => self::OPTION_GROUP_SELECT,
            self::OPTION_TYPE_MULTIPLE => self::OPTION_GROUP_SELECT,
            self::OPTION_TYPE_DATE => self::OPTION_GROUP_DATE,
            self::OPTION_TYPE_DATE_TIME => self::OPTION_GROUP_DATE,
            self::OPTION_TYPE_TIME => self::OPTION_GROUP_DATE,
        ];

        return isset($optionGroupsToTypes[$type]) ?
            $optionGroupsToTypes[$type] : '';
    }

    /**
     * @param bool $withSelect
     * @return array
     */
    public static function getLayoutOptions($withSelect = true)
    {
        $options = [
            self::LAYOUT_ABOVE_OPTION => __(self::LAYOUT_ABOVE_OPTION_TEXT),
            self::LAYOUT_BEFORE_OPTION => __(self::LAYOUT_BEFORE_OPTION_TEXT),
            self::LAYOUT_BELOW_OPTION => __(self::LAYOUT_BELOW_OPTION_TEXT),
            self::LAYOUT_COLOR_PICKER => __(self::LAYOUT_COLOR_PICKER_TEXT),
            self::LAYOUT_COLOR_PICKER_MAIN => __(self::LAYOUT_COLOR_PICKER_MAIN_TEXT),
            self::LAYOUT_GRID => __(self::LAYOUT_GRID_TEXT),
            self::LAYOUT_LIST => __(self::LAYOUT_LIST_TEXT),
        ];

        if ($withSelect) {
            $options = array_merge(['' => __('-- Select --')], $options);
        }

        return $options;
    }

    /**
     * @return null
     */
    public function getValues()
    {
        if (!$this->_values) {
            $objectManager = ObjectManager::getInstance();
            $optionValueModel = $objectManager->get(
                '\MageArray\CustomOptions\Model\Template\Option\Type');
            $this->_values = $optionValueModel->getCollection()
                ->addFieldToFilter('option_id', ['eq' => $this->getId()]);
            $this->_values->getSelect()
                ->order('main_table.sort_order');
        }
        return $this->_values;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->getTemplateId() . '_' . $this->getOptionId();
    }

    /**
     * @return bool
     */
    public function haveAnyChanges()
    {
        $hasChanges = false;
        $availableFields = [
            'title',
            'type',
            'is_require',
            'sku',
            'max_characters',
            'file_extension',
            'image_size_x',
            'image_size_y',
            'sort_order',
            'price',
            'price_type',
            'layout',
            'description',
            'css_class',
            'image_height',
            'image_width',
            'row_id'];
        foreach ($availableFields as $field) {
            if ($this->hasData($field) && $this->dataHasChangedFor($field)) {
                $hasChanges = true;
                break;
            }
        }
        return $hasChanges;
    }

    /**
     * @return mixed
     */
    public function getMergeData()
    {
        $data = $this->getData();
        $fields = [
            'option_id',
            'previous_type',
            'id', 'values',
            'template_id',
            'previous_group'];
        foreach ($fields as $key) {
            if (isset($data[$key])) {
                unset($data[$key]);
            }
        }
        if (!isset($data[DataHelper::OPTION_FIELD])) {
            $data[DataHelper::OPTION_FIELD] = $this->getIdentifier();
        }

        return $data;
    }
}
