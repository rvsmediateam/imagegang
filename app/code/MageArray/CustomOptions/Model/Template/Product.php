<?php
namespace MageArray\CustomOptions\Model\Template;

use Magento\Framework\Model\AbstractModel;

/**
 * Class Product
 * @package MageArray\CustomOptions\Model\Template
 */
class Product extends AbstractModel
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_init(
            'MageArray\CustomOptions\Model\ResourceModel\Template\Product');
    }
}
