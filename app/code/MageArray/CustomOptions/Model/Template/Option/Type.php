<?php
namespace MageArray\CustomOptions\Model\Template\Option;

use Magento\Framework\Model\AbstractModel;
use MageArray\CustomOptions\Helper\Data as DataHelper;

/**
 * Class Type
 * @package MageArray\CustomOptions\Model\Template\Option
 */
class Type extends AbstractModel
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_init(
            'MageArray\CustomOptions\Model\ResourceModel\Template\Option\Type');
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->getOptionId() . '_' . $this->getOptionTypeId();
    }

    /**
     * @return mixed
     */
    public function getMergeData()
    {
        $data = $this->getData();
        foreach (['option_id', 'option_type_id'] as $key) {
            if (isset($data[$key])) {
                unset($data[$key]);
            }
        }
        if (!isset($data[DataHelper::OPTION_FIELD])) {
            $data[DataHelper::OPTION_FIELD] = $this->getIdentifier();
        }

        return $data;
    }

    /**
     * @return bool
     */
    public function haveAnyChanges()
    {
        $hasChanges = false;
        $availableFields = [
            'title',
            'sku',
            'sort_order',
            'price',
            'price_type',
            'store_image',
            'row_id',
            'dependent_options'];
        foreach ($availableFields as $field) {
            if ($this->hasData($field) && $this->dataHasChangedFor($field)) {
                $hasChanges = true;
                break;
            }
        }
        return $hasChanges;
    }
}
