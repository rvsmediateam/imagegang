<?php
namespace MageArray\CustomOptions\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Template
 * @package MageArray\CustomOptions\Model\ResourceModel
 */
class Template extends AbstractDb
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_init('ma_custom_template', 'template_id');
    }
}
