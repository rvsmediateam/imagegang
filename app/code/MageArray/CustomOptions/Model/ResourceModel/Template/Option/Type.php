<?php
namespace MageArray\CustomOptions\Model\ResourceModel\Template\Option;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Type
 * @package MageArray\CustomOptions\Model\ResourceModel\Template\Option
 */
class Type extends AbstractDb
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_init('ma_custom_template_option_type', 'option_type_id');
    }
}
