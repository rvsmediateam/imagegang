<?php
namespace MageArray\CustomOptions\Model\ResourceModel\Template\Option\Type;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package MageArray\CustomOptions\Model\ResourceModel\Template\Option\Option
 */
class Collection extends AbstractCollection
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_init(
            'MageArray\CustomOptions\Model\Template\Option\Type',
            'MageArray\CustomOptions\Model\ResourceModel\Template\Option\Type');
    }
}
