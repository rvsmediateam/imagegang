<?php
namespace MageArray\CustomOptions\Model\ResourceModel\Template;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package MageArray\CustomOptions\Model\ResourceModel\Template
 */
class Collection extends AbstractCollection
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_init('MageArray\CustomOptions\Model\Template',
            'MageArray\CustomOptions\Model\ResourceModel\Template');
    }
}
