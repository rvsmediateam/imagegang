<?php
namespace MageArray\CustomOptions\Model\ResourceModel\Template;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Option
 * @package MageArray\CustomOptions\Model\ResourceModel\Template
 */
class Option extends AbstractDb
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_init('ma_custom_template_option', 'option_id');
    }
}
