<?php
namespace MageArray\CustomOptions\Model\ResourceModel\Template\Product;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package MageArray\CustomOptions\Model\ResourceModel\Template\Product
 */
class Collection extends AbstractCollection
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_init(
            'MageArray\CustomOptions\Model\Template\Product',
            'MageArray\CustomOptions\Model\ResourceModel\Template\Product'
        );
    }
}
