<?php
namespace MageArray\CustomOptions\Model\ResourceModel\Template;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Product
 * @package MageArray\CustomOptions\Model\ResourceModel\Template
 */
class Product extends AbstractDb
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_init('ma_custom_template_product', 'id');
    }
}
