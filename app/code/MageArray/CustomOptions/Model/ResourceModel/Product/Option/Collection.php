<?php
namespace MageArray\CustomOptions\Model\ResourceModel\Product\Option;

use Magento\Framework\App\ObjectManager;
use Magento\Catalog\Model\ResourceModel\Product\Option;

/**
 * Class Collection
 * @package MageArray\CustomOptions\Model\ResourceModel\Product\Option
 */
class Collection extends Option\Collection
{
    /**
     * @return $this
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        if ($this->_storeManager->getStore()->getId() > 0) {
            $objectManager = ObjectManager::getInstance();
            $dataHelper = $objectManager->get(
                'MageArray\CustomOptions\Helper\Data');
            $disableIds = $dataHelper->getDisableTemplateIds();
            if ($disableIds) {
                foreach ($disableIds as $id) {
                    $this->getSelect()->where(
                        "ma_custom_option NOT LIKE '{$id}_%'");
                }
                $this->getSelect()->orWhere("ma_custom_option IS NULL");
                $this->getSelect()->orWhere("ma_custom_option=''");
            }
        }

        return $this;
    }
}
