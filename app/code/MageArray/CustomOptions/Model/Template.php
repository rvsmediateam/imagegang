<?php
namespace MageArray\CustomOptions\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\App\ObjectManager;
use MageArray\CustomOptions\Model\Template\Option;

/**
 * Class Template
 * @package MageArray\CustomOptions\Model
 */
class Template extends AbstractModel
{
    /**
     * @var
     */
    protected $_products;
    /**
     * @var null
     */
    protected $_options = null;
    /**
     * @var
     */
    protected $_objectManager;
    /**
     * @var
     */
    protected $_optionsCollection;
    
    /**
     *
     */
    protected function _construct()
    {
        $this->_init(
            'MageArray\CustomOptions\Model\ResourceModel\Template');
    }

    /**
     * @return int
     */
    public function getStoreId()
    {
        return 0;
    }

    /**
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        if (!$this->_objectManager) {
            $this->_objectManager = ObjectManager::getInstance();
        }
        return $this->_objectManager;
    }

    /**
     * @param bool $refresh
     * @return mixed
     */
    public function getProducts($refresh = false)
    {
        if (!$this->_products || $refresh == true) {
            $templateProductModel = $this->getObjectManager()->get(
                '\MageArray\CustomOptions\Model\Template\Product');
            $this->_products = $templateProductModel->getCollection()
                ->addFieldToFilter('template_id',
                    ['eq' => (int)$this->getId()]);
        }

        return $this->_products;
    }

    /**
     * @param bool $refresh
     * @return mixed
     */
    public function getProductIds($refresh = false)
    {
        return $this->getProducts($refresh)->getColumnValues('product_id');
    }

    /**
     * @return bool
     */
    public function getOptionsReadonly()
    {
        return false;
    }

    /**
     * @param $templateId
     * @param bool $refresh
     * @return mixed
     */
    public function getOptionsCollectionByTemplateId($templateId, $refresh = false)
    {
        $templateId = (int)$templateId;
        if (!isset($this->_optionsCollection[$templateId])
            || $refresh == true) {
            $optionModel = $this->getObjectManager()->get(
                '\MageArray\CustomOptions\Model\Template\Option');
            $this->_optionsCollection[$templateId] =
                $optionModel->getCollection()
                    ->addFieldToFilter('template_id', ['eq' => $templateId]);

            $this->_optionsCollection[$templateId]
                ->getSelect()
                ->order('main_table.sort_order');
        }
        return $this->_optionsCollection[$templateId];
    }

    /**
     * @param bool $refresh
     * @return array|null
     */
    public function getOptions($refresh = false)
    {
        if (!is_array($this->_options) || $refresh == true) {
            $this->_options = [];
            $optionCollection = $this->getOptionsCollectionByTemplateId(
                $this->getId(), $refresh);

            foreach ($optionCollection as $_option) {
                if ($_option->getGroupByType() == Option::OPTION_GROUP_SELECT) {
                    $values = $_option->getValues();
                    if ($values->getSize()) {
                        $_option->setValues($values);
                    }
                }
                $this->_options[] = $_option;
            }
        }
        return $this->_options;
    }
}
