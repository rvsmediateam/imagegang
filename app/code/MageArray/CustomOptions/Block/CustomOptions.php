<?php
namespace MageArray\CustomOptions\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\Registry;
use Magento\Framework\Image\AdapterFactory;
use MageArray\CustomOptions\Helper\Data as DataHelper;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class CustomOptions
 * @package MageArray\CustomOptions\Block
 */
class CustomOptions extends Template
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $_fileSystem;
    /**
     * @var DataHelper
     */
    protected $_dataHelper;
    /**
     * @var Registry
     */
    protected $_registry;
    /**
     * @var AdapterFactory
     */
    protected $_imageFactory;

    /**
     * CustomOptions constructor.
     * @param Template\Context $context
     * @param DataHelper $dataHelper
     * @param Registry $registry
     * @param AdapterFactory $imageFactory
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        DataHelper $dataHelper,
        Registry $registry,
        AdapterFactory $imageFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_storeManager = $context->getStoreManager();
        $this->_fileSystem = $context->getFilesystem();
        $this->_dataHelper = $dataHelper;
        $this->_registry = $registry;
        $this->_imageFactory = $imageFactory;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->_registry->registry('current_product');
    }

    /**
     * @return bool
     */
    public function showAccordion()
    {
        $status = false;
        if ($this->hasAccordion()) {
            $status = true;
            $product = $this->getProduct();
            if ($product->getAccordionWithOption()) {
                $status = false;
            }
        }
        return $status;
    }

    /**
     * @return mixed
     */
    public function getOptionList()
    {
        $product = $this->getProduct();
        $option = [];
        $types = ['drop_down', 'radio', 'checkbox', 'multiple'];
        foreach ($product->getOptions() as $o) {
            if (in_array($o->getType(), $types)) {
                foreach ($o->getValues() as $value) {
                    $typeId = $value->getOptionTypeId();
                    $thumbnail = '';
                    $image = '';
                    $descptn = '';
                    if ($value->getStoreImage()) {
                        $thumbnail = $this->resize(
                            $value->getStoreImage(),
                            $o->getImageWidth(),
                            $o->getImageHeight()
                        );
                        $image = $this->getMediaUrl() . $value->getStoreImage();
                        $descptn = $value->getDescription();
                    }

                    $option['thumbnail'][$typeId] = $thumbnail;
                    $option['fullimage'][$typeId] = $image;
                    $option['descptn'][$typeId] = $descptn;
                    $option['title'][$typeId] = $value->getDefaultTitle();
                }
            }
            $option['classs'][$o->getId()] = $o->getCssClass();
            $option['description'][$o->getId()] = $o->getDescription();
            $option['layout'][$o->getId()] = $o->getLayout();
            // settings
            $option['showoption'] = $this->showOption();
            $option['setaccordion'] = $this->showAccordion();
            $option['showCustomOptionTooltip'] = $this->_dataHelper->showCustomOptionTooltip();
            $option['showOptionsTooltip'] = $this->_dataHelper->showOptionsTooltip();
        }
        return json_encode($option, true);
    }

    /**
     * @return mixed
     */
    public function showOption()
    {
        return $this->_dataHelper->hideColorPickerLayout();
    }

    /**
     * @return mixed
     */
    public function hasAccordion()
    {
        return $this->_dataHelper->enableAccordion();
    }

    /**
     * @return string
     */
    public function getMediaUrl()
    {
        return $this->_dataHelper->getCatalogMediaUrl();
    }

    /**
     * @param $image
     * @param null $width
     * @param null $height
     * @return string
     */
    public function resize($image, $width = null, $height = null)
    {
        if (!$width || !$height) {
            if ($width) {
                $height = $width;
            } elseif ($height) {
                $width = $height;
            } else {
                $height = 100;
                $width = 100;
            }
        }

        try {
            $absolutePath = $this->_fileSystem
                    ->getDirectoryRead(DirectoryList::MEDIA)
                    ->getAbsolutePath('catalog/product') . $image;
            $resized = 'resized/' . $width . 'x' . $height;
            $imageResized = $this->_fileSystem
                    ->getDirectoryRead(DirectoryList::MEDIA)
                    ->getAbsolutePath($resized).'/' . $image;
            if (!file_exists($imageResized)) {
                //create image factory...
                $imageResize = $this->_imageFactory->create();
                $imageResize->open($absolutePath);
                $imageResize->constrainOnly(TRUE);
                $imageResize->keepTransparency(TRUE);
                $imageResize->keepFrame(FALSE);
                $imageResize->keepAspectRatio(TRUE);
                $imageResize->resize($width, $height);
                //save image
                $imageResize->save($imageResized);
            }
            return $this->_dataHelper->getMediaUrl() . $resized . $image;
        } catch (\Exception $e) {
            return null;
        }
    }
}
