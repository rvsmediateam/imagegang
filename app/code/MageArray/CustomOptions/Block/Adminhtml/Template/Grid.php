<?php
namespace MageArray\CustomOptions\Block\Adminhtml\Template;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Helper\Data;
use MageArray\CustomOptions\Model\Template;
use Magento\Backend\Block\Widget\Grid\Extended;

/**
 * Class Grid
 * @package MageArray\CustomOptions\Block\Adminhtml\Temlate
 */
class Grid extends Extended
{
    /**
     * @var Template
     */
    protected $_templateModel;
    /**
     * @var Template\Product
     */
    protected $_templateProduct;

    /**
     * Grid constructor.
     * @param Context $context
     * @param Data $backendHelper
     * @param Template $templateModel
     * @param Template\Product $templateProduct
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $backendHelper,
        Template $templateModel,
        Template\Product $templateProduct,
        array $data = []
    ) {
        parent::__construct($context, $backendHelper, $data);
        $this->_templateModel = $templateModel;
        $this->_templateProduct = $templateProduct;
    }

    /**
     *
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('artistGrid');
        $this->setDefaultSort('template_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
    }

    /**
     * Prepare collection
     *
     * @return \Magento\Backend\Block\Widget\Grid
     */
    protected function _prepareCollection()
    {
        $templateCollection = $this->_templateModel
            ->getCollection();
        $tableName = $this->_templateProduct->getResource()->getMainTable();
        $templateCollection->getSelect()
            ->joinLeft(['TP' => $tableName],
                'main_table.template_id = TP.template_id',
                ['product_count' => "COUNT(TP.product_id)"])
            ->group('main_table.template_id');

        $this->setCollection($templateCollection);
        return parent::_prepareCollection();
    }

    /**
     * Prepare columns
     *
     * @return \Magento\Backend\Block\Widget\Grid\Extended
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'template_id',
            [
                'header' => __('ID'),
                'index' => 'template_id'
            ]
        );

        $this->addColumn(
            'title',
            [
                'header' => __('Title'),
                'index' => 'title',
            ]
        );

        $this->addColumn(
            'product_count',
            [
                'header' => __('Products'),
                'index' => 'product_count'
            ]
        );

        $this->addColumn(
            'action',
            [
                'header' => __('Action'),
                'index' => 'template_id',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => [
                    [
                        'caption' => __('Edit'),
                        'url' => [
                            'base' => '*/*/edit',
                        ],
                        'field' => 'template_id'
                    ]
                ],
                'filter' => false,
                'sortable' => false
            ]
        );

        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('template_id');
        $this->getMassactionBlock()->setFormFieldName('template_ids');

        $this->getMassactionBlock()->addItem('delete', [
            'label' => __('Delete'),
            'url' => $this->getUrl('*/*/massDelete', ['' => '']),
            'confirm' => __('Are you sure?')
        ]);

        return $this;
    }
}