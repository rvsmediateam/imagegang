<?php
namespace MageArray\CustomOptions\Block\Adminhtml\Template\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;

/**
 * Class Main
 * @package MageArray\CustomOptions\Block\Adminhtml\Template\Edit\Tab
 */
class Main extends Generic implements TabInterface
{
    /**
     * Prepare form
     *
     * @return \Magento\Backend\Block\Widget\Form
     */
    protected function _prepareForm()
    {
        $template = $this->_coreRegistry->registry('current_template');

        $form = $this->_formFactory->create();

        $fieldset = $form->addFieldset('base_fieldset',
            ['legend' => __('Options Template Settings ')]);
        if ($template->getId()) {
            $fieldset->addField('template_id',
                'hidden',
                ['name' => 'template_id']);
        }

        $fieldset->addField(
            'title',
            'text',
            [
                'name' => 'title',
                'label' => __('Title'),
                'title' => __('Title'),
                'required' => true
            ]
        );

        $fieldset->addField(
            'can_option_delete_from_product',
            'select',
            [
                'label' => __('Option delete from product?'),
                'title' => __('Option delete from product?'),
                'name' => 'can_option_delete_from_product',
                'required' => true,
                'options' => ['1' => __('Yes'), '0' => __('No')],
                'note' => __('Option delete from the product when you delete option here?')
            ]
        );

        $fieldset->addField(
            'is_active',
            'select',
            [
                'label' => __('Active'),
                'title' => __('Active'),
                'name' => 'is_active',
                'required' => true,
                'options' => ['1' => __('Yes'), '0' => __('No')]
            ]
        );

        if (!$template->getId()) {
            $template->setIsActive(true);
        }
        $form->setValues($template->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Option Template');
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Option Template');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * @param string $html
     * @return string
     */
    protected function _afterToHtml($html)
    {
        $html = parent::_afterToHtml($html);
        $html .= $this->getChildHtml('admin.product.options');
        return $html;
    }
}
