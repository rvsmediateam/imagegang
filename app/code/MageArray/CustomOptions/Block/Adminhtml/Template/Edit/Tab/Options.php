<?php

namespace MageArray\CustomOptions\Block\Adminhtml\Template\Edit\Tab;

use Magento\Catalog\Block\Adminhtml\Product\Edit\Tab\Options as CustomOptions;
use Magento\Backend\Block\Widget;

/**
 * Class Options
 * @package MageArray\CustomOptions\Block\Adminhtml\Template\Edit\Tab
 */
class Options extends CustomOptions
{
    /**
     * @var string
     */
    protected $_template =
        'MageArray_CustomOptions::custom_option/template/edit/options.phtml';

    /**
     * Prepare Layout
     *
     * @return \Magento\Framework\View\Element\Template
     */
    protected function _prepareLayout()
    {
        $this->addChild(
            'add_button',
            'Magento\Backend\Block\Widget\Button',
            [
                'label' => __('Add New Option'),
                'class' => 'add',
                'id' => 'add_new_defined_option'
            ]
        );

        $this->addChild('options_box', 'MageArray\CustomOptions\Block\Adminhtml\Template\Edit\Tab\Options\Option');

        return Widget::_prepareLayout();
    }
}
