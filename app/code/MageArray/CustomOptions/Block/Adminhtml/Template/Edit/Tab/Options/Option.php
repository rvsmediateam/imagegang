<?php
namespace MageArray\CustomOptions\Block\Adminhtml\Template\Edit\Tab\Options;

use Magento\Catalog\Block\Adminhtml\Product\Edit\Tab\Options\Option as BackendOption;
use MageArray\CustomOptions\Model\Template\Option as TemplateOption;
use Magento\Framework\App\ObjectManager;
use Magento\Store\Model\Store;
use Magento\Store\Model\ScopeInterface;
use Magento\Catalog\Model\Product\Option as ProductOption;

/**
 * Class Option
 * @package MageArray\CustomOptions\Block\Adminhtml\Template\Edit\Tab\Options
 */
class Option extends BackendOption
{
    /**
     * @var string
     */
    protected $_template = 'custom_option/template/edit/options/option.phtml';

    /**
     * @var
     */
    protected $_templateInstance;

    /**
     * @var
     */
    protected $_dataHelper;

    /**
     * @return mixed
     */
    public function getDataHelper()
    {
        if (!$this->_dataHelper) {
            $this->_dataHelper = ObjectManager::getInstance()->get(
                'MageArray\CustomOptions\Helper\Data');
        }
        return $this->_dataHelper;
    }

    /**
     * @return mixed
     */
    public function hasDependantModuleEnable()
    {
        return $this->getDataHelper()->isDependantModuleEnable();
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->getCurrentTemplate();
    }

    /**
     * @return mixed
     */
    public function getCurrentTemplate()
    {
        if (!$this->_templateInstance) {
            $this->_templateInstance = $this->_coreRegistry
                ->registry('current_template');
        }
        return $this->_templateInstance;
    }

    /**
     * @return string
     */
    public function getTemplatesHtml()
    {
        if ($block = $this->getChildBlock('select_option_type')) {
            $child = $block->getChildBlock('delete_select_row_button');
            if ($child->getClass()) {
                $child->setClass($child->getClass() . ' action-delete');
            }
            $block->setTemplate('MageArray_CustomOptions::custom_option/template/edit/options/type/select.phtml');
        }
        return parent::getTemplatesHtml();
    }

    /**
     * @return array|\Magento\Framework\DataObject[]
     */
    public function getOptionValues()
    {
        $optionsArr = $this->getProduct()->getOptions();
        if ($optionsArr == null) {
            $optionsArr = [];
        }

        if (!$this->_values || $this->getIgnoreCaching()) {
            $showPrice = $this->getCanReadPrice();
            $values = [];
            $scope = (int)$this->_scopeConfig->getValue(
                Store::XML_PATH_PRICE_SCOPE,
                ScopeInterface::SCOPE_STORE
            );
            $dataHelper = $this->getDataHelper();
            $hasDependantModule = $this->hasDependantModuleEnable();
            $hasExtraElements = $dataHelper->enableExtraElements();
            foreach ($optionsArr as $option) {
                /* @var $option \Magento\Catalog\Model\Product\Option */

                $this->setItemCount($option->getOptionId());

                $value = [];

                $value['id'] = $option->getOptionId();
                $value['item_count'] = $this->getItemCount();
                $value['option_id'] = $option->getOptionId();
                $value['title'] = $option->getTitle();
                $value['type'] = $option->getType();
                $value['is_require'] = $option->getIsRequire();
                $value['sort_order'] = $option->getSortOrder();
                $value['can_edit_price'] = $this->getCanEditPrice();
                /* Custom */
                if ($hasExtraElements) {
                    $value['layout'] = $option->getLayout();
                    $value['description'] = $option->getDescription();
                    $value['css_class'] = $option->getCssClass();
                    $value['image_height'] = $option->getImageHeight();
                    $value['image_width'] = $option->getImageWidth();
                }

                if ($hasDependantModule) {
                    $value['row_id'] = $option->getRowId();
                }

                /* Custom */

                if ($this->getProduct()->getStoreId() != '0') {
                    $value['checkboxScopeTitle'] = $this->getCheckboxScopeHtml(
                        $option->getOptionId(),
                        'title',
                        $option->getStoreTitle() === null
                    );
                    $value['scopeTitleDisabled'] =
                        ($option->getStoreTitle() === null) ? 'disabled' : null;
                }

                if ($option->getGroupByType() ==
                    ProductOption::OPTION_GROUP_SELECT
                ) {
                    $i = 0;
                    $itemCount = 0;
                    foreach ($option->getValues() as $_value) {
                        $value['optionValues'][$i] = [
                            'item_count' => max($itemCount,
                                $_value->getOptionTypeId()),
                            'option_id' => $_value->getOptionId(),
                            'option_type_id' => $_value->getOptionTypeId(),
                            'title' => $_value->getTitle(),
                            'price' => $showPrice ? $this->getPriceValue(
                                $_value->getPrice(),
                                $_value->getPriceType()
                            ) : '',
                            'price_type' => $showPrice ?
                                $_value->getPriceType() : 0,
                            'sku' => $_value->getSku(),
                            'sort_order' => $_value->getSortOrder(),
                        ];
                        /* custom */
                        if ($hasExtraElements) {
                            $value['optionValues'][$i]['store_image'] =
                                $_value->getStoreImage();
                            $value['optionValues'][$i]['image_url'] =
                                $dataHelper->getImageUrl(
                                    $_value->getStoreImage());
                            $value['optionValues'][$i]['description'] =
                                $_value->getDescription();
                        }

                        if ($hasDependantModule) {
                            $value['optionValues'][$i]['dependent_options'] =
                                $_value->getDependentOptions();
                            $value['optionValues'][$i]['row_id'] =
                                $_value->getRowId();
                        }
                        /* custom */

                        if ($this->getProduct()->getStoreId() != '0') {
                            $value['optionValues'][$i]['checkboxScopeTitle'] =
                                $this->getCheckboxScopeHtml(
                                    $_value->getOptionId(),
                                    'title',
                                    $_value->getStoreTitle() === null,
                                    $_value->getOptionTypeId());
                            $value['optionValues'][$i]['scopeTitleDisabled'] =
                                ($_value->getStoreTitle() === null) ?
                                    'disabled' : null;
                            if ($scope == Store::PRICE_SCOPE_WEBSITE) {
                                $value['optionValues'][$i]
                                ['checkboxScopePrice'] =
                                    $this->getCheckboxScopeHtml(
                                        $_value->getOptionId(),
                                        'price',
                                        $_value->getstorePrice() === null,
                                        $_value->getOptionTypeId(),
                                        ['$(this).up(1).previous()']
                                    );
                                $value['optionValues'][$i]
                                ['scopePriceDisabled'] =
                                    ($_value->getStorePrice() === null)
                                        ? 'disabled' : null;
                            }
                        }
                        $i++;
                    }
                } else {
                    $value['price'] = $showPrice ? $this->getPriceValue(
                        $option->getPrice(),
                        $option->getPriceType()
                    ) : '';
                    $value['price_type'] = $option->getPriceType();
                    $value['sku'] = $option->getSku();
                    $value['max_characters'] = $option->getMaxCharacters();
                    $value['file_extension'] = $option->getFileExtension();
                    $value['image_size_x'] = $option->getImageSizeX();
                    $value['image_size_y'] = $option->getImageSizeY();
                    if ($this->getProduct()->getStoreId() != '0'
                        && $scope == Store::PRICE_SCOPE_WEBSITE
                    ) {
                        $value['checkboxScopePrice'] =
                            $this->getCheckboxScopeHtml(
                                $option->getOptionId(),
                                'price',
                                $option->getStorePrice() === null
                            );
                        $value['scopePriceDisabled'] =
                            ($option->getStorePrice() === null) ?
                                'disabled' : null;
                    }
                }
                $values[] = new \Magento\Framework\DataObject($value);
            }
            $this->_values = $values;
        }

        return $this->_values;
    }

    /**
     * @return array
     */
    public function getLayoutOptions()
    {
        return TemplateOption::getLayoutOptions();
    }

    /**
     * @return mixed
     */
    public function getLayoutSelectHtml()
    {
        $select = $this->getLayout()->createBlock(
            'Magento\Framework\View\Element\Html\Select'
        )->setData(
            [
                'id' => $this->getFieldId() . '_<%- data.id %>_layout',
                'class' => 'select select-product-option-layout',
            ]
        )->setName(
            $this->getFieldName() . '[<%- data.id %>][layout]'
        )->setOptions(
            $this->getLayoutOptions()
        );

        return $select->getHtml();
    }
}
