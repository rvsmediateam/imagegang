<?php
namespace MageArray\CustomOptions\Block\Adminhtml\Template\Edit;

use Magento\Backend\Block\Widget\Form\Generic;

/**
 * Class Form
 * @package MageArray\CustomOptions\Block\Adminhtml\Template\Edit
 */
class Form extends Generic
{
    /**
     * Prepare form
     *
     * @return \Magento\Backend\Block\Widget\Form
     */
    protected function _prepareForm()
    {
        $form = $this->_formFactory->create(
            [
                'data' => [
                    'id' => 'edit_form',
                    'action' => $this->getData('action'),
                    'method' => 'post',
                    'enctype' => 'multipart/form-data'
                ]
            ]
        );
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
