<?php
namespace MageArray\CustomOptions\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;
/**
 * Class Template
 * @package MageArray\CustomOptions\Block\Adminhtml
 */
class Template extends Container
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_template';
        $this->_blockGroup = 'MageArray_CustomOptions';
        $this->_headerText = __('Manage Templates');
        $this->_addButtonLabel = __('New Option Template');
        parent::_construct();
    }
}
