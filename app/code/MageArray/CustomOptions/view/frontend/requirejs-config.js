/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            optionImages:'MageArray_CustomOptions/js/custom.options',
            jqueryColorbox:'MageArray_CustomOptions/js/jquery.colorbox',
			maAccordion:'MageArray_CustomOptions/js/ma-accordion'
        }
    }
};
