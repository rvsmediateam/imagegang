define([
    'jquery',
    'mage/template',
    'jquery/ui',
    'MageArray_CustomOptions/js/jquery.colorbox'
], function ($, nbImageTplt, modal) {
    $.widget('mageArray.optionImages', {

        issetImg: false,
        mainImg: false,
        oldOption: [],
        preloadedImageThumb: [],
        preloadedDescptn: [],

        _create: function () {

            $.extend(this, this.options);
            $.extend(this, this.options.config);

            this.opImagesBox = nbImageTplt('[data-template=magearray-co-images-box]');
            this.opImgTplt = nbImageTplt('[data-template=magearray-co-images-thumbnail]');
            this.tooltipTemplate = nbImageTplt('[data-template=magearray-co-images-tooltip]');

            this.loadOptionImages();
            this.setPreSettings();

        },
        setMainImage: function (url = '', caption = '') {
            if (url) {
                var images = [];
                images.push({
                    caption: caption,
                    full: url,
                    img: url,
                    thumb: url
                });

                var gallery = jQuery('[data-gallery-role=gallery-placeholder]', '.column.main');
                var galleryObject = gallery.data('gallery');
                var imageArr = galleryObject.returnCurrentImages();

                imageCount = parseInt(imageArr.length);
                if (this.issetImg == false) {
                    this.issetImg = imageCount;
                    galleryObject.updateDataByIndex(this.issetImg, imageArr[0]);
                    galleryObject.seek(1);
                }

                galleryObject.updateDataByIndex(0, images[0]);
                galleryObject.seek(1);
            }
        },
        setPreSettings: function () {
            var MA = this;

            if (parseInt(this.showCustomOptionTooltip)) {
                $('.truncated').tooltip({
                    track: true,
                    content: function () {
                        return $(this).prop('title');
                    }
                });
            } else {
                $('.field.option-image').children('.label').children('.truncated').remove();
            }

            if (parseInt(this.showOptionsTooltip)) {
                $('.truncated').tooltip({
                    track: true,
                    content: function () {
                        return $(this).prop('title');
                    }
                });
            }

            $(".colorbox").colorbox(
                {
                    rel: 'nofollow'
                }
            );

            //Configure colorbox call back to resize with custom dimensions
            $.colorbox.settings.onLoad = function () {
                colorboxResize();
            }

            //Customize colorbox dimensions
            var colorboxResize = function (resize) {
                var width = "90%";
                var height = "90%";

                if ($(window).width() > 960) {
                    width = "860"
                }
                if ($(window).height() > 700) {
                    height = "630"
                }

                $.colorbox.settings.height = height;
                $.colorbox.settings.width = width;

                //if window is resized while lightbox open
                if (resize) {
                    $.colorbox.resize({
                        'height': height,
                        'width': width
                    });
                }
            }

            //In case of window being resized
            $(window).resize(function () {
                colorboxResize(true);
            });

            $('.picker_main .custom-image-box').click(function () {
                var href = $(this).find('[rel="colorbox"]').attr('href');
                var titles = $(this).find('[rel="colorbox"]').attr('title');
                MA.setMainImage(href, titles);
            });

        },
        loadOptionImages: function () {

            var elm, optionId, parentField, isNewOption, valueId, prevVId, tooltip, MA = this;

            var customOpelements = $('.product-custom-option');

            if (parseInt(this.setaccordion)) {
                customOpelements.closest('form').addClass('setaccordion');
            }

            for (var n = 0; n < customOpelements.length; n++) {
                elm = $(customOpelements[n]);
                var optionIdStartIndex, optionIdEndIndex;
                if (elm.is(":file")) {
                    optionIdStartIndex = elm.attr('name').indexOf('_') + 1;
                    optionIdEndIndex = elm.attr('name').lastIndexOf('_');
                } else {
                    optionIdStartIndex = elm.attr('name').indexOf('[') + 1;
                    optionIdEndIndex = elm.attr('name').indexOf(']');
                }

                optionId = parseInt(elm.attr('name').substring(optionIdStartIndex, optionIdEndIndex), 10);

                if (!this.oldOption[optionId]) {
                    this.oldOption[optionId] = {};
                    parentField = elm[0].type == 'radio' || elm[0].type == 'checkbox' ? elm.closest('.options-list').closest('.field') : elm.closest('.field');
                    isNewOption = true;
                    tooltip = true;
                }

                if (isNewOption) { //call 1st time for all types options

                    parentField.addClass('option-image');

                    if (this.description[optionId] && tooltip) { // add tooltip
                        parentField.children('.label').append(this.tooltipTemplate({
                            description: this.description[optionId]
                        }));
                        tooltip = false;
                    }

                    if (this.classs[optionId]) // add user custom class
                    {
                        parentField.addClass(this.classs[optionId]);
                    }
                    if (this.layout[optionId] && !parentField.hasClass(this.layout[optionId])) // add layout class
                    {
                        parentField.addClass(this.layout[optionId]);
                    }
                }

                switch (elm[0].type) {
                    case "select-one": {
                        if (this.layout[optionId] == 'color_picker' || this.layout[optionId] == 'picker_main') {
                            parentField.find('.control').prepend(this.opImagesBox); // add images box div
                            var options = elm[0].options;
                            for (var i = 0, len = options.length; i < len; ++i) {
                                if (options[i].value) {
                                    valueId = options[i].value;
                                    if (this.thumbnail[valueId]) {

                                        if (parseInt(this.showoption)) {
                                            parentField.addClass('hidepicker');
                                        }

                                        var imageTmplt = this.opImgTplt({
                                            id: 'value_' + valueId,
                                            title: this.title[valueId],
                                            display: 'block',
                                            descptn: this.descptn[valueId],
                                            dataurl: this.fullimage[valueId],
                                            image: this.thumbnail[valueId]
                                        });
                                        if (isNewOption) {
                                            parentField.find('.image-tplt').append(imageTmplt);
                                            isNewOption = false;
                                        } else {
                                            $('#option_image_value_' + prevVId).parent().parent().after(imageTmplt);
                                        }
                                        prevVId = valueId;

                                        if (this.descptn[prevVId]) {
                                            $('#descptn_value_' + prevVId).show();
                                        } else {
                                            $('#descptn_value_' + prevVId).hide();
                                        }

                                        var imgBoxPrt = $('#option_image_value_' + prevVId).closest('.custom-image-box');
                                        var imgBoxClick = $('#option_image_value_' + prevVId);
                                        imgBoxClick.attr('optionId', optionId);
                                        imgBoxClick.attr('vleId', prevVId);
                                        imgBoxClick.parent().parent().removeClass('hide-imgbox');

                                        $(imgBoxPrt).click(function () {

                                            $(this).closest('.image-tplt').children('.active').removeClass('active');
                                            $(this).addClass('active');
                                            var opId = $(this).find('.custom-image img').attr('optionId');
                                            var vleId = $(this).find('.custom-image img').attr('vleId');
                                            var option = $('#select_' + opId).find('option');
                                            for (var i = 0; i < option.length; i++) {
                                                if (option[i].value == vleId) {
                                                    $('#select_' + opId).val(option[i].value).change();
                                                    //option[i].selected = true;
                                                    break;
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                            elm.change($.proxy(this.reloadSelect, this, elm, optionId));
                            this.reloadSelect(elm, optionId);

                        } else {
                            var options = elm[0].options;
                            for (var i = 0, len = options.length; i < len; ++i) {
                                if (options[i].value) {
                                    valueId = options[i].value;
                                    if (this.thumbnail[valueId]) {
                                        this.preloadedImageThumb[valueId] = new Image();
                                        this.preloadedImageThumb[valueId].src = this.thumbnail[valueId];
                                        this.preloadedDescptn[valueId] = this.descptn[valueId];
                                    }
                                }
                            }

                            if (this.layout[optionId] == 'above_option') {

                                parentField.find('.control').prepend(this.opImagesBox); // add images box div
                                parentField.find('.image-tplt').append(this.opImgTplt({
                                    id: optionId,
                                    title: this.title[valueId],
                                    display: 'none',
                                    descptn: this.descptn[valueId],
                                    dataurl: this.fullimage[valueId],
                                    image: this.spacer
                                }));
                            }

                            if (this.layout[optionId] == 'before_option') {
                                parentField.find('.control').prepend(this.opImagesBox); // add images box div
                                parentField.find('.image-tplt').append(this.opImgTplt({
                                    id: optionId,
                                    title: this.title[valueId],
                                    display: 'none',
                                    descptn: this.descptn[valueId],
                                    dataurl: this.fullimage[valueId],
                                    image: this.spacer
                                }));

                                parentField.find('.image-tplt').addClass('left');
                                parentField.find('.control select').addClass('right');
                            }

                            if (this.layout[optionId] == 'below_option') {
                                if (parentField.find('div.control .btnbox').length) {
                                    parentField.find('div.control .btnbox').before(this.opImagesBox);
                                } else {
                                    parentField.find('div.control').append(this.opImagesBox);
                                }

                                parentField.find('.image-tplt').append(this.opImgTplt({
                                    id: optionId,
                                    title: this.title[valueId],
                                    display: 'none',
                                    descptn: this.descptn[valueId],
                                    dataurl: this.fullimage[valueId],
                                    image: this.spacer
                                }));

                            }
                            elm.change($.proxy(this.observeSelectOne, this, elm, optionId));
                            this.observeSelectOne(elm, optionId);
                        }

                        /******/
                        break;
                    }
                    case "radio": {
                        valueId = elm.val();
                        if (elm.attr('value') != '') {
                            if (re_isNewOption) {
                                isNewOption = true;
                                re_isNewOption = false;
                            }

                            if (this.thumbnail[valueId]) {
                                this.preloadedImageThumb[valueId] = new Image();
                                this.preloadedImageThumb[valueId].src = this.thumbnail[valueId];
                            }

                            if (this.descptn[valueId]) {
                                this.preloadedDescptn[valueId] = this.descptn[valueId];
                            }

                            if (this.layout[optionId] == 'grid' || this.layout[optionId] == 'list') {
                                if (this.thumbnail[valueId]) {
                                    elm.parent().prepend(this.opImagesBox); // add images box div
                                    elm.parent().find('.image-tplt').append(this.opImgTplt({
                                        id: 'value_' + valueId,
                                        title: this.title[valueId],
                                        display: 'block',
                                        descptn: this.descptn[valueId],
                                        dataurl: this.fullimage[valueId],
                                        image: this.thumbnail[valueId]
                                    }));

                                    if (this.descptn[valueId]) {
                                        $('#descptn_value_' + valueId).show();
                                    } else {
                                        $('#descptn_value_' + valueId).hide();
                                    }

                                    $('#option_image_value_' + valueId).parent().parent().removeClass('hide-imgbox');
                                    $('#option_image_value_' + valueId).closest('.field').click(function () {
                                        $(this).find('[type="radio"]').prop("checked", true);
                                        $(this).find('[type="radio"]').change();
                                        $(this).parent().find('.active').removeClass('active');
                                        $(this).addClass('active');
                                    });
                                }
                                isNewOption = false;
                            } else {

                                if (isNewOption) {

                                    if (this.layout[optionId] == 'above_option') {
                                        parentField.find('.control').prepend(this.opImagesBox); // add images box div
                                        parentField.find('.image-tplt').append(this.opImgTplt({
                                            id: optionId,
                                            display: 'none',
                                            title: this.title[valueId],
                                            descptn: this.descptn[valueId],
                                            dataurl: this.fullimage[valueId],
                                            image: this.spacer
                                        }));
                                    }

                                    if (this.layout[optionId] == 'before_option') {
                                        parentField.find('.control').prepend(this.opImagesBox); // add images box div
                                        parentField.find('.image-tplt').append(this.opImgTplt({
                                            id: optionId,
                                            display: 'none',
                                            title: this.title[valueId],
                                            descptn: this.descptn[valueId],
                                            dataurl: this.fullimage[valueId],
                                            image: this.spacer
                                        }));
                                        parentField.find('.image-tplt').addClass('left');
                                        parentField.find('.control .options-list').addClass('right');
                                    }

                                    if (this.layout[optionId] == 'below_option' && this.thumbnail[valueId]) {
                                        if (parentField.find('div.control .btnbox').length) {
                                            parentField.find('div.control .btnbox').before(this.opImagesBox);
                                        } else {
                                            parentField.find('div.control').append(this.opImagesBox);
                                        }
                                        parentField.find('.image-tplt').append(this.opImgTplt({
                                            id: optionId,
                                            title: this.title[valueId],
                                            display: 'none',
                                            descptn: this.descptn[valueId],
                                            dataurl: this.fullimage[valueId],
                                            image: this.spacer
                                        }));
                                    }
                                    isNewOption = false;
                                }
                                elm.click($.proxy(this.observeRadio, this, elm, optionId, valueId));
                            }
                        } else { // if radio button not required in admin will show none
                            elm.click($.proxy(this.observeRadio, this, elm, optionId, valueId));
                            isNewOption = false;
                            var re_isNewOption = true;
                        }

                        if (elm[0].checked) {
                            elm.trigger('click');
                        }

                        /******/
                        break;
                    }
                    case "checkbox": {
                        valueId = elm.val();
                        if (this.thumbnail[valueId]) {

                            if (this.layout[optionId] == 'grid' || this.layout[optionId] == 'list') {

                                elm.parent().prepend(this.opImagesBox); // add images box div
                                elm.parent().find('.image-tplt').append(this.opImgTplt({
                                    id: 'value_' + valueId,
                                    title: this.title[valueId],
                                    display: 'block',
                                    descptn: this.descptn[valueId],
                                    dataurl: this.fullimage[valueId],
                                    image: this.thumbnail[valueId]
                                }));


                                if (this.descptn[valueId]) {
                                    $('#descptn_value_' + valueId).show();
                                } else {
                                    $('#descptn_value_' + valueId).hide();
                                }

                                /* $('#option_image_value_' + valueId).parent().parent().removeClass('hide-imgbox');
                                 $('#option_image_value_' + valueId).click(function(){
                                 var checkBoxes = $(this).closest('.field').find('[type="checkbox"]');
                                 checkBoxes.prop("checked", !checkBoxes.prop("checked"));
                                 checkBoxes.change();
                                 }); */

                                $('#option_image_value_' + valueId).parent().parent().removeClass('hide-imgbox');
                                $('#option_image_value_' + valueId).closest('.field').click(function (e) {
                                    console.log('in');

                                    var checkBoxes = $(this).find('[type="checkbox"]');
                                    checkBoxes.prop("checked", !checkBoxes.prop("checked"));

                                    if (checkBoxes.is(':checked')) {
                                        $(this).addClass('active');
                                    } else {
                                        $(this).removeClass('active');
                                    }

                                });

                                isNewOption = false;

                            } else {

                                if (this.layout[optionId] == 'above_option') {
                                    parentField.find('.control').prepend(this.opImagesBox); // add images box div
                                    var imageTmplt = this.opImgTplt({
                                        id: 'value_' + valueId,
                                        title: this.title[valueId],
                                        display: 'none',
                                        descptn: this.descptn[valueId],
                                        dataurl: this.fullimage[valueId],
                                        image: this.thumbnail[valueId]
                                    });
                                }

                                if (this.layout[optionId] == 'below_option') {
                                    if (parentField.find('div.control .btnbox').length) {
                                        parentField.find('div.control .btnbox').before(this.opImagesBox);
                                    } else {
                                        parentField.find('div.control').append(this.opImagesBox);
                                    }
                                    var imageTmplt = this.opImgTplt({
                                        id: 'value_' + valueId,
                                        title: this.title[valueId],
                                        display: 'none',
                                        descptn: this.descptn[valueId],
                                        dataurl: this.fullimage[valueId],
                                        image: this.thumbnail[valueId]
                                    });
                                }

                                if (isNewOption) {
                                    parentField.find('.image-tplt').append(imageTmplt);
                                    isNewOption = false;
                                } else {
                                    $('#option_image_value_' + prevVId).parent().parent().after(imageTmplt);
                                }
                                prevVId = valueId;
                                elm.click($.proxy(this.observeCheckbox, this, elm, valueId));
                            }
                        }

                        if (elm[0].checked) {
                            this.observeCheckbox(elm, valueId);
                        }

                        /******/
                        break;
                    }
                    case "select-multiple": {

                        var options = elm[0].options;
                        for (var i = 0, len = options.length; i < len; ++i) {
                            if (options[i].value) {
                                valueId = options[i].value;
                                if (this.thumbnail[valueId]) {
                                    var imageHtml = this.opImgTplt({
                                        id: 'value_' + valueId,
                                        title: this.title[valueId],
                                        display: 'none',
                                        descptn: this.descptn[valueId],
                                        dataurl: this.fullimage[valueId],
                                        image: this.thumbnail[valueId]
                                    });

                                    if (isNewOption) {

                                        if (this.layout[optionId] == 'above_option') {
                                            parentField.find('.control').prepend(this.opImagesBox);
                                        }

                                        if (this.layout[optionId] == 'below_option') {
                                            if (parentField.find('div.control .btnbox').length) {
                                                parentField.find('div.control .btnbox').before(this.opImagesBox);
                                            } else {
                                                parentField.find('div.control').append(this.opImagesBox);
                                            }
                                        }

                                        parentField.addClass('option-image');
                                        parentField.addClass(this.classs[optionId]);

                                        parentField.find('.image-tplt').append(imageHtml);
                                        isNewOption = false;
                                    } else {
                                        $('#option_image_value_' + prevVId).parent().parent().after(imageHtml);
                                    }
                                    prevVId = valueId;
                                }
                            }
                        }
                        elm.change($.proxy(this.observeSelectMultiple, this, elm));
                        this.observeSelectMultiple(elm);

                        /******/
                        break;
                    }
                } // end switch
            }
            ;
        },
        reloadSelect: function (element, optionId) {
            var valueId = element.val();
            if (valueId == '' || !this.thumbnail[valueId]) {
                element.parent().parent().find('.active').removeClass('active');
            } else {
                $('#option_image_value_' + valueId).closest('.image-tplt').children('.active').removeClass('active');
                $('#option_image_value_' + valueId).parent().parent().addClass('active');
            }
        },

        observeRadio: function (element, optionId, valueId) {
            this.updateImage(optionId, valueId);
        },

        observeCheckbox: function (element, valueId) {
            if (element[0].checked) {
                if (this.descptn[valueId]) {
                    $('#descptn_value_' + valueId).show();
                } else {
                    $('#descptn_value_' + valueId).hide();
                }
                $('#option_image_value_' + valueId).show();
                $('#option_image_value_' + valueId).parent().parent().addClass('show-imgbox');
                $('#option_image_value_' + valueId).parent().parent().removeClass('hide-imgbox');
            } else {
                $('#option_image_value_' + valueId).hide();
                $('#option_image_value_' + valueId).parent().parent().addClass('hide-imgbox');
                $('#option_image_value_' + valueId).parent().parent().removeClass('show-imgbox');
            }
        },

        observeSelectOne: function (element, optionId) {
            var valueId = element.val();
            this.updateImage(optionId, valueId);
        },

        observeSelectMultiple: function (element) {
            var vId;
            var options = element[0].options;
            for (var i = 0; i < options.length; ++i) {
                if (options[i].value) {
                    vId = options[i].value;
                    if (options[i].selected) {
                        if (this.descptn[vId]) {
                            $('#descptn_value_' + vId).show();
                        } else {
                            $('#descptn_value_' + vId).hide();
                        }
                        $('#option_image_value_' + vId).show();
                        $('#option_image_value_' + vId).parent().parent().addClass('show-imgbox');
                        $('#option_image_value_' + vId).parent().parent().removeClass('hide-imgbox');
                    } else {
                        $('#option_image_value_' + vId).hide();
                        $('#option_image_value_' + vId).parent().parent().addClass('hide-imgbox');
                        $('#option_image_value_' + vId).parent().parent().removeClass('show-imgbox');
                    }
                }
            }
        },

        updateImage: function (optionId, valueId) {
            var image = $('#option_image_' + optionId);
            if (image.length == 0)
                return;

            if (valueId != '' && this.thumbnail[valueId]) {
                image[0].src = this.preloadedImageThumb[valueId].src;
                image.show();
                image.parent().parent().addClass('show-imgbox');
                image.parent().parent().removeClass('hide-imgbox');

                var full_image = $('#full_image_' + optionId);
                full_image.attr('href', this.fullimage[valueId]);
                full_image.attr('title', this.descptn[valueId]);

                var op_title_ = $('#op_title_' + optionId);
                op_title_.text(this.title[valueId]);

                var op_descptn = $('#descptn_' + optionId);
                if (this.descptn[valueId]) {
                    op_descptn.attr('title', this.descptn[valueId]);
                    op_descptn.show();
                } else {
                    op_descptn.attr('title', '');
                    op_descptn.hide();
                }


            } else {
                image.parent().parent().addClass('hide-imgbox');
                image.parent().parent().removeClass('show-imgbox');
                image.hide();
            }
        },

    });
    return $.mage.optionImages;
});
