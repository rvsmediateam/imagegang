define([
    'jquery',
    'mage/template',
    'jquery/ui'
], function($, nbImageTplt, modal) {
	
	"use strict";
    $.widget('mage.maAccordion', {
		oldOption: [],
		validElements: [],
		_create: function() {
			this.maButtonBox = nbImageTplt('[data-template=magearray-button-box]');
			this.setAccordion();
			this.onAddToCartButton();
		},
		setAccordion: function() {
			var firstElm, lastElm;
			var optionElements = $('.product-custom-option');
			for (var n = 0; n < optionElements.length; n++) {
				
				var elm = $(optionElements[n]);	
                var optionId = parseInt(elm.attr('name').match(/\d+/)[0]);
			
				if (!this.oldOption[optionId]) 
				{
					this.oldOption[optionId] = {};
					var parentField = elm[0].type == 'radio' || elm[0].type == 'checkbox' ? elm.closest('.options-list').closest('.field') : elm.closest('.field');
					var isNewOption = true;
				}
					
				if(n == 0) // first element
				{ firstElm = true; } else { firstElm = false;}
				
				if(n == parseInt(optionElements.length - 1)) // last element
				{ lastElm = true; } else { lastElm = false;}
				
				if($(elm).is(':radio, :checkbox'))
				{
					if (isNewOption) {
						this.firstFunction(elm, parentField, optionId, firstElm);
						isNewOption = false;
					}
					elm.click($.proxy(this.nextVisibal, this, parentField, '', ''));
				} else {
					if (isNewOption) {
						
						this.firstFunction(elm, parentField, optionId, firstElm);
						
						isNewOption = false;
						elm.change($.proxy(this.nextVisibal, this, parentField, '', ''));
						if(elm[0].type == 'textarea')
						{
							//elm.click($.proxy(this.nextVisibal, this, parentField, '', ''));
						}
					}
				}
				
				if(firstElm) { $('#prev-elm-'+optionId).hide(); }
				if(lastElm) { $('#next-elm-'+optionId).hide(); }
			}
		},
		firstFunction: function(elm, parentField, optionId, firstElm) {
			var maTitle = parentField.children('label');
			maTitle.addClass('ma-title');
			
			var divControl = parentField.children('div.control');
			divControl.addClass('ma-contain');
			divControl.append(this.maButtonBox({opId : optionId}));
			
			if(elm[0].type == 'file') { maTitle.removeAttr('for'); }
			
			if(!firstElm)
			{
				divControl.hide();
			}
			
			var prevEmlBtn = $('#prev-elm-'+optionId);
			prevEmlBtn.click($.proxy(this.goPrevElement, this, parentField));
			
			var nextEmlBtn = $('#next-elm-'+optionId);
			nextEmlBtn.click($.proxy(this.goNextElement, this, elm, parentField));
			
			maTitle.click($.proxy(this.onClickEvent, this, maTitle));
			this.validElements.push(parentField);			
		},
		goPrevElement: function(element) {
			$.each(element.prevAll(), function(index, element)
			{ 
				if($(element).css("display") === "block")
				{ 
					$(element).children('label').trigger("click");
					return false;
				}
			});
		},
		goNextElement: function(elm, parentField) {
			if(elm.closest('form').valid())
			{
				this.nextVisibal(parentField, '', 'display');
			}
		},
		onClickEvent: function(maTitle) {
			var elmt = maTitle.parent().find('div.control');
			if(elmt.css("display") !== "block")
			{
				maTitle.parent().parent().find('div.control').slideUp(); // hide all elements
				elmt.slideDown();
				this.canShowMyNextButton(elmt.parent());
			}
		},
		/* nextVisibalDiv: function() {
			var MA = this;
			$.each(MA.validElements, function(index, element)
			{
				var parentElm = element[0].type == 'radio' || element[0].type == 'checkbox' ? element.closest('.options-list').closest('.field') : element.closest('.field');
				if($(parentElm).css("display") === "block")
				{
					MA.nextVisibal(parentElm,'', '');
				}
			});
		}, */
		prevVisibal: function(element) {
			var prevElm = element.prevAll();
			if(prevElm.length === 0)
			{
				return false;
			}
			return true;
		},
		nextVisibal: function(parentElm, nullData, display = '') {
			
			var MA = this;
			var anyElement = false;
			var nextBtnlg = false;
			
			$.each(parentElm.nextAll(), function(index, element)
			{ 
				if($(element).css("display") == "block")
				{ 
					if(display == 'display') // call goNextElement function
					{ // if find any next element. it will show
						anyElement = element;
					} 
					
					nextBtnlg = $(element);
					
					return false;
				}
			});
			
			if(anyElement)
			{ // if get any next element. it will show
				display = false;
				$(anyElement).children('label').trigger("click");
				
					MA.canShowMyNextButton(anyElement);
				
			}
			
			if(nextBtnlg.length)
			{ // if get any next element. it will show
				$(parentElm).find('.btnbox .action-next').show();
			} else {
				$(parentElm).find('.btnbox .action-next').hide();
			}
		},
		canShowMyNextButton: function(parentElm) {
			//parentElm = parentElm.prev();
			
			var anyNextOne = false; 
			$.each($(parentElm).nextAll(), function(index, element)
			{
				if($(element).css("display") === "block")
				{
					anyNextOne = true;
					return false;
				} else {
					anyNextOne = false; 
				}
			});
			
			if(anyNextOne)
			{
				$(parentElm).find('.btnbox .action-next').show();
			} else {
				$(parentElm).find('.btnbox .action-next').hide();
			}
		},
		returnNextVisibal: function(parentElm) {
			var MA = this;
			var noAny = false;
			
			$.each(parentElm.nextAll(), function(index, element)
			{
				if($(element).css("display") === "block")
				{
					noAny = element;
					return false;
				} 
			});
			
			if(noAny)
			{
				parentElm.find('.btnbox').show();
			} else {
				parentElm.find('.btnbox').hide();
			}
		},
		onAddToCartButton: function(maTitle) {
			var MA = this;
			var canAddToCart = true;
			
			$('#product-addtocart-button').click(function(event){
				$.each(MA.validElements, function(index, element)
				{	
					element.parent().find('div.control').hide();
					element.find('div.control').show();
					if(element.closest('form').valid())
					{
						if(parseInt(MA.validElements.length - 1) != index)
						{
							element.find('div.control').hide();
						}
						canAddToCart = true;
					} else {
						if(element.css("display") !== "block")
						{
							element.parent().find('div.control').slideUp(); // hide all elements
						}
						element.find('div.control').slideDown();
						canAddToCart = false;
						return false;
					}
				});
				
				if(!canAddToCart)
				{
					event.preventDefault();
				}
			});
		}
	});
    return $.mage.maAccordion;
});
