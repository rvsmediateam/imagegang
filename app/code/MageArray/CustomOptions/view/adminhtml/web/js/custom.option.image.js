function uploadoptionimage(data, imgId) {
	if (data.files && data.files[0])
	{
		require(['jquery'], function($) {

			var removeImgElmt = document.getElementById(imgId.replace("logicId", "removeimage"));
			var opImage = document.getElementById(imgId);
			var hiddenElmt = imgId.replace("logicId", "hiddeninput");
			var fileInput = document.getElementById(hiddenElmt);
			
			var dataForm = new FormData();
			dataForm.append("form_key", window.FORM_KEY);
			dataForm.append("image", data.files[0]);
			
			$.ajax({
				url: window.GALLERY_URL,
				dataType: 'JSON',
				cache: false,
				contentType: false,
				processData: false,
				data: dataForm,
				type: 'post',
				showLoader: true,
				success: function(imgdata) {
					console.log(imgdata);
					if (!imgdata.error) {
						removeImgElmt.show();
						opImage.show();
						opImage.src = imgdata.url;
						fileInput.value = imgdata.file;
						$('#' + hiddenElmt).focus();
					} else {
						removeImgElmt.hide();
						opImage.hide();
						opImage.src = "";
						$('<div />').html(imgdata.error).modal({
							title: 'Something Went Wrong.',
							autoOpen: true,
							closed: function() {},
							buttons: [{
								text: 'Ok',
								attr: {
									'data-action': 'ok'
								},
								'class': 'action-primary'
							}]
						});
					}
				}
			});
		});
	}
}

function deleteimage(id) 
{
	require(['jquery'], function($) {
		$('#removeimage-' + id).hide();
		$('#logicId-' + id).hide();
		$('#logicId-' + id).attr('src','');
		$('#hiddeninput-' + id).val('');
		$('#hiddeninput-' + id).focus();
	});
}