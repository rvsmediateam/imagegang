/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/ui-select'
], function ($, _, registry, UiSelect) {
    'use strict';

    return UiSelect.extend({
        defaults: {
            previousGroup: null,
            groupsConfig: {},
            valuesMap: {},
            indexesMap: {},
            filterPlaceholder: 'ns = ${ $.ns }, parentScope = ${ $.parentScope }'
        },

        /**
         * Initialize component.
         * @returns {Element}
         */
        initialize: function () {
			
            return this
                ._super()
                .initMapping()
                .updateComponents(this.initialValue, true);
        },

        /**
         * Create additional mappings.
         *
         * @returns {Element}
         */
        initMapping: function () {
            _.each(this.groupsConfig, function (groupData, group) {
                _.each(groupData.values, function (value) {
                    this.valuesMap[value] = group;
                }, this);

                _.each(groupData.indexes, function (index) {
                    if (!this.indexesMap[index]) {
                        this.indexesMap[index] = [];
                    }

                    this.indexesMap[index].push(group);
                }, this);
            }, this);

            return this;
        },

        /**
         * Callback that fires when 'value' property is updated.
         *
         * @param {String} currentValue
         * @returns {*}
         */
        onUpdate: function (currentValue) {
            this.onUpdateChangeLayout(currentValue);
            this.updateComponents(currentValue);
            return this._super();
        },

        /**
         * Show, hide or clear components based on the current type value.
         *
         * @param {String} currentValue
         * @param {Boolean} isInitialization
         * @returns {Element}
         */
        updateComponents: function (currentValue, isInitialization) {
            var currentGroup = this.valuesMap[currentValue];

            if (currentGroup !== this.previousGroup) {
                _.each(this.indexesMap, function (groups, index) {
                    var template = this.filterPlaceholder + ', index = ' + index,
                        visible = groups.indexOf(currentGroup) !== -1,
                        component;

                    switch (index) {
                        case 'container_type_static':
                        case 'values':
                            template = 'ns=' + this.ns +
                                ', dataScope=' + this.parentScope +
                                ', index=' + index;
                            break;
                    }

                    /*eslint-disable max-depth */
                    if (isInitialization) {
                        registry.async(template)(
                            function (currentComponent) {
                                currentComponent.visible(visible);
                            }
                        );
                    } else {
                        component = registry.get(template);

                        if (component) {
                            component.visible(visible);

                            /*eslint-disable max-depth */
                            if (_.isFunction(component.clear)) {
                                component.clear();
                            }
                        }
                    }
                }, this);

                this.previousGroup = currentGroup;
            }
			
            return this;
        },
		changeOptionLayou: function (select, disableDption) {
			//select.find("option:first-child").attr("selected", true);
			select.find('option').each(function(){
				$(this).removeAttr('disabled');
				if(jQuery.inArray($(this).attr('value'), disableDption) !== -1)
				{
					$(this).attr('disabled',true);
				}
			});
		},
		onUpdateChangeLayoutOnLoad: function (currentValue, id) {
		
			var select = $('#' + id).closest('fieldset').find('.option_image_layout select');
			switch (currentValue) {
				case 'Drop-down':
					//
						this.otherOptionShow($('#' + id).closest('fieldset'));
						var disableDption = ['grid','list'];
						this.changeOptionLayou(select, disableDption);
					// 
					break;
					
				case 'Radio Buttons':
					//
						this.otherOptionShow($('#' + id).closest('fieldset'));
						var disableDption = ['color_picker', 'picker_main'];
						this.changeOptionLayou(select, disableDption);
					//
					break;
					
				case 'Checkbox':
					//
						this.otherOptionShow($('#' + id).closest('fieldset'));
						var disableDption = ['before_option', 'color_picker', 'picker_main'];
						this.changeOptionLayou(select, disableDption);
					//
					break;
					
				case 'Multiple Select':
					//
						this.otherOptionShow($('#' + id).closest('fieldset'));
						var disableDption = ['before_option', 'color_picker', 'picker_main', 'grid','list'];
						this.changeOptionLayou(select, disableDption);
						break;
					//
				default:
					this.otherOptionHide($('#' + id).closest('fieldset'));
			}
		},
		otherOptionShow: function (el) {
			el.find('.option_image_layout').css('opacity', '1');
			
			//el.find('.option_description').css('opacity', '1');
			
			//el.find('.css_class').css('opacity', '1');
			
			el.find('.image_width').css('opacity', '1');
			
			el.find('.image_height').css('opacity', '1');
		},
		otherOptionHide: function (el) {
			
			el.find('.option_image_layout').css('opacity', '0');
			//el.find('.option_image_layout select').val('');
			
			/* el.find('.option_description').css('opacity', '0');
			el.find('.option_description textarea').val(''); */
			
			/* el.find('.css_class').css('opacity', '0');
			el.find('.css_class input').val(''); */
			
			el.find('.image_width').css('opacity', '0');
			el.find('.image_width input').val('');
			
			el.find('.image_height').css('opacity', '0');
			el.find('.image_height input').val('');
		},
		onUpdateChangeLayout: function (currentValue) {
			var select = $(this.cacheUiSelect).closest('fieldset').find('.option_image_layout select');
			switch (currentValue) {
				case 'drop_down':
					//
						this.otherOptionShow($(this.cacheUiSelect).closest('fieldset'));
						var disableDption = ['grid','list'];
						this.changeOptionLayou(select, disableDption);
					// 
					break;
					
				case 'radio':
					//
						this.otherOptionShow($(this.cacheUiSelect).closest('fieldset'));
						var disableDption = ['color_picker', 'picker_main'];
						this.changeOptionLayou(select, disableDption);
					//
					break;
					
				case 'checkbox':
					//
						this.otherOptionShow($(this.cacheUiSelect).closest('fieldset'));
						var disableDption = ['before_option', 'color_picker', 'picker_main'];
						this.changeOptionLayou(select, disableDption);
					//
					break;
					
				case 'multiple':
					//
						this.otherOptionShow($(this.cacheUiSelect).closest('fieldset'));
						var disableDption = ['before_option', 'color_picker', 'picker_main', 'grid','list'];
						this.changeOptionLayou(select, disableDption);
						break;
					//
				default:
					this.otherOptionHide($(this.cacheUiSelect).closest('fieldset'));
			}
        }
    });
});
