define(['underscore', 'mageUtils', 'uiLayout', 'uiElement', 'Magento_Ui/js/lib/validation/validator'], function(_, utils, layout, Element, validator) {
    'use strict';
	
	return Element.extend({
        defaults: {
            visible: true,
            preview: '',
            focused: false,
            required: false,
            disabled: false,
            valueChangedByUser: false,
            elementTmpl: 'MageArray_CustomOptions/form/element/upload-option-image',
            tooltipTpl: 'ui/form/element/helper/tooltip',
            fallbackResetTpl: 'ui/form/element/helper/fallback-reset',
            'input_type': 'input',
            placeholder: '',
            description: '',
            labelVisible: true,
            label: '',
            error: '',
            warn: '',
            notice: '',
            customScope: '',
            default: '',
            isDifferedFromDefault: false,
            showFallbackReset: false,
            additionalClasses: {},
            isUseDefault: '',
            valueUpdate: false,
            switcherConfig: {
                component: 'Magento_Ui/js/form/switcher',
                name: '${ $.name }_switcher',
                target: '${ $.name }',
                property: 'value'
            },
            listens: {
                visible: 'setPreview',
                value: 'setDifferedFromDefault',
                '${ $.provider }:data.reset': 'reset',
                '${ $.provider }:data.overload': 'overload',
                '${ $.provider }:${ $.customScope ? $.customScope + "." : ""}data.validate': 'validate',
                'isUseDefault': 'toggleUseDefault'
            },
            links: {
                //value: '${ $.provider }:${ $.dataScope }'
				value: ''
            }
        },
        initialize: function() {
            _.bindAll(this, 'reset');
            this._super().setInitialValue()._setClasses().initSwitcher();
			this._super().initFormId();
            return this;
        },
		initFormId: function() {
            var namespace;
            if (this.formId) {
                return this;
            }
            namespace = this.name.split('.');
            this.formId = namespace[0];
            return this;
        },
        initObservable: function() {
            var rules = this.validation = this.validation || {};
            this._super();
            this.observe('error disabled focused preview logicId visible value warn isDifferedFromDefault').observe('isUseDefault').observe({
                'required': !!rules['required-entry']
            });
            return this;
        },
        initConfig: function() {
            var uid = utils.uniqueid(),
                optid, name, valueUpdate, scope;
            this._super();
            scope = this.dataScope;
            name = scope.split('.').slice(1);
            valueUpdate = this.showFallbackReset ? 'afterkeydown' : this.valueUpdate;
            _.extend(this, {
                uid: uid,
                optid: "uploadoptionimage(this , 'logicId-" + name[2] + "-" + name[4] + "')",
                noticeId: 'notice-' + uid,
                inputName: utils.serializeName(name.join('.')),
                valueUpdate: valueUpdate
            });
            return this;
        },
        initSwitcher: function() {
            if (this.switcherConfig.enabled) {
                layout([this.switcherConfig]);
            }
            return this;
        },
        setInitialValue: function() {
            this.initialValue = this.getInitialValue();
            if (this.value.peek() !== this.initialValue) {
                this.value(this.initialValue);
            }
            this.on('value', this.onUpdate.bind(this));
            this.isUseDefault(this.disabled());
            return this;
        },
        _setClasses: function() {
            var additional = this.additionalClasses,
                classes;
            if (_.isString(additional) && additional.trim().length) {
                additional = this.additionalClasses.trim().split(' ');
                classes = this.additionalClasses = {};
                additional.forEach(function(name) {
                    classes[name] = true;
                }, this);
            }
            _.extend(this.additionalClasses, {
                _required: this.required,
                _error: this.error,
                _warn: this.warn,
                _disabled: this.disabled
            });
            return this;
        },
        getInitialValue: function() {
            var values = [this.value(), this.default],
                value;
            values.some(function(v) {
                if (v !== null && v !== undefined) {
                    value = v;
                    return true;
                }
                return false;
            });
            return this.normalizeData(value);
        },
        setVisible: function(isVisible) {
            this.visible(isVisible);
            return this;
        },
        show: function() {
            this.visible(true);
            return this;
        },
        hide: function() {
            this.visible(false);
            return this;
        },
        disable: function() {
            this.disabled(true);
            return this;
        },
        enable: function() {
            this.disabled(false);
            return this;
        },
        setValidation: function(rule, options) {
            var rules = utils.copy(this.validation),
                changed;
            if (_.isObject(rule)) {
                _.extend(this.validation, rule);
            } else {
                this.validation[rule] = options;
            }
            changed = utils.compare(rules, this.validation).equal;
            if (changed) {
                this.required(!!rules['required-entry']);
                this.validate();
            }
            return this;
        },
        getPreview: function() {
            return this.value();
        },
        hasAddons: function() {
            return this.addbefore || this.addafter;
        },
        hasService: function() {
            return this.service && this.service.template;
        },
        hasChanged: function() {
            var notEqual = this.value() !== this.initialValue;
            return !this.visible() ? false : notEqual;
        },
        hasData: function() {
            return !utils.isEmpty(this.value());
        },
        reset: function() {
            this.value(this.initialValue);
            this.error(false);
            return this;
        },
        overload: function() {
            this.setInitialValue();
            this.bubble('update', this.hasChanged());
        },
        clear: function() {
            this.value('');
            return this;
        },
        normalizeData: function(value) {
            return utils.isEmpty(value) ? '' : value;
        },
        validate: function() {
            var value = this.value(),
                result = validator(this.validation, value, this.validationParams),
                message = !this.disabled() && this.visible() ? result.message : '',
                isValid = this.disabled() || !this.visible() || result.passed;
            this.error(message);
            this.bubble('error', message);
            if (!isValid) {
                this.source.set('params.invalid', true);
            }
            return {
                valid: isValid,
                target: this
            };
        },
        onUpdate: function() {
            this.bubble('update', this.hasChanged());
            this.validate();
        },
        restoreToDefault: function() {
            this.value(this.default);
        },
        setDifferedFromDefault: function() {
            var value = typeof this.value() != 'undefined' && this.value() !== null ? this.value() : '',
                defaultValue = typeof this.default != 'undefined' && this.default !== null ? this.default : '';
            this.isDifferedFromDefault(value !== defaultValue);
        },
        toggleUseDefault: function(state) {
            this.disabled(state);
        },
        userChanges: function() {
            this.valueChangedByUser = true;
        }
    });
});
