/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            'Magento_Catalog/js/custom-options-type':'MageArray_CustomOptions/js/custom-options-type'
        }
    }
};
