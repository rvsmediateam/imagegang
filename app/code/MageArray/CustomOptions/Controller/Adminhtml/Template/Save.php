<?php

namespace MageArray\CustomOptions\Controller\Adminhtml\Template;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use MageArray\CustomOptions\Model\Template as TemplateModel;
use MageArray\CustomOptions\Model\Template\Product as TemplateProduct;
use MageArray\CustomOptions\Model\Template\Option as TemplateOption;
use MageArray\CustomOptions\Model\Template\OptionFactory as TemplateOptionFactory;
use MageArray\CustomOptions\Model\Template\Option\TypeFactory as TemplateOptionTypeFactory;
use MageArray\CustomOptions\Helper\Data as DataHelper;
use Magento\Backend\Helper\Js as JsHelper;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\Product\OptionFactory;
use Magento\Framework\App\ResourceConnection;


/**
 * Class Save
 * @package MageArray\CustomOptions\Controller\Adminhtml\Template
 */
class Save extends Action
{
    /**
     * @var TemplateModel
     */
    protected $_templateModel;
    /**
     * @var TemplateProduct
     */
    protected $_templateProduct;
    /**
     * @var TemplateOptionFactory
     */
    protected $_templateOptionFactory;
    /**
     * @var TemplateOptionTypeFactory
     */
    protected $_templateOptionTypeFactory;

    /**
     * @var DataHelper
     */
    protected $_dataHelper;
    /**
     * @var JsHelper
     */
    protected $_jsHelper;
    /**
     * @var ProductFactory
     */
    protected $_productFactory;
    /**
     * @var OptionFactory
     */
    protected $_productOptionFactory;

    /**
     * @var ResourceConnection
     */
    protected $_resources;

    /**
     * @var
     */
    protected $_template;
    /**
     * @var
     */
    protected $_postData;
    /**
     * @var
     */
    protected $_images;

    /**
     * @var
     */
    protected $_productCollection;
    /**
     * @var array
     */
    protected $_productCustomOptions = [];

    /**
     * @var array
     */
    protected $_changeProductIds = [];

    /**
     * @var array
     */
    protected $_maxRowIdOfProduct = [];

    /**
     * @var bool
     */
    protected $_hasCommonChanges = false;

    public function __construct(
        Context $context,
        TemplateModel $templateModel,
        TemplateProduct $templateProduct,
        TemplateOptionFactory $templateOptionFactory,
        TemplateOptionTypeFactory $templateOptionTypeFactory,
        DataHelper $dataHelper,
        JsHelper $jsHelper,
        ProductFactory $productFactory,
        OptionFactory $optionFactory,
        ResourceConnection $resourceConnection
    ) {
        parent::__construct($context);
        $this->_templateModel = $templateModel;
        $this->_templateProduct = $templateProduct;
        $this->_templateOptionFactory = $templateOptionFactory;
        $this->_templateOptionTypeFactory = $templateOptionTypeFactory;
        $this->_dataHelper = $dataHelper;
        $this->_jsHelper = $jsHelper;
        $this->_productFactory = $productFactory;
        $this->_productOptionFactory = $optionFactory;
        $this->_resources = $resourceConnection;
    }

    /**
     * @return bool
     */
    public function hasDependent()
    {
        return $this->_dataHelper->isDependantModuleEnable();
    }

    /**
     * @return $this
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            try {
                $templateModel = $this->_templateModel;

                $id = $this->getRequest()->getParam('template_id');
                if ($id) {
                    $templateModel->load($id);
                    if (!$templateModel->getId()) {
                        throw new \RuntimeException(
                            'Template no longer exists.');
                    }
                }
                $this->_images = $this->_dataHelper
                    ->prepareImages($this->getRequest()->getFiles());

                $templateModel->setData($data);
                $templateModel->save();

                $this->_template = $templateModel;
                $this->_postData = $data;

                $this->saveProducts();

                $this->prepareProductCollection();

                $this->saveCustomOptions();
                $this->saveProductOptions();

                $this->messageManager
                    ->addSuccess(__('Template saved successfully.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')
                    ->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit',
                        [
                            'template_id' => $templateModel->getId(),
                            '_current' => true
                        ]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError(__($e->getMessage()));
            } catch (\RuntimeException $e) {
                $this->messageManager->addError(__($e->getMessage()));
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __(
                    'Something went wrong while saving the options template.'));
                $this->messageManager->addException($e, __($e->getMessage()));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit',
                [
                    'template_id' =>
                        $this->getRequest()->getParam('template_id')
                ]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     *
     */
    public function saveProducts()
    {
        $postData = $this->_postData;
        if (isset($postData['products'])) {
            $productIds = $this->_jsHelper
                ->decodeGridSerializedInput($postData['products']);
            $oldProducts = (array)$this->_template->getProductIds();
            $newProducts = (array)$productIds;

            $table = $this->_templateProduct->getResource()->getMainTable();

            $connection = $this->_templateProduct->getResource()
                ->getConnection();

            $insert = array_diff($newProducts, $oldProducts);
            $delete = array_diff($oldProducts, $newProducts);

            if ($delete) {
                $where = [
                    'template_id = ?' => (int)$this->_template->getId(),
                    'product_id IN (?)' => $delete
                ];
                $connection->delete($table, $where);
            }

            if ($insert) {
                $data = [];
                foreach ($insert as $productId) {
                    $this->_changeProductIds[] = $productId;
                    $data[] = [
                        'template_id' => (int)$this->_template->getId(),
                        'product_id' => (int)$productId
                    ];
                }
                $connection->insertMultiple($table, $data);
            }
        }
    }

    /**
     *
     */
    public function saveCustomOptions()
    {
        $postData = $this->_postData;
        if (isset($postData['product']['options'])) {
            $options = $postData['product']['options'];
            $hasExtraElements = $this->_dataHelper->enableExtraElements();

            foreach ($options as $optionIndex => $_option) {
                /* Save option values */

                if (!isset($_option['option_id'])
                    || !$_option['option_id']
                    || $_option['option_id'] <= 0
                ) {
                    $_option['option_id'] = null;
                    $this->_hasCommonChanges = true;
                }
                $templateOption = $this->_templateOptionFactory
                    ->create()
                    ->load($_option['option_id']);

                if (isset($_option['is_delete']) && $_option['is_delete']) {
                    if ($_option['option_id']) {
                        $templateOption->load($_option['option_id']);
                        if ($templateOption->getId()) {
                            if ($this->_template
                                ->getData('can_option_delete_from_product')
                            ) {
                                $this->_removeProductCustomOption(
                                    $templateOption);
                                $this->_hasCommonChanges = true;
                            }
                            $templateOption->delete();
                        }
                    }
                    continue;
                }

                $templateOption->setData($_option)
                    ->setTemplateId($this->_template->getTemplateId());

                if (!$this->_hasCommonChanges) {
                    $this->_hasCommonChanges =
                        $templateOption->haveAnyChanges();
                }
                if ($this->_hasCommonChanges) {
                    $templateOption->save();
                }
                $this->_updateProductCustomOption($templateOption);

                /* Save option values */

                /* Save option type values */
                if ($templateOption->getGroupByType($templateOption->getType())
                    == TemplateOption::OPTION_GROUP_SELECT
                ) {
                    if (isset($_option['values'])
                        && is_array($_option['values'])
                    ) {
                        foreach ($_option['values']
                                 as $valueIndex => $_optionType) {
                            if (!isset($_optionType['option_type_id'])
                                || !$_optionType['option_type_id']
                                || $_optionType['option_type_id'] <= 0
                            ) {
                                $_optionType['option_type_id'] = null;
                            }
                            $templateOptionType = $this->_templateOptionTypeFactory
                                ->create()
                                ->load($_optionType['option_type_id']);

                            if (isset($_optionType['is_delete'])
                                && $_optionType['is_delete']
                            ) {
                                if ($_optionType['option_type_id']) {
                                    if ($templateOptionType->getId()) {
                                        $this->_removeProductCustomOptionValue(
                                            $templateOptionType,
                                            $templateOption);
                                        $this->_hasCommonChanges = true;
                                        $templateOptionType->delete();
                                    }
                                }
                                continue;
                            }

                            if ($hasExtraElements && isset($this->_images[$optionIndex][$valueIndex])) {
                                $fileName = $this->_dataHelper
                                    ->uploadImage($this->_images[$optionIndex]
                                    [$valueIndex]);
                                if ($fileName) {
                                    if ($templateOptionType->getStoreImage()) {
                                        $this->_dataHelper
                                            ->deleteFile($templateOptionType
                                                ->getStoreImage());
                                    }
                                    $_optionType['store_image'] = $fileName;
                                }
                            }

                            if ($hasExtraElements
                                && isset($_optionType['remove_image'])
                                && $_optionType['remove_image']
                            ) {
                                $_optionType['store_image'] = '';
                                if ($templateOptionType->getStoreImage()) {
                                    $this->_dataHelper
                                        ->deleteFile($templateOptionType
                                            ->getStoreImage());
                                }
                            }

                            $templateOptionType->setData($_optionType)
                                ->setOptionId($templateOption->getId());
                            if (!$this->_hasCommonChanges) {
                                $this->_hasCommonChanges =
                                    $templateOptionType->haveAnyChanges();
                            }
                            if ($this->_hasCommonChanges) {
                                $templateOptionType->save();
                            }
                            $this->_updateProductCustomOptionValue(
                                $templateOptionType, $templateOption);
                        }
                    }
                } else {
                    $values = $templateOption->getValues();
                    if ($values && $values->getSize()) {
                        foreach ($values as $_value) {
                            $this->_removeProductCustomOptionValue($_value,
                                $templateOption);
                            $_value->delete();
                        }
                    }
                }
                /* Save option type values */
            }
        }
    }

    /**
     *
     */
    public function saveProductOptions()
    {
        if ($this->_productCustomOptions
            && ($this->_hasCommonChanges
                || $this->_changeProductIds)
        ) {
            foreach ($this->_productCollection as $product) {
                if ($this->_hasCommonChanges == false
                    && !in_array($product->getId(), $this->_changeProductIds)
                ) {
                    continue;
                }
                if (isset($this->_productCustomOptions[$product->getId()])) {
                    $productOptions = $this->_productOptionFactory
                        ->create()
                        ->getProductOptions($product);
                    if ($productOptions) {
                        foreach ($productOptions as $_removeOption) {
                            $_removeOption->delete();
                        }
                    }

                    foreach ($this->_productCustomOptions[$product->getId()]
                             as $customOptionData) {
                        $customOptionData = $this->_dataHelper
                            ->updateProductOptionImages($customOptionData);
                        $this->_productOptionFactory
                            ->create()
                            ->setData($customOptionData)
                            ->setOptionId(null)
                            ->setProductId($product->getId())
                            ->setProductSku($product->getSku())
                            ->save();
                    }
                }
            }
            $connection = $this->_resources->getConnection();
            $productTable = $this->_resources->getTableName(
                'catalog_product_entity');
            $productIds = implode("','",
                array_keys($this->_productCustomOptions));
            $sql = "Update $productTable set `has_options`='1',`required_options`='1' WHERE entity_id IN('$productIds')";
            $connection->query($sql);
        }
    }

    /**
     *
     */
    public function prepareProductCollection()
    {
        $productIds = $this->_template->getProductIds(true);
        if ($productIds) {
            $this->_productCollection = $this->_productFactory
                ->create()
                ->getCollection()
                ->addAttributeToSelect('*')
                ->addIdFilter($productIds);
        }
    }

    /**
     * @param $product
     * @return mixed
     */
    protected function _getCustomOptionsByProduct($product)
    {
        if (!isset($this->_productCustomOptions[$product->getId()])) {
            $this->_productCustomOptions[$product->getId()] = [];
            $maxRowId = 0;
            $options = $this->_productOptionFactory
                ->create()
                ->getProductOptionCollection($product);
            $templateOptionObject = $this->_templateOptionFactory->create();
            foreach ($options as $_option) {
                if ($_option->getRowId() > $maxRowId) {
                    $maxRowId = $_option->getRowId();
                }
                $optionData = $_option->getData();
                if ($templateOptionObject->getGroupByType($_option->getType())
                    == TemplateOption::OPTION_GROUP_SELECT
                ) {
                    $values = $_option->getValuesCollection()->getData();
                    foreach ($values as $_value) {
                        if (!isset($_value['price'])
                            || empty($_value['price'])
                        ) {
                            $_value['price'] = 0;
                        }
                        if (!isset($_value['price_type'])
                            || empty($_value['price_type'])
                        ) {
                            $_value['price_type'] = 'fixed';
                        }
                        if (isset($_value['row_id'])
                            && $_value['row_id']
                            && $_value['row_id'] > $maxRowId
                        ) {
                            $maxRowId = $_value['row_id'];
                        }
                        $optionData['values'][] = $_value;
                    }
                }
                $this->_productCustomOptions[$product->getId()][] = $optionData;
            }
            $this->_maxRowIdOfProduct[$product->getId()] = $maxRowId;
        }
        return $this->_productCustomOptions[$product->getId()];
    }

    /**
     * @param $templateOption
     */
    protected function _updateProductCustomOption($templateOption)
    {
        if ($this->_productCollection) {
            foreach ($this->_productCollection as $_product) {
                $options = $this->_getCustomOptionsByProduct($_product);
                $update = false;
                $lastSortOrder = 1;
                $productId = $_product->getId();
                foreach ($options as $key => $_option) {
                    if ($_option['sort_order'] >= $lastSortOrder) {
                        $lastSortOrder = $_option['sort_order'] + 1;
                    }
                    if (!$_option[DataHelper::OPTION_FIELD]
                        || $_option[DataHelper::OPTION_FIELD]
                        != $templateOption->getIdentifier()
                    ) {
                        continue;
                    }

                    $this->_productCustomOptions[$productId][$key] =
                        array_merge($_option, $templateOption->getMergeData());

                    $rowId = $this->_getRowId($productId,
                        $this->_productCustomOptions[$productId][$key]);
                    if ($rowId) {
                        $this->_productCustomOptions[$productId][$key]
                        ['row_id'] = $rowId;
                    }

                    $update = true;
                }
                if (!$update) {

                    $data = $templateOption->getMergeData();
                    $rowId = $this->_getRowId($productId, $data);
                    if ($rowId) {
                        $data['row_id'] = $rowId;
                    }
                    $data['sort_order'] = $lastSortOrder;
                    $this->_productCustomOptions[$productId][] = $data;
                }
            }
        }
    }

    /**
     * @param $optionValue
     * @param $templateOption
     */
    protected function _updateProductCustomOptionValue($optionValue, $templateOption)
    {
        if ($this->_productCollection) {
            foreach ($this->_productCollection as $_product) {
                $productId = $_product->getId();
                $options = $this->_getCustomOptionsByProduct($_product);

                foreach ($options as $key => $_option) {
                    if (!$_option[DataHelper::OPTION_FIELD]
                        || $_option[DataHelper::OPTION_FIELD]
                        != $templateOption->getIdentifier()
                    ) {
                        continue;
                    }
                    if (!isset($_option['values'])) {
                        $_option['values'] = [];
                    }

                    $update = false;
                    $lastSortOrder = 1;
                    foreach ($_option['values'] as $typeKey => $_typeValue) {
                        if ($_typeValue['sort_order'] >= $lastSortOrder) {
                            $lastSortOrder = $_typeValue['sort_order'] + 1;
                        }
                        if (!$_typeValue[DataHelper::OPTION_FIELD]
                            || $_typeValue[DataHelper::OPTION_FIELD]
                            != $optionValue->getIdentifier()
                        ) {
                            continue;
                        }

                        $this->_productCustomOptions[$productId][$key]
                        ['values'][$typeKey] = array_merge($_typeValue,
                            $optionValue->getMergeData());

                        $rowId = $this->_getRowId($productId,
                            $this->_productCustomOptions[$productId][$key]
                            ['values'][$typeKey]);
                        if ($rowId) {
                            $this->_productCustomOptions[$productId][$key]
                            ['values'][$typeKey]['row_id'] = $rowId;
                        }

                        $dependentOptions = $this->_prepareDependentOption(
                            $productId,
                            $this->_productCustomOptions[$productId][$key]
                            ['values'][$typeKey]);

                        if ($dependentOptions) {
                            $this->_productCustomOptions[$productId][$key]
                            ['values'][$typeKey]
                            ['dependent_options'] = $dependentOptions;
                        }

                        $update = true;
                    }
                    if (!$update) {
                        $data = $optionValue->getMergeData();
                        $rowId = $this->_getRowId($productId, $data);
                        if ($rowId) {
                            $data['row_id'] = $rowId;
                        }

                        $dependentOptions = $this->_prepareDependentOption(
                            $productId, $data);

                        if ($dependentOptions) {
                            $data['dependent_options'] = $dependentOptions;
                        }

                        $data['sort_order'] = $lastSortOrder;
                        $this->_productCustomOptions[$productId][$key]
                        ['values'][] = $data;
                    }
                }
            }
        }
    }

    /**
     * @param $templateOption
     */
    protected function _removeProductCustomOption($templateOption)
    {
        if ($this->_productCollection) {
            foreach ($this->_productCollection as $_product) {
                $options = $this->_getCustomOptionsByProduct($_product);
                foreach ($options as $key => $_option) {
                    if (!$_option[DataHelper::OPTION_FIELD]
                        || $_option[DataHelper::OPTION_FIELD]
                        != $templateOption->getIdentifier()
                    ) {
                        continue;
                    }
                    unset($this->_productCustomOptions[$_product->getId()]
                        [$key]);
                }
            }
        }
    }

    /**
     * @param $optionValue
     * @param $templateOption
     */
    protected function _removeProductCustomOptionValue($optionValue, $templateOption)
    {
        if ($this->_productCollection) {
            foreach ($this->_productCollection as $_product) {
                $options = $this->_getCustomOptionsByProduct($_product);
                foreach ($options as $key => $_option) {
                    if (!$_option[DataHelper::OPTION_FIELD]
                        || $_option[DataHelper::OPTION_FIELD]
                        != $templateOption->getIdentifier()
                    ) {
                        continue;
                    }
                    if (!isset($_option['values'])) {
                        $_option['values'] = [];
                    }

                    foreach ($_option['values'] as $typeKey => $_typeValue) {
                        if (!$_typeValue[DataHelper::OPTION_FIELD]
                            || $_typeValue[DataHelper::OPTION_FIELD]
                            != $optionValue->getIdentifier()
                        ) {
                            continue;
                        }
                        unset($this->_productCustomOptions[$_product->getId()]
                            [$key]['values'][$typeKey]);
                    }
                }
            }
        }
    }

    /**
     * @param $productId
     * @param $options
     * @return bool
     */
    protected function _getRowId($productId, $options)
    {
        $rowId = false;
        if ($this->hasDependent()
            && isset($options['row_id'])
            && $options['row_id']
            && isset($this->_maxRowIdOfProduct[$productId])
            && $this->_maxRowIdOfProduct[$productId]
        ) {
            $rowId = $options['row_id'] + $this->_maxRowIdOfProduct[$productId];
        }
        return $rowId;
    }

    /**
     * @param $productId
     * @param $option
     * @return string
     */
    protected function _prepareDependentOption($productId, $option)
    {
        $selectedOptions = [];
        if ($this->hasDependent()
            && isset($option['dependent_options'])
            && $option['dependent_options']
            && isset($this->_maxRowIdOfProduct[$productId])
            && $this->_maxRowIdOfProduct[$productId]
        ) {
            $selectedOptions = array_map('trim',
                explode(',', $option['dependent_options']));
            foreach ($selectedOptions as $key => $value) {
                $selectedOptions[$key] =
                    $value + $this->_maxRowIdOfProduct[$productId];
            }
        }
        return implode(',', $selectedOptions);
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization
            ->isAllowed('Magento_Catalog::ma_option_templates');
    }
}
