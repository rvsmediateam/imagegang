<?php
namespace MageArray\CustomOptions\Controller\Adminhtml\Template;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use MageArray\CustomOptions\Model\Template;

/**
 * Class Edit
 * @package MageArray\CustomOptions\Controller\Adminhtml\Template
 */
class Edit extends Action
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var Template
     */
    protected $_templateModel;

    /**
     * Edit constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param Template $templateModel
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        Template $templateModel
    ) {
        parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
        $this->_templateModel = $templateModel;
    }

    /**
     *
     */
    public function execute()
    {
        try {
            $id = $this->getRequest()->getParam('template_id', 0);
            $template = $this->_templateModel->load($id);

            if ($id && !$template->getId()) {
                throw new \Exception('Template no longer exists.');
            }

            $this->_coreRegistry->register('current_template', $template);
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
            $this->_redirect('*/*/');
            return;
        }
        $this->_view->loadLayout();
        $this->_view->getPage()
            ->setActiveMenu('Magento_Catalog::ma_option_templates');

        $this->_view->getPage()
            ->addBreadcrumb(__('Edit Template'),
                __('Edit Template'));

        $title = "New Template";
        if ($template->getId()) {
            $title = __("Edit Template: %1", $template->getTitle());
        }
        $this->_view->getPage()
            ->getConfig()
            ->getTitle()
            ->prepend(__($title));
        $this->_view->renderLayout();
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization
            ->isAllowed('Magento_Catalog::ma_option_templates');
    }
}
