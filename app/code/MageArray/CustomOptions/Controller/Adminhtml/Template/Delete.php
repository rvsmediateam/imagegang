<?php
namespace MageArray\CustomOptions\Controller\Adminhtml\Template;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use MageArray\CustomOptions\Model\Template;

/**
 * Class Delete
 * @package MageArray\CustomOptions\Controller\Adminhtml\Template
 */
class Delete extends Action
{
    /**
     * @var Template
     */
    protected $_templateModel;

    /**
     * Edit constructor.
     * @param Context $context
     * @param Template $templateModel
     */
    public function __construct(
        Context $context,
        Template $templateModel
    ) {
        parent::__construct($context);
        $this->_templateModel = $templateModel;
    }

    /**
     *
     */
    public function execute()
    {
        try {
            $id = $this->getRequest()->getParam('template_id', 0);
            $template = $this->_templateModel->load($id);

            if (!$id || !$template->getId()) {
                throw new \Exception('Template no longer exists.');
            }

            $template->delete();
            $this->messageManager->addSuccess(
                __('Custom option template deleted successfully.'));
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        $this->_redirect('*/*/');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization
            ->isAllowed('Magento_Catalog::ma_option_templates');
    }
}
