<?php

namespace MageArray\CustomOptions\Controller\Adminhtml\Template;

use Magento\Backend\App\Action;

/**
 * Class NewAction
 * @package MageArray\CustomOptions\Controller\Adminhtml\Template
 */
class NewAction extends Action
{
    /**
     *
     */
    public function execute()
    {
        $this->_forward('edit');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization
            ->isAllowed('Magento_Catalog::ma_option_templates');
    }
}
