<?php

namespace MageArray\CustomOptions\Controller\Adminhtml\Template;

use Magento\Backend\App\Action;

/**
 * Class Index
 * @package MageArray\CustomOptions\Controller\Adminhtml\Template
 */
class Index extends Action
{
    /**
     *
     */
    public function execute()
    {
        $this->_view
            ->loadLayout()
            ->getPage()
            ->setActiveMenu('Magento_Catalog::catalog_products');

        $this->_view->getPage()
            ->getConfig()
            ->getTitle()
            ->prepend(__('Manage Option Templates'));
        $this->_view->renderLayout();
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization
            ->isAllowed('Magento_Catalog::ma_option_templates');
    }
}
