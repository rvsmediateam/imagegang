<?php
namespace MageArray\CustomOptions\Controller\Adminhtml\Gallery;

use Magento\Framework\App\Action\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Image\AdapterFactory;
use Magento\Catalog\Model\Product\Media\Config;

/**
 * Class Index
 * @package MageArray\CustomOptions\Controller\Adminhtml\Gallery
 */
class Index extends Action
{
    /**
     * @var AdapterFactory
     */
    protected $adapterFactory;

    /**
     * @var Config
     */
    protected $mediaConfig;

    /**
     * Index constructor.
     * @param Context $context
     * @param AdapterFactory $adapterFactory
     * @param Config $mediaConfig
     */
    public function __construct(
        Context $context,
        AdapterFactory $adapterFactory,
        Config $mediaConfig
    ) {
        parent::__construct($context);
        $this->adapterFactory = $adapterFactory;
        $this->mediaConfig = $mediaConfig;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Raw
     */
    public function execute()
    {
        try {
            $uploader = $this->_objectManager->create(
                'Magento\MediaStorage\Model\File\Uploader',
                ['fileId' => 'image']
            );
            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);

            $imageAdapter = $this->adapterFactory->create();
            $uploader->addValidateCallback('catalog_product_image',
                $imageAdapter, 'validateUploadFile');
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(true);

            $mediaDirectory = $this->_objectManager
                ->get('Magento\Framework\Filesystem')
                ->getDirectoryRead(DirectoryList::MEDIA);

            $result = $uploader->save($mediaDirectory
                ->getAbsolutePath($this->mediaConfig
                    ->getBaseMediaPath()));

            $this->_eventManager->dispatch(
                'catalog_product_gallery_upload_image_after',
                ['result' => $result, 'action' => $this]
            );

            unset($result['tmp_name']);
            unset($result['path']);

            $result['url'] = $this->mediaConfig->getMediaUrl($result['file']);
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage()];
        }

        $this->_response
            ->representJson(json_encode($result));
    }
}
