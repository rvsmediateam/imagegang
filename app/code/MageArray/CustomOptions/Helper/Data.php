<?php
namespace MageArray\CustomOptions\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Backend\Model\UrlInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\View\Element\Template\Context as TemplateContext;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\File\Uploader;
use MageArray\CustomOptions\Model\TemplateFactory;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Data
 * @package MageArray\CustomOptions\Helper
 */
class Data extends AbstractHelper
{
    /**
     *
     */
    const XML_ENABLE_EXTRA_ELEMENTS = 'ma_custom_options/general/enable';
    /**
     *
     */
    const SHOW_CUSTOM_OPTION_TOOLTIP = 'ma_custom_options/general/show_custom_option_tooltip';
    /**
     *
     */
    const SHOW_OPTIONS_TOOLTIP = 'ma_custom_options/general/show_options_tooltip';
    /**
     *
     */
    const POPUP_OPTION_IMAGE = 'ma_custom_options/general/popup_option_image';
    /**
     *
     */
    const XML_HIDE_COLOR_PICKER_LAYOUT = 'ma_custom_options/general/showoption';
    /**
     *
     */
    const XML_ACCORDION_ENABLE = 'ma_custom_options/general/accordion';

    /**
     *
     */
    const OPTION_FIELD = 'ma_custom_option';

    /**
     *
     */
    const IMAGE_DIR = 'customOptions';

    /**
     * @var UrlInterface
     */
    protected $_backendUrl;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var UploaderFactory
     */
    protected $_uploaderFactory;
    /**
     * @var Filesystem\Directory\ReadInterface
     */
    protected $_mediaDirPath;
    /**
     * @var Filesystem\Directory\WriteInterface
     */
    protected $_mediaDirectory;
    /**
     * @var TemplateFactory
     */
    protected $_templateFactory;

    /**
     * @var Dependant
     */
    protected $_dependantHelper;

    /**
     * Data constructor.
     * @param Context $context
     * @param Dependant $dependantHelper
     * @param UrlInterface $backendUrl
     * @param TemplateContext $templateContext
     * @param UploaderFactory $uploaderFactory
     * @param TemplateFactory $templateFactory
     */
    public function __construct(
        Context $context,
        Dependant $dependantHelper,
        UrlInterface $backendUrl,
        TemplateContext $templateContext,
        UploaderFactory $uploaderFactory,
        TemplateFactory $templateFactory
    ) {
        parent::__construct($context);
        $this->_dependantHelper = $dependantHelper;
        $this->_backendUrl = $backendUrl;
        $this->_storeManager = $templateContext->getStoreManager();
        $this->_uploaderFactory = $uploaderFactory;
        $this->_mediaDirPath = $templateContext->getFilesystem()->getDirectoryRead(
            DirectoryList::MEDIA);
        $this->_mediaDirectory = $templateContext->getFilesystem()->getDirectoryWrite(
            DirectoryList::MEDIA);
        $this->_templateFactory = $templateFactory;
    }

    /**
     * @return bool
     */
    public function isDependantModuleEnable()
    {
        return $this->_dependantHelper->isEnabled();
    }

    /**
     * @param $storePath
     * @return mixed
     */
    public function getStoreConfig($storePath)
    {
        return $this->scopeConfig->getValue($storePath,
            ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function enableExtraElements()
    {
        return $this->getStoreConfig(self::XML_ENABLE_EXTRA_ELEMENTS);
    }

    /**
     * @return mixed
     */
    public function showCustomOptionTooltip()
    {
        return $this->getStoreConfig(self::SHOW_CUSTOM_OPTION_TOOLTIP);
    }

    /**
     * @return mixed
     */
    public function showOptionsTooltip()
    {
        return $this->getStoreConfig(self::SHOW_OPTIONS_TOOLTIP);
    }

    /**
     * @return mixed
     */
    public function popupOptionImage()
    {
        return $this->getStoreConfig(self::POPUP_OPTION_IMAGE);
    }

    /**
     * @return mixed
     */
    public function hideColorPickerLayout()
    {
        return $this->getStoreConfig(self::XML_HIDE_COLOR_PICKER_LAYOUT);
    }

    /**
     * @return mixed
     */
    public function enableAccordion()
    {
        return $this->getStoreConfig(self::XML_ACCORDION_ENABLE);
    }

    /**
     * get products tab Url in admin
     * @return string
     */
    public function getProductsGridUrl()
    {
        return $this->_backendUrl->getUrl('ma_custom_option/template/products',
            ['_current' => true]);
    }

    /**
     * @return string
     */
    public function getImageDirName()
    {
        return self::IMAGE_DIR;
    }

    /**
     * @return string
     */
    public function getMediaDir()
    {
        return $this->_mediaDirPath->getAbsolutePath(self::getImageDirName());
    }

    /**
     * @param $fileName
     * @return string
     */
    protected function _filePath($fileName)
    {
        return self::getMediaDir() . '/' . $fileName;
    }

    /**
     * @param $fileName
     * @return bool
     */
    public function isFileExist($fileName)
    {
        $result = false;
        if ($fileName) {
            $result = file_exists(self::_filePath($fileName));
        }
        return $result;
    }

    /**
     * @param $fileName
     * @return null|string
     */
    public function getImageUrl($fileName)
    {
        $imageUrl = null;
        if (self::isFileExist($fileName)) {
            $imageUrl = self::getMediaUrl() .
                self::getImageDirName() . '/' . $fileName;
        }
        return $imageUrl;
    }

    /**
     * @return mixed
     */
    public function getMediaUrl()
    {
        return $this->_storeManager->getStore()
            ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    /**
     * @param $fileName
     * @return bool
     */
    public function deleteFile($fileName)
    {
        $result = false;
        if (self::isFileExist($fileName)) {
            @unlink(self::_filePath($fileName));
            $result = true;
        }
        return $result;
    }

    /**
     * @return string
     */
    public function getCatalogMediaUrl()
    {
        return $this->getMediaUrl() . 'catalog/product';
    }

    /**
     * @param $postData
     * @return array
     */
    public function prepareImages($postData)
    {
        $files = [];

        if (isset($postData['product']['options'])) {
            $data = $postData['product']['options'];
            foreach ($data as $optionId => $_data) {
                if (isset($_data['values']) && is_array($_data['values'])) {
                    foreach ($_data['values'] as $valueId => $image) {
                        $file = isset($image['image']) ? $image['image'] : null;
                        if ($file && is_array($file)
                            && isset($file['tmp_name'])
                            && !empty($file['tmp_name'])
                        ) {
                            $files[$optionId][$valueId] = $file;
                        }

                    }
                }
            }
        }
        return $files;
    }

    /**
     * @param $file
     * @return null|string
     */
    public function uploadImage($file)
    {
        $fileName = null;
        try {
            if ($file && is_array($file)
                && isset($file['tmp_name'])
                && !empty($file['tmp_name'])
            ) {
                $uploader = $this->_uploaderFactory
                    ->create(['fileId' => $file]);
                $uploader->setAllowedExtensions(
                    ['jpg', 'jpeg', 'gif', 'png']);
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(false);
                $uploader->save($this->getMediaDir());
                $fileName = $uploader->getUploadedFileName();
            }
        } catch (\Exception $e) {
        }
        return $fileName;
    }

    /**
     * @param $productOption
     * @return mixed
     */
    public function updateProductOptionImages($productOption)
    {
        if (isset($productOption['values'])) {
            foreach ($productOption['values'] as &$_value) {
                if (isset($_value['store_image']) && $_value['store_image']) {
                    if ($this->enableExtraElements()) {
                        if (count(explode('/', $_value['store_image'])) > 1) {
                            continue;
                        }
                        $fileName = $this->_copyImageForProduct(
                            $_value['store_image']);
                        if ($fileName) {
                            $_value['store_image'] = $fileName;
                        }
                    } else {
                        $_value['store_image'] = null;
                    }

                }
            }
        }
        return $productOption;
    }

    /**
     * @param $templateImage
     * @return string
     */
    protected function _copyImageForProduct($templateImage)
    {
        try {
            $mediaDir = $this->_mediaDirPath->getAbsolutePath();
            $discretionPath = Uploader::getDispretionPath($templateImage);
            $newFileName = Uploader::getNewFileName($mediaDir .
                'catalog/product/' . $discretionPath . '/' . $templateImage);
            $fileName = $discretionPath . '/' . $newFileName;
            $source = self::IMAGE_DIR . '/' . $templateImage;
            $destination = 'catalog/product' . $fileName;
            $this->_mediaDirectory->copyFile($source, $destination);
            return $fileName;
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param $fileName
     * @return $this
     */
    public function removeProductImage($fileName)
    {
        if ($fileName) {
            $mediaDir = $this->_mediaDirPath->getAbsolutePath();
            @unlink($mediaDir . 'catalog/product' . $fileName);
        }
        return $this;
    }

    /**
     * @return array
     */
    public function getDisableTemplateIds()
    {
        $collection = $this->_templateFactory
            ->create()
            ->getCollection()
            ->addFieldToFilter('is_active', 0);
        return $collection->getColumnValues('template_id');
    }
}
