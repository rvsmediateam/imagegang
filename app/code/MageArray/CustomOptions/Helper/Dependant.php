<?php
namespace MageArray\CustomOptions\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ObjectManager;

/**
 * Class Dependant
 * @package MageArray\CustomOptions\Helper
 */
class Dependant extends AbstractHelper
{
    /**
     * @var ModuleManager
     */
    protected $_moduleManager;
    /**
     * @var ObjectManager
     */
    protected $_objectManager;

    /**
     * Dependant constructor.
     * @param Context $context
     * @param ModuleManager $moduleManager
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
        $this->_moduleManager = $context->getModuleManager();
        $this->_objectManager = ObjectManager::getInstance();
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        $result = false;
        if ($this->_moduleManager->isEnabled('MageArray_DependentOptions')) {
            $result = $this->_objectManager
                ->get('MageArray\DependentOptions\Helper\Data')
                ->isEnabled();
        }
        return $result;
    }
}
