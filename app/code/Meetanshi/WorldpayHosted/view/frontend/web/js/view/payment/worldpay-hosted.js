/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component,
              rendererList) {
        'use strict';

        rendererList.push(
            {
                type: 'worldpay_hosted',
                component: 'Meetanshi_WorldpayHosted/js/view/payment/method-renderer/worldpay-payments'
            }
        );

        /** Add view logic here if needed */
        return Component.extend({});
    }
);
