<?php

namespace Meetanshi\WorldpayHosted\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Store\Model\StoreManagerInterface;
use Meetanshi\WorldpayHosted\Helper\Data;

class WorldpayHostedConfigProvider implements ConfigProviderInterface
{
    protected $helper;
    protected $storeManager;

    protected $methodCodes = ['worldpay_hosted'];
    protected $methods = [];

    public function __construct(Data $helper, PaymentHelper $paymentHelper, StoreManagerInterface $storeManager)
    {
        $this->helper = $helper;
        $this->storeManager = $storeManager;
        foreach ($this->methodCodes as $code) {
            $this->methods[$code] = $paymentHelper->getMethodInstance($code);
        }
    }

    public function getConfig()
    {
        $redirectUrl = $this->storeManager->getStore()->getBaseUrl() . 'worldpay_hosted/payment/redirect';
        $showLogo = $this->helper->showLogo();
        $imageUrl = $this->helper->getPaymentLogo();

        $config = [];
        $config['payment']['worldpay_hosted_payment']['imageurl'] = ($showLogo) ? $imageUrl : '';
        $config['payment']['worldpay_hosted_payment']['is_active'] = $this->helper->isActive();
        $config['payment']['worldpay_hosted_payment']['payment_instruction'] = trim($this->helper->getPaymentInstructions());
        $config['payment']['worldpay_hosted_payment']['redirect_url'] = $redirectUrl;

        return $config;
    }
}
