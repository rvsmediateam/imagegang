<?php

namespace Meetanshi\WorldpayHosted\Model;

use Meetanshi\WorldpayHosted\Helper\Data as WorldpayHostedHelper;
use Magento\Payment\Model\Method\AbstractMethod;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Payment\Model\Method\Logger;
use Magento\Framework\Module\ModuleListInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Sales\Model\OrderFactory;
use Magento\Framework\Url;
use Magento\Directory\Model\RegionFactory;
use Magento\Directory\Model\CountryFactory;
use Magento\Checkout\Model\Session;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Quote\Api\Data\CartInterface;
use Meetanshi\WorldpayHosted\Block\Payment\Info;
use Magento\Payment\Model\InfoInterface;

class Payment extends AbstractMethod
{
    const CODE = 'worldpay_hosted';

    protected $_code = self::CODE;
    protected $_infoBlockType = Info::class;
    protected $_isGateway = true;
    protected $_canCapture = true;
    protected $_canRefund = true;
    protected $_canAuthorize = true;
    protected $_canUseInternal = false;

    protected $worldpay_hostedHelper;

    public function __construct(Context $context, Registry $registry, ExtensionAttributesFactory $extensionFactory, AttributeValueFactory $customAttributeFactory, PaymentHelper $paymentData, ScopeConfigInterface $scopeConfig, Logger $logger, ModuleListInterface $moduleList, TimezoneInterface $localeDate, OrderFactory $orderFactory, Url $urlBuilder, RegionFactory $region, CountryFactory $country, Session $checkoutSession, StoreManagerInterface $storeManager, WorldpayHostedHelper $worldpay_hostedHelper, AbstractResource $resource = null, AbstractDb $resourceCollection = null, array $data = [])
    {
        $this->urlBuilder = $urlBuilder;
        $this->moduleList = $moduleList;
        $this->scopeConfig = $scopeConfig;
        $this->checkoutSession = $checkoutSession;
        $this->storeManager = $storeManager;
        $this->region = $region;
        $this->country = $country;
        $this->logger = $logger;
        $this->worldpay_hostedHelper = $worldpay_hostedHelper;

        parent::__construct($context, $registry, $extensionFactory, $customAttributeFactory, $paymentData, $scopeConfig, $logger, $resource, $resourceCollection, $data);
    }

    public function initialize($paymentAction, $stateObject)
    {
        return parent::initialize($paymentAction, $stateObject);
    }

    public function isAvailable(CartInterface $quote = null)
    {
//        $available = $this->worldpay_hostedHelper->isPaymentAvailable();
//        if (!$available) {
//            return false;
//        } else {
            return parent::isAvailable($quote);
      //  }
    }

    public function validate()
    {
        return parent::validate();
    }

    public function getOrderPlaceRedirectUrl()
    {
        return $this->urlBuilder->getUrl('worldpay_hosted/payment/redirect', ['_secure' => true]);
    }

    public function authorize(InfoInterface $payment, $amount)
    {
        return parent::authorize($payment, $amount);
    }

    public function capture(InfoInterface $payment, $amount)
    {
        return parent::capture($payment, $amount);
    }

    public function refund(InfoInterface $payment, $amount)
    {
        return parent::refund($payment, $amount);
    }
}
