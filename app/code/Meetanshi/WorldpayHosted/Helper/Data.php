<?php

namespace Meetanshi\WorldpayHosted\Helper;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Session\SessionManager;
use Magento\Framework\View\Asset\Repository;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class Data extends AbstractHelper
{
    const CONFIG_WORLDPAY_ACTIVE = 'payment/worldpay_hosted/active';
    const CONFIG_WORLDPAY_MODE = 'payment/worldpay_hosted/mode';

    const CONFIG_WORLDPAY_LOGO = 'payment/worldpay_hosted/show_logo';

    const CONFIG_WORLDPAY_INSTRUCTIONS = 'payment/worldpay_hosted/instructions';

    const CONFIG_WORLDPAY_SANDBOX_INSTALLATION_ID = 'payment/worldpay_hosted/sandbox_installation_id';
    const CONFIG_WORLDPAY_LIVE_INSTALLATION_ID = 'payment/worldpay_hosted/live_installation_id';

    const CONFIG_WORLDPAY_SANDBOX_GATEWAY_URL = 'payment/worldpay_hosted/sandbox_gateway_url';
    const CONFIG_WORLDPAY_LIVE_GATEWAY_URL = 'payment/worldpay_hosted/live_gateway_url';

    const CONFIG_WORLDPAY_MD5_SECRET = 'payment/worldpay_hosted/md5_secret';

    const CONFIG_WORLDPAY_CAPTURE_DELAY = 'payment/worldpay_hosted/capture_delay';

    const CONFIG_WORLDPAY_DELIVERY_ADDRESS = 'payment/worldpay_hosted/delivery_address';

    const CONFIG_WORLDPAY_FIXED_CONTACT = 'payment/worldpay_hosted/fixed_contact';

    const CONFIG_WORLDPAY_HIDE_CONTACT = 'payment/worldpay_hosted/hide_contact';

    const CONFIG_WORLDPAY_INVOICE = 'payment/worldpay_hosted/allow_invoice';

    protected $directoryList;
    protected $storeManager;
    protected $request;
    protected $encryptor;
    protected $sessionManager;
    protected $checkoutSession;
    private $repository;

    public function __construct(Context $context, EncryptorInterface $encryptor, DirectoryList $directoryList, StoreManagerInterface $storeManager, Http $request, SessionManager $sessionManager, Repository $repository, CheckoutSession $checkoutSession)
    {
        parent::__construct($context);
        $this->encryptor = $encryptor;
        $this->directoryList = $directoryList;
        $this->storeManager = $storeManager;
        $this->request = $request;
        $this->sessionManager = $sessionManager;
        $this->repository = $repository;
        $this->checkoutSession = $checkoutSession;
    }

    public function isAutoInvoice()
    {
        return $this->scopeConfig->getValue(self::CONFIG_WORLDPAY_INVOICE, ScopeInterface::SCOPE_STORE);
    }

    public function isActive()
    {
        return $this->scopeConfig->getValue(self::CONFIG_WORLDPAY_ACTIVE, ScopeInterface::SCOPE_STORE);
    }

    public function getPaymentInstructions()
    {
        return $this->scopeConfig->getValue(self::CONFIG_WORLDPAY_INSTRUCTIONS, ScopeInterface::SCOPE_STORE);
    }

    public function isPaymentAvailable()
    {
        $installationID = trim($this->getInstallationID());
        if (!$installationID) {
            return false;
        }
        return true;
    }

    public function getInstallationID()
    {
        if ($this->getMode()) {
            return $this->encryptor->decrypt($this->scopeConfig->getValue(self::CONFIG_WORLDPAY_SANDBOX_INSTALLATION_ID, ScopeInterface::SCOPE_STORE));
        } else {
            return $this->encryptor->decrypt($this->scopeConfig->getValue(self::CONFIG_WORLDPAY_LIVE_INSTALLATION_ID, ScopeInterface::SCOPE_STORE));
        }
    }

    public function getMode()
    {
        return $this->scopeConfig->getValue(self::CONFIG_WORLDPAY_MODE, ScopeInterface::SCOPE_STORE);
    }

    public function getPaymentForm($order)
    {
        $amount = $order->getBaseGrandTotal();
        $testMode = ($this->getMode() == '1') ? '100' : '0';
        $currencyCode = $order->getBaseCurrencyCode();
        $installationID = $this->getInstallationID();
        $md5Secret = $this->getMD5Secret();
        $cartID = 'World-' . $order->getIncrementId();

        $captureDelay = $this->getCaptureDelay();
        if ($captureDelay == -1) {
            $authMode = "E";
        } else if ($captureDelay <= 14) {
            $authMode = "A";
        } else {
            $authMode = "A";
        }

        $billingAddress = $order->getBillingAddress()->getData();

        $streetData = $billingAddress['street'];
        $streetList = preg_split("/\\r\\n|\\r|\\n/", $streetData);

        $address1 = $streetList[0];

        if (array_key_exists(1, $streetList)) {
            $address2 = $streetList[1];
        } else {
            $address2 = "";
        }

        if (array_key_exists(2, $streetList)) {
            $address3 = $streetList[2];
        } else {
            $address3 = "";
        }

        $shippingAddress = $order->getShippingAddress()->getData();

        $shippingStreetData = $shippingAddress['street'];
        $shippingStreetList = preg_split("/\\r\\n|\\r|\\n/", $shippingStreetData);

        $daddress1 = $shippingStreetList[0];

        if (array_key_exists(1, $shippingStreetList)) {
            $daddress2 = $shippingStreetList[1];
        } else {
            $daddress2 = "";
        }

        if (array_key_exists(2, $shippingStreetList)) {
            $daddress3 = $shippingStreetList[2];
        } else {
            $daddress3 = "";
        }
        $hideContact = 'false';
        $fixContact = 'false';

        if ($this->isHideContact()) {
            $hideContact = 'true';
        }
        if ($this->isFixedContact()) {
            $fixContact = 'true';
        }

        $signatureFields = $currencyCode . ":" . $amount . ":" . $testMode . ":" . $installationID;
        $signature = md5($md5Secret . ":" . $signatureFields);
        //$signature = hash('SHA512', $md5Secret . ":" . $signatureFields);

        $html = "<form id='WorldpayHostedForm' name='worldpayhostedsubmit' action='" . $this->getGatewayUrl() . "' method='POST'>";
        $html .= "<input type='hidden' name='instId' value='" . $installationID . "' />";
        $html .= "<input type='hidden' name='amount' value='" . $amount . "' />";
        $html .= "<input type='hidden' name='cartId' value='" . $cartID . "' />";
        $html .= "<input type='hidden' name='currency' value='" . $currencyCode . "' />";
        $html .= "<input type='hidden' name='testMode' value='" . $testMode . "' />";
        $html .= "<input type='hidden' name='desc' value='" . $this->getPaymentSubject() . "' />";
        $html .= "<input type='hidden' name='authMode' value='" . $authMode . "' />";

        $html .= "<input type='hidden' name='name' value='" . $order->getBillingAddress()->getName() . "' />";
        $html .= "<input type='hidden' name='address1' value='" . $address1 . "' />";
        $html .= "<input type='hidden' name='address2' value='" . $address2 . "' />";
        $html .= "<input type='hidden' name='address3' value='" . $address3 . "' />";
        $html .= "<input type='hidden' name='postcode' value='" . $billingAddress['postcode'] . "' />";
        $html .= "<input type='hidden' name='tel' value='" . $billingAddress['telephone'] . "' />";
        $html .= "<input type='hidden' name='country' value='" . $billingAddress['country_id'] . "' />";
        $html .= "<input type='hidden' name='town' value='" . $billingAddress['city'] . "' />";
        $html .= "<input type='hidden' name='fax' value='" . $billingAddress['fax'] . "' />";
        $html .= "<input type='hidden' name='email' value='" . $order->getCustomerEmail() . "' />";
        if ($this->allowDeliveryAddress()):
            $html .= "<input type='hidden' name='delvName' value='" . $order->getShippingAddress()->getName() . "' />";
            $html .= "<input type='hidden' name='delvAddress1' value='" . $daddress1 . "' />";
            $html .= "<input type='hidden' name='delvAddress2' value='" . $daddress2 . "' />";
            $html .= "<input type='hidden' name='delvAddress3' value='" . $daddress3 . "' />";
            $html .= "<input type='hidden' name='delvTown' value='" . $shippingAddress['city'] . "' />";
            $html .= "<input type='hidden' name='delvPostcode' value='" . $shippingAddress['postcode'] . "' />";
            $html .= "<input type='hidden' name='delvCountry' value='" . $shippingAddress['country_id'] . "' />";
        endif;
        $html .= "<input type='hidden' name='fixContact' value='" . $fixContact . "' />";
        $html .= "<input type='hidden' name='hideContact' value='" . $hideContact . "' />";
        $html .= "<input type='hidden' name='signature' value='" . $signature . "' />";
        $html .= "<input type='hidden' name='MC_callback' value='" . $this->getCallbackUrl() . "' />";
        $html .= "<input type='hidden' name='MC_returnurl' value='" . $this->getReturnUrl() . "' />";
        $html .= "<input type='hidden' name='MC_cancelurl' value='" . $this->getCancelUrl() . "' />";
        $html .= "</form>";

        \Magento\Framework\App\ObjectManager::getInstance()->get('Psr\Log\LoggerInterface')->debug(print_r(['Form Params' => $html], true));

        return $html;
    }

    public function getMD5Secret()
    {
        return $this->encryptor->decrypt($this->scopeConfig->getValue(self::CONFIG_WORLDPAY_MD5_SECRET, ScopeInterface::SCOPE_STORE));
    }

    public function getCaptureDelay()
    {
        return $this->scopeConfig->getValue(self::CONFIG_WORLDPAY_CAPTURE_DELAY, ScopeInterface::SCOPE_STORE);
    }

    public function isHideContact()
    {
        return $this->scopeConfig->getValue(self::CONFIG_WORLDPAY_HIDE_CONTACT, ScopeInterface::SCOPE_STORE);
    }

    public function isFixedContact()
    {
        return $this->scopeConfig->getValue(self::CONFIG_WORLDPAY_FIXED_CONTACT, ScopeInterface::SCOPE_STORE);
    }

    public function getGatewayUrl()
    {
        if ($this->getMode()) {
            return $this->scopeConfig->getValue(self::CONFIG_WORLDPAY_SANDBOX_GATEWAY_URL, ScopeInterface::SCOPE_STORE);
        } else {
            return $this->scopeConfig->getValue(self::CONFIG_WORLDPAY_LIVE_GATEWAY_URL, ScopeInterface::SCOPE_STORE);
        }
    }

    public function allowDeliveryAddress()
    {
        return $this->scopeConfig->getValue(self::CONFIG_WORLDPAY_DELIVERY_ADDRESS, ScopeInterface::SCOPE_STORE);
    }

    public function getCallbackUrl()
    {
        $baseUrl = $this->storeManager->getStore()->getBaseUrl();
        return $baseUrl . "worldpay_hosted/payment/success";
    }

    public function getReturnUrl()
    {
        $baseUrl = $this->storeManager->getStore()->getBaseUrl();
        return $baseUrl . "worldpay_hosted/payment/returns";
    }

    public function getCancelUrl()
    {
        $baseUrl = $this->storeManager->getStore()->getBaseUrl();
        return $baseUrl . "worldpay_hosted/payment/cancel";
    }

    public function getPaymentSubject()
    {
        $subject = trim($this->scopeConfig->getValue('general/store_information/name', ScopeInterface::SCOPE_STORE));
        if (!$subject) {
            return "Magento 2 order";
        }

        return $subject;
    }

    public function showLogo()
    {
        return $this->scopeConfig->getValue(self::CONFIG_WORLDPAY_LOGO, ScopeInterface::SCOPE_STORE);
    }

    public function getPaymentLogo()
    {
        $params = ['_secure' => $this->request->isSecure()];
        return $this->repository->getUrlWithParams('Meetanshi_WorldpayHosted::images/worldpay.png', $params);
    }
}
