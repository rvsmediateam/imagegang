<?php

namespace Meetanshi\WorldpayHosted\Block\Payment;

use Magento\Payment\Block\ConfigurableInfo;

class Info extends ConfigurableInfo
{
    protected $_template = 'Meetanshi_WorldpayHosted::info.phtml';

    public function getLabel($field)
    {
        switch ($field) {
            case 'cardType':
                return __('Card Type');
            case 'transId':
                return __('Transaction ID');
            case 'rawAuthMessage':
                return __('Authentication Message');
            case 'rawAuthCode':
                return __('Authentication Code');
            case 'transStatus':
                return __('Transaction Status');
            default:
                break;
        }
    }
}
