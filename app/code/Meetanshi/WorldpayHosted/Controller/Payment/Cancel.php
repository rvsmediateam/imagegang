<?php

namespace Meetanshi\WorldpayHosted\Controller\Payment;

use Meetanshi\WorldpayHosted\Controller\Payment as WorldpayHostedPayment;

class Cancel extends WorldpayHostedPayment
{
    public function execute()
    {
        $this->messageManager->addErrorMessage('Transaction was not Successful. Your Order was not Completed. Please try again later');
        $this->checkoutSession->restoreQuote();
        return $this->_redirect('checkout/cart');
    }
}
