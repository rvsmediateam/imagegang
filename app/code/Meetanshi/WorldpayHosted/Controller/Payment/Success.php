<?php

namespace Meetanshi\WorldpayHosted\Controller\Payment;

use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Payment\Transaction;
use Meetanshi\WorldpayHosted\Controller\Payment as WorldpayHostedPayment;

class Success extends WorldpayHostedPayment
{
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        \Magento\Framework\App\ObjectManager::getInstance()->get('Psr\Log\LoggerInterface')->debug(print_r(['Success Params' => $params], true));
        if (is_array($params) && !empty($params)) {
            \Magento\Framework\App\ObjectManager::getInstance()->get('Psr\Log\LoggerInterface')->debug("1");
            $rawAuthCode = $params['rawAuthCode'];
            $orderId = explode("-", $params['cartId']);
            $order = $this->orderFactory->create()->loadByIncrementId($orderId['1']);
            $payment = $order->getPayment();
            if($rawAuthCode == 'A')
            {
                \Magento\Framework\App\ObjectManager::getInstance()->get('Psr\Log\LoggerInterface')->debug("2");
                $cardType = $params['cardType'];
                $transactionID = $params['transId'];
                $rawAuthMessage = $params['rawAuthMessage'];
                $rawAuthCode = $params['rawAuthCode'];
                $transStatus = $params['transStatus'];

                $payment->setTransactionId($transactionID);
                $payment->setLastTransId($transactionID);
                $payment->setAdditionalInformation('cardType', $cardType);
                $payment->setAdditionalInformation('transId', $transactionID);
                $payment->setAdditionalInformation('rawAuthMessage', $rawAuthMessage);
                $payment->setAdditionalInformation('rawAuthCode', $rawAuthCode);
                $payment->setAdditionalInformation('transStatus', $transStatus);
                \Magento\Framework\App\ObjectManager::getInstance()->get('Psr\Log\LoggerInterface')->debug("3");
                $payment->setAdditionalInformation((array)$payment->getAdditionalInformation());
                $trans = $this->transactionBuilder;
                $transaction = $trans->setPayment($payment)->setOrder($order)->setTransactionId($transactionID)->setAdditionalInformation((array)$payment->getAdditionalInformation())->setFailSafe(true)->build(Transaction::TYPE_CAPTURE);
                \Magento\Framework\App\ObjectManager::getInstance()->get('Psr\Log\LoggerInterface')->debug("4");
                $payment->addTransactionCommentsToOrder($transaction, 'Transaction is approved by the bank');
                $payment->setParentTransactionId(null);

                $payment->save();
                \Magento\Framework\App\ObjectManager::getInstance()->get('Psr\Log\LoggerInterface')->debug("5");
                $this->orderSender->notify($order);
                \Magento\Framework\App\ObjectManager::getInstance()->get('Psr\Log\LoggerInterface')->debug("6");
                $order->addStatusHistoryComment(__('Transaction is approved by the bank'), Order::STATE_PROCESSING)->setIsCustomerNotified(true);

                $order->save();

                $transaction->save();
                \Magento\Framework\App\ObjectManager::getInstance()->get('Psr\Log\LoggerInterface')->debug("7");
                if ($this->helper->isAutoInvoice()) {
                    \Magento\Framework\App\ObjectManager::getInstance()->get('Psr\Log\LoggerInterface')->debug("8");
                    if (!$order->canInvoice()) {
                        $order->addStatusHistoryComment('Sorry, Order cannot be invoiced.', false);
                    }
                    $invoice = $this->invoiceService->prepareInvoice($order);
                    if (!$invoice) {
                        $order->addStatusHistoryComment('Can\'t generate the invoice right now.', false);
                    }

                    if (!$invoice->getTotalQty()) {
                        $order->addStatusHistoryComment('Can\'t generate an invoice without products.', false);
                    }
                    $invoice->setRequestedCaptureCase(Invoice::CAPTURE_OFFLINE);
                    $invoice->register();
                    $invoice->getOrder()->setCustomerNoteNotify(true);
                    $invoice->getOrder()->setIsInProcess(true);
                    $transactionSave = $this->transactionFactory->create()->addObject($invoice)->addObject($invoice->getOrder());
                    $transactionSave->save();

                    try {
                        $this->invoiceSender->send($invoice);
                    } catch (\Magento\Framework\Exception\LocalizedException $e) {
                        $order->addStatusHistoryComment('Can\'t send the invoice Email right now.', false);
                    }

                    $order->addStatusHistoryComment('Automatically Invoice Generated.', false);
                    $order->save();
                }
                \Magento\Framework\App\ObjectManager::getInstance()->get('Psr\Log\LoggerInterface')->debug("1");
                return $this->_redirect('checkout/onepage/success');
            }

            if($rawAuthCode == 'C')
            {
                \Magento\Framework\App\ObjectManager::getInstance()->get('Psr\Log\LoggerInterface')->debug("9");
                $errorMsg = __('Transaction was not Successful. Your Order was not completed. Please try again later');

                if (array_key_exists('cardType', $params)) {
                    $cardType = $params['cardType'];
                    $payment->setAdditionalInformation('cardType', $cardType);
                }
                if (array_key_exists('transId', $params)) {
                    $transactionID = $params['transId'];
                    $payment->setAdditionalInformation('transId', $transactionID);
                }
                if (array_key_exists('rawAuthMessage', $params)) {
                    $rawAuthMessage = $params['rawAuthMessage'];
                    $payment->setAdditionalInformation('rawAuthMessage', $rawAuthMessage);
                }
                if (array_key_exists('rawAuthCode', $params)) {
                    $rawAuthCode = $params['rawAuthCode'];
                    $payment->setAdditionalInformation('rawAuthCode', $rawAuthCode);
                }
                if (array_key_exists('transStatus', $params)) {
                    $transStatus = $params['transStatus'];
                    $payment->setAdditionalInformation('transStatus', $transStatus);
                }

                $payment->setAdditionalInformation((array)$payment->getAdditionalInformation());

                $order->cancel()->setState(\Magento\Sales\Model\Order::STATE_CANCELED, true, 'Gateway has declined the payment.');
                $payment->setStatus('DECLINED');
                $payment->setShouldCloseParentTransaction(1)->setIsTransactionClosed(1);
                $payment->save();
                $order->setStatus(\Magento\Sales\Model\Order::STATE_CANCELED);
                $order->addStatusToHistory($order->getStatus(), $errorMsg);
                $this->messageManager->addErrorMessage($errorMsg);
                $this->checkoutSession->restoreQuote();
                $order->save();
                return $this->_redirect('checkout/cart');
            }

        }
    }
}
