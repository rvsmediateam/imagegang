<?php

namespace Meetanshi\WorldpayHosted\Controller\Payment;

use Meetanshi\WorldpayHosted\Controller\Payment as WorldpayHostedPayment;
use Magento\Sales\Model\Order;

class Redirect extends WorldpayHostedPayment
{
    public function execute()
    {
        if ($this->getRequest()->isAjax()) {
            $result = $this->jsonFactory->create();
            try {
                $order = $this->checkoutSession->getLastRealOrder();
                $order->setState(Order::STATE_PENDING_PAYMENT, true);
                $order->setStatus(Order::STATE_PENDING_PAYMENT);
                $order->save();
                $html = $this->helper->getPaymentForm($order);
                return $result->setData(['error' => false, 'success' => true, 'html' => $html]);
            } catch (\Exception $e) {
                $this->checkoutSession->restoreQuote();
                return $result->setData(['error' => true, 'success' => false, 'message' => __('Payment exception')]);
            }
        }
        return false;
    }
}
