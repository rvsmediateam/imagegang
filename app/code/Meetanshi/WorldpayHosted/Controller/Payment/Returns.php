<?php

namespace Meetanshi\WorldpayHosted\Controller\Payment;

use Meetanshi\WorldpayHosted\Controller\Payment as WorldpayHostedPayment;

class Returns extends WorldpayHostedPayment
{
    public function execute()
    {
        return $this->_redirect('checkout/onepage/success');
    }
}
