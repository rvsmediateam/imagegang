<?php

namespace Meetanshi\WorldpayHosted\Controller;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Sales\Model\OrderFactory;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Checkout\Helper\Data as CheckoutHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Sales\Model\OrderNotifier;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Framework\DB\TransactionFactory;
use Magento\Framework\App\Request\Http;
use Magento\Sales\Model\Order\Payment\Transaction\Builder;
use Meetanshi\WorldpayHosted\Helper\Data as WorldpayHostedHelper;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Sales\Model\Service\InvoiceService;

abstract class Payment extends Action
{
    protected $customerSession;
    protected $checkoutSession;
    protected $resultJsonFactory;
    protected $orderFactory;
    protected $storeManager;
    protected $worldpay_hostedPayment;
    protected $jsonFactory;
    protected $config;
    protected $logger;
    protected $invoiceSender;
    protected $transactionFactory;
    protected $transactionBuilder;
    protected $request;
    protected $helper;
    protected $orderCollection;
    protected $invoiceService;
    protected $orderSender;

    public function __construct(Context $context, PaymentHelper $paymentHelper, OrderFactory $orderFactory, CheckoutSession $checkoutSession, CheckoutHelper $checkoutData, JsonFactory $resultJsonFactory, OrderNotifier $orderSender, StoreManagerInterface $storeManager, InvoiceSender $invoiceSender, TransactionFactory $transactionFactory, Http $request, Builder $transactionBuilder, WorldpayHostedHelper $helper, InvoiceService $invoiceService, CollectionFactory $orderCollection, $params = [])
    {
        $this->checkoutSession = $checkoutSession;
        $this->orderFactory = $orderFactory;
        $this->orderSender = $orderSender;
        $this->worldpay_hostedPayment = $paymentHelper->getMethodInstance('worldpay_hosted');
        $this->jsonFactory = $resultJsonFactory;
        $this->storeManager = $storeManager;
        $this->request = $request;
        $this->transactionBuilder = $transactionBuilder;
        $this->helper = $helper;
        $this->transactionFactory = $transactionFactory;
        $this->orderCollection = $orderCollection;
        $this->invoiceSender = $invoiceSender;
        $this->invoiceService = $invoiceService;
        parent::__construct($context);
    }
}
