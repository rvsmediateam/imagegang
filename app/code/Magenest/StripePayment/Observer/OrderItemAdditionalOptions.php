<?php
/**
 * Created by Magenest JSC.
 * Author: Jacob
 * Date: 10/01/2019
 * Time: 9:41
 */

namespace Magenest\StripePayment\Observer;

use Magento\Framework\Event\ObserverInterface;

class OrderItemAdditionalOptions implements ObserverInterface
{
    protected $stripeHelper;
    protected $customerSession;

    public function __construct(
        \Magenest\StripePayment\Helper\Data $stripeHelper,
        \Magento\Customer\Model\Session $customerSession
    ) {

        $this->stripeHelper = $stripeHelper;
        $this->customerSession = $customerSession;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            if ($this->customerSession->isLoggedIn()) {
                $stripeCustomerId = $this->stripeHelper->getStripeCustomerId();
                if ($stripeCustomerId) {
                } else {
                    $this->stripeHelper->createCustomer();
                }
            }
        } catch (\Exception $e) {
            // catch error if any
        }
    }
}
