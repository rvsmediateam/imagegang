requirejs( [ 'require', 'jquery', 'https://unpkg.com/masonry-layout@4.2.0/dist/masonry.pkgd.min.js' ],
function( require, $, Masonry ) {
    require( [ 'jquery-bridget/jquery-bridget' ],
    function( jQueryBridget ) {
        jQueryBridget( 'masonry', Masonry, $ );
		$(window).on('load', function(){
			$('.home-images .grid').masonry({
				itemSelector: '.grid-item',
				columnWidth: '.col1',
				gutter: 0,
				percentPosition: true
			});
        });
    });
});