define([
    'underscore',
    'jquery',
    'mage/template',
    'jquery/ui'
], function (_, $, nbImageTplt, modal) {

    "use strict";
    var dependentRowId = [];
    $.widget('mage.dependentOptions', {
        reCheckElements: [],
        _create: function () {
            $.extend(this, this.options);
            $.extend(this, this.options.config);
            this.onOptionsLoad();
            this.setHideDependancy();
            if (this.configureMode == 'configure') {
                this.reCheckDependent();
            }
        },
        onOptionsLoad: function () {
            var elm, optionId, valueId, fieldDiv;

            var optionElements = $('.product-custom-option');
            for (var n = 0; n < optionElements.length; n++) {
                elm = $(optionElements[n]);
                optionId = parseInt(elm.attr('name').match(/\d+/)[0]);

                fieldDiv = elm[0].type == 'radio' || elm[0].type == 'checkbox' ? elm.closest('.options-list').closest('.field') : elm.closest('.field');
                if (!fieldDiv.hasClass('dependent-field')) {
                    fieldDiv.addClass('dependent-field');
                }
                var calendar = elm.attr('data-calendar-role');
                if (elm[0].type == 'select-one' || elm[0].type == 'select-multiple' && !calendar) { 
                    var options = elm[0].options;
                    for (var i = 0, len = options.length; i < len; ++i) {
                        if (options[i].value && this.value_id[optionId]) {
                            var rowidArr = this.value_id[optionId][options[i].value];
                            if (rowidArr) {
                                $.each(rowidArr, function (index, value) {
                                    options[i].classList.add("rowid_" + index);
                                    options[i].setAttribute("rowid", index);
                                    dependentRowId[index] = value;
                                });
                            }
                        }
                    }

                    elm.change($.proxy(this.changeSelectOption, this, elm, optionId));
                }

                if (elm[0].type == 'radio' || elm[0].type == 'checkbox') {
                    valueId = elm.val();
                    var rowidArr = this.value_id[optionId][valueId];
                    if (rowidArr) {
                        $.each(rowidArr, function (index, value) {
                            elm[0].classList.add("rowid_" + index);
                            elm[0].setAttribute("rowid", index);
                            dependentRowId[index] = value;
                        });
                    }

                    if (!valueId) {
                        this.hideElements(elm);
                        elm[0].classList.add("rowid_radio_null");

                    }// radio null option hide
                    elm.click($.proxy(this.clickRadioCheckbox, this, elm, optionId, valueId));
                }

                if (elm[0].type == 'text' || elm[0].type == 'file' || elm[0].type == 'textarea' || calendar) {
                    elm[0].classList.add("rowid_" + this.option_id[optionId]);
                    elm[0].setAttribute("rowid", this.option_id[optionId]);
                }
                this.reCheckElements.push(elm);
            }
        },
        setHideDependancy: function () {

            var MA = this;
            $.each(dependentRowId, function (index, value) {
                if (value) {
                    var rowId = value.split(",");
                    if (parseInt(MA.independent)) {
                        for (var i = 0; i < rowId.length; i++) {
                            var elmt = $('.rowid_' + rowId[i]);

                            if (elmt[0].localName == 'option') {
                                $.each(elmt.parent().children(), function (index, element) {
                                    MA.hideElements($(element));
                                });
                            }

                            if (elmt[0].localName == 'input' || elmt[0].localName == 'textarea') {
                                $(elmt).parent().parent().find('input,textarea').each(function () {
                                    $(this).parent().hide();
                                });
                            }
                        }
                    } else {
                        for (var i = 0; i < rowId.length; i++) {
                            var elmt = $('.rowid_' + rowId[i]);
                            MA.hideElements(elmt);
                        }
                    }
                }
            });
        },
        clickRadioCheckbox: function (element, optionId, valueId) {
            var MA = this;
            if (element[0].type == 'checkbox') {
                $('#options-' + optionId + '-list').find(':checkbox').each(function () {
                    // hide element
                    var rowid = $(this).attr('rowid');
                    if (!$(this).is(':checked')) {
                        var childRowIds = MA.childRowIds(rowid);
                        for (var i = 0; i < childRowIds.length; i++) {
                            var elmt = $('.rowid_' + childRowIds[i]);
                            MA.hideElements(elmt);
                        }
                    }
                });

                $('#options-' + optionId + '-list').find(':checkbox').each(function () {
                    // show element
                    var rowid = $(this).attr('rowid');
                    if ($(this).is(':checked')) {
                        var value = dependentRowId[rowid];
                        if (value) {
                            var rowIds = value.split(",").map(Number);
                            for (var i = 0; i < rowIds.length; i++) {
                                var elmt = $('.rowid_' + rowIds[i]);
                                MA.showElements(elmt);
                            }
                        }
                    }
                });

            } else {

                $('#options-' + optionId + '-list').find(':radio').each(function () {
                    // hide element
                    var rowid = $(this).attr('rowid');
                    if (!$(this).is(':checked')) {
                        var childRowIds = MA.childRowIds(rowid);
                        for (var i = 0; i < childRowIds.length; i++) {
                            var elmt = $('.rowid_' + childRowIds[i]);
                            MA.hideElements(elmt);
                        }
                    }
                });

                $('#options-' + optionId + '-list').find(':radio').each(function () {
                    // show element
                    var rowid = $(this).attr('rowid');
                    if ($(this).is(':checked')) {
                        var value = dependentRowId[rowid];
                        if (value) {
                            var rowIds = value.split(",").map(Number);
                            for (var i = 0; i < rowIds.length; i++) {
                                var elmt = $('.rowid_' + rowIds[i]);
                                MA.showElements(elmt);
                            }
                        }
                    }
                });
            }


        },
        childRowIds: function (rowid) {
            /* 	return all child id and this sub child ids */
            var childArr = [];
            var rowIdList = '';
            var i = 0;
            while (1) {
                if (newRowIdStr) {
                    var rowIdStr = newRowIdStr;
                } else {
                    var rowIdStr = dependentRowId[rowid];
                }

                if (!rowIdStr) {
                    return true;
                }

                var rowIds = rowIdStr.split(",");
                for (var i = 0; i < rowIds.length; i++) {
                    childArr.push(rowIds[i]);
                    if (dependentRowId[rowIds[i]]) {
                        rowIdList += dependentRowId[rowIds[i]] + ',';
                    }
                }

                var newRowIdStr = rowIdList.substring(0, rowIdList.length - 1); // remove last semicolon
                if (!newRowIdStr) {
                    break;
                }
                rowIdList = '';

                i++;
            }
            return childArr;
        },
        changeSelectOption: function (element, optionId) {
            var MA = this;
            element.find("option").each(function () {
                var rowid = $(this).attr('rowid');

                if (!$(this).is(':selected')) {
                    var childRowIds = MA.childRowIds(rowid);
                    for (var i = 0; i < childRowIds.length; i++) {
                        var elmt = $('.rowid_' + childRowIds[i]);
                        MA.hideElements(elmt);
                    }
                }
            });

            element.find("option").each(function () {
                var rowid = $(this).attr('rowid');

                if ($(this).is(':selected')) {
                    var value = dependentRowId[rowid];
					
                    if (value) {
                        var rowIds = value.split(",").map(Number);
                        for (var i = 0; i < rowIds.length; i++) {
                            var elmt = $('.rowid_' + rowIds[i]);
                            MA.showElements(elmt);
                        }
                    }
                }
            });
        },
        hideElements: function (elmt) {
            var MA = this;
            if (elmt.length) {
                if (elmt[0].localName == 'option') {

                    elmt.hide();
                    elmt.closest('.field').hide();
                    if (this.configureMode == 'view') {
                        elmt.parent().val('');
                    }

                    // check if elmt any brother element display
                    elmt.parent().find("option").each(function () {
                        var rowid = $(this).attr('rowid');
                        if ($(this).css("display") !== "none" && rowid) {
                            elmt.closest('.field').show();
                        }
                    });
                }

                var calendar = elmt.attr('data-calendar-role');
                if (elmt[0].localName == 'input' || calendar != undefined) {
                    elmt.closest('.field').hide();
                    elmt.closest('.dependent-field').hide();
                    elmt.closest('.dependent-field').closest('.field').hide();

                    // check if elmt any brother element display
                    if (elmt[0].type == 'radio' || elmt[0].type == 'checkbox') {
                        elmt.closest('.options-list').find(":radio, :checkbox").each(function () {

                            if ($(this).closest('.field').css("display") !== "none") {
                                $(this).closest('.dependent-field').show();
                                $(this).closest('.dependent-field').closest('.field').show();

                                $('.rowid_radio_null').show();
                                $('.rowid_radio_null').closest('.field').show();
                            } else {
                                $('.rowid_radio_null').hide();
                                $('.rowid_radio_null').closest('.field').hide();
                            }
                        });
                    }
                }

                if (elmt[0].type == 'checkbox' && $(elmt).is(':checked')) {
                    if (this.configureMode == 'view') {
                        $(elmt).trigger("click"); // uncheck checkbox
                    }
                }

                if (elmt[0].type == 'radio') {
                    if (this.configureMode == 'view') {
                        $(elmt).prop('checked', false);
                    }
                }

                if (elmt[0].localName == 'textarea') {
                    elmt.hide();
                    elmt.closest('.field').hide();
                }

                $(elmt).closest('div.control').find('.custom-image-box').removeClass('show-imgbox'); // use in custom option image
                $(elmt).closest('div.control').find('.custom-image-box').addClass('hide-imgbox'); // use in custom option image
            }
        },
        showElements: function (elmt) {
            if (elmt.length) {
                if (
                    elmt[0].localName == 'option'
                ) { // select multi-select
                    elmt.show();
                    elmt.closest('.field').show(); 
					
                }

                var calendar = elmt.attr('data-calendar-role');
                if (elmt[0].localName == 'input' || calendar != undefined) { // checkbox text radio file
                    elmt.closest('.field').show();
                    elmt.closest('.dependent-field').show();
                    elmt.closest('.dependent-field').closest('.field').show();
                }

                if (elmt[0].localName == 'textarea') { // textarea
                    elmt.show();
                    elmt.closest('.field').show();
                }
					elmt.closest('div.control').find('.custom-image-box').removeClass('hide-imgbox'); // use in custom option image
					elmt.closest('div.control').find('.custom-image-box').addClass('show-imgbox'); // use in custom option image
            }
        },
        showElementByRowId: function (rowid) {
            var value = dependentRowId[rowid];
            if (value) {
                var rowIds = value.split(",").map(Number);
                for (var i = 0; i < rowIds.length; i++) {
                    var elmt = $('.rowid_' + rowIds[i]);
                    this.showElements(elmt);
                }
            }
        },
        reCheckDependent: function () {
            var MA = this;
            $.each(this.reCheckElements, function (index, element) {
                element = $(element[0]);
                if (element) {
                    var optionId = parseInt(element.attr('name').match(/\d+/)[0]);
                    var fieldDiv = element[0].type == 'radio' || element[0].type == 'checkbox' ? element.closest('.options-list').closest('.field') : element.closest('.field');
                    if (element.is(':radio, :checkbox')) {
                        if (element.is(':checked')) {
                            MA.clickRadioCheckbox(element, optionId);
                        }
                    }

                    if (element.is('select')) {
                        if (element.val()) {
                            MA.changeSelectOption(element, '');
                        }
                    }
                }
            });
        }

    });
    return $.mage.dependentOptions;
});
