define([
    'jquery',
    'mage/template',
    'jquery/ui',
    'MageArray_CustomOptions/js/jquery.colorbox'
], function ($, nbImageTplt, modal) {
    $.widget('mageArray.optionImages', {

        issetImg: false,
        mainImg: false,
        oldOption: [],
        preloadedImageThumb: [],
        preloadedDescptn: [],

        _create: function () {

            $.extend(this, this.options);
            $.extend(this, this.options.config);

            this.opImagesBox = nbImageTplt('[data-template=magearray-co-images-box]');
            this.opImgTplt = nbImageTplt('[data-template=magearray-co-images-thumbnail]');
            this.tooltipTemplate = nbImageTplt('[data-template=magearray-co-images-tooltip]');
				
            this.loadOptionImages();
            this.setPreSettings();

        },
        setMainImage: function (url = '', caption = '') {
            if (url) {
                var images = [];
                images.push({
                    caption: caption,
                    full: url,
                    img: url,
                    thumb: url
                });

                var gallery = jQuery('[data-gallery-role=gallery-placeholder]', '.column.main');
                var galleryObject = gallery.data('gallery');
                var imageArr = galleryObject.returnCurrentImages();

                imageCount = parseInt(imageArr.length);
                if (this.issetImg == false) {
                    this.issetImg = imageCount;
                    galleryObject.updateDataByIndex(this.issetImg, imageArr[0]);
                    galleryObject.seek(1);
                }

                galleryObject.updateDataByIndex(0, images[0]);
                galleryObject.seek(1);
            }
        },
        setPreSettings: function () {
            var MA = this;

            if (parseInt(this.showCustomOptionTooltip)) {
                $('.truncated').tooltip({
                    track: true,
                    content: function () {
                        return $(this).prop('title');
                    }
                });
            } else {
                $('.field.option-image').children('.label').children('.truncated').remove();
            }

            if (parseInt(this.showOptionsTooltip)) {
                $('.truncated').tooltip({
                    track: true,
                    content: function () {
                        return $(this).prop('title');
                    }
                });
            }

            $(".colorbox").colorbox(
                {
                    rel: 'nofollow'
                }
            );

            //Configure colorbox call back to resize with custom dimensions
            $.colorbox.settings.onLoad = function () {
                colorboxResize();
            }

            //Customize colorbox dimensions
            var colorboxResize = function (resize) {
                var width = "90%";
                var height = "90%";

                if ($(window).width() > 960) {
                    width = "860"
                }
                if ($(window).height() > 700) {
                    height = "630"
                }

                $.colorbox.settings.height = height;
                $.colorbox.settings.width = width;

                //if window is resized while lightbox open
                if (resize) {
                    $.colorbox.resize({
                        'height': height,
                        'width': width
                    });
                }
            }

            //In case of window being resized
            $(window).resize(function () {
                colorboxResize(true);
            });

            $('.picker_main .custom-image-box').click(function () {
                var href = $(this).find('[rel="colorbox"]').attr('href');
                var titles = $(this).find('[rel="colorbox"]').attr('title');
                MA.setMainImage(href, titles);
            });

        },
        loadOptionImages: function () {

            var elm, optionId, parentField, isNewOption, valueId, prevVId, tooltip, MA = this;

            var customOpelements = $('.product-custom-option');

            if (parseInt(this.setaccordion)) {
                customOpelements.closest('form').addClass('setaccordion');
            }

            for (var n = 0; n < customOpelements.length; n++) {
                elm = $(customOpelements[n]);
                var optionIdStartIndex, optionIdEndIndex;
                if (elm.is(":file")) {
                    optionIdStartIndex = elm.attr('name').indexOf('_') + 1;
                    optionIdEndIndex = elm.attr('name').lastIndexOf('_');
                } else {
                    optionIdStartIndex = elm.attr('name').indexOf('[') + 1;
                    optionIdEndIndex = elm.attr('name').indexOf(']');
                }

                optionId = parseInt(elm.attr('name').substring(optionIdStartIndex, optionIdEndIndex), 10);

                if (!this.oldOption[optionId]) {
                    this.oldOption[optionId] = {};
                    parentField = elm[0].type == 'radio' || elm[0].type == 'checkbox' ? elm.closest('.options-list').closest('.field') : elm.closest('.field');
                    isNewOption = true;
                    tooltip = true;
                }

                if (isNewOption) { //call 1st time for all types options

                    parentField.addClass('option-image');

                    if (this.description[optionId] && tooltip) { // add tooltip
                        parentField.children('.label').append(this.tooltipTemplate({
                            description: this.description[optionId]
                        }));
                        tooltip = false;
                    }

                    if (this.classs[optionId]) // add user custom class
                    {
                        parentField.addClass(this.classs[optionId]);
                    }
                    if (this.layout[optionId] && !parentField.hasClass(this.layout[optionId])) // add layout class
                    {
                        parentField.addClass(this.layout[optionId]);
                    }
					
                }

                switch (elm[0].type) {
                    case "select-one": {
                        if (this.layout[optionId] == 'color_picker' || this.layout[optionId] == 'picker_main') {
                            parentField.find('.control').prepend(this.opImagesBox); // add images box div
                            var options = elm[0].options;
                            for (var i = 0, len = options.length; i < len; ++i) {
                                if (options[i].value) {
                                    valueId = options[i].value;
                                    if (this.thumbnail[valueId]) {

                                        if (parseInt(this.showoption)) {
                                            parentField.addClass('hidepicker');
                                        }

                                        var imageTmplt = this.opImgTplt({
                                            id: 'value_' + valueId,
                                            title: this.title[valueId],
                                            display: 'block',
                                            descptn: this.descptn[valueId],
                                            dataurl: this.fullimage[valueId],
                                            image: this.thumbnail[valueId]
                                        });
                                        if (isNewOption) {
                                            parentField.find('.image-tplt').append(imageTmplt);
                                            isNewOption = false;
											setTimeout(function() {
												$('.dependent-field.custom-chains .custom-image-box:first-child .custom-image').click();
												},100);
                                        } else {
                                            $('#option_image_value_' + prevVId).parent().parent().after(imageTmplt);
                                        }
                                        prevVId = valueId;

                                        if (this.descptn[prevVId]) {
                                            $('#descptn_value_' + prevVId).show();
                                        } else {
                                            $('#descptn_value_' + prevVId).hide();
                                        }	
                                        var imgBoxPrt = $('#option_image_value_' + prevVId).closest('.custom-image-box');
                                        var imgBoxClick = $('#option_image_value_' + prevVId);
                                        imgBoxClick.attr('optionId', optionId);
                                        imgBoxClick.attr('vleId', prevVId);
                                        imgBoxClick.parent().parent().removeClass('hide-imgbox');

                                        $(imgBoxPrt).click(function () {											
											if($(this).hasClass('active')){
												$(this).removeClass('active');
											}
											else{
												$(this).closest('.image-tplt').children('.active').removeClass('active');
												$(this).addClass('active');
											}
                                            var opId = $(this).find('.custom-image img').attr('optionId');
                                            var vleId = $(this).find('.custom-image img').attr('vleId');
                                            var option = $('#select_' + opId).find('option');
											/*Custom Work */
											var parentChainClass = $(this).parent().parent().parent('.custom-chains');
											if (parentChainClass.length) {
												jQuery('.imageleft').empty();
												//alert(jQuery('.price').text());
												var pendantSelectedVal = jQuery('.charms-pendants .custom-image-box.active .custom-image img').attr('vleId');
												var pendantOptions = jQuery('.charms-pendants .custom-image-box.active .custom-image img').attr('optionId');
												var selectedOption = $('#select_' + pendantOptions).find('option');
												
												var pendantVal = jQuery('.charms-pendants select');
												var imgSrc = $(this).find('.custom-image a').attr('href');
												//jQuery('.imageleft').append('<img src='+imgSrc+'>');
												var mediaHeight = $('.nechImage').height();
												if( $(window).width()<768){
													var mediaHeight = $('.nechImage').height();
													var height = mediaHeight+'px';
												}
												else{
													var height = '500px';
												}
												jQuery('.imageleft').css({"background": "url("+imgSrc+")","background-position":"center top","background-repeat":"no-repeat","width":"100%","height":height,"position": "relative","background-size": "contain"});
											}
											var parentPendantClass = $(this).parent().parent().parent('.charms-pendants');
											if (parentPendantClass.length) {

												var imgSrc = $(this).find('.custom-image a').attr('href');
												var imgSrcId = $(this).find('.custom-image img').attr('optionId');
												var lengthId = $('.imagependantleft img').length;
												 var charmimageIndex = jQuery(this).index();
												var imagesrcUrl = [];
												if(charmimageIndex==0){
													$("#"+imgSrcId).remove();
												}
												else{
												if(lengthId>0){
													img_url = imgSrc;
													var imgalready = $('.imagependantleft img');
													for (var i = 0; i < imgalready.length; i++) {
															var srcUrlImage = imgalready[i];
															imagesrcUrl.push(srcUrlImage.id);	
															
													}	
													if (imagesrcUrl.length === 0) {													
													}

													else {
														 
														if(jQuery.inArray(imgSrcId,imagesrcUrl)!== -1){
																//	alert('sd');
															for (var i = 0; i < imgalready.length; i++) {
																var srcUrlImage = imgalready[i];
																if(srcUrlImage.id == imgSrcId)
																{
																	 $("#"+imgSrcId).attr("src",imgSrc);
																}
															}	
														}
														else{
															jQuery('.imagependantleft').append('<img src='+imgSrc+' id='+imgSrcId+'>');	
														
														}
													}
													
												}else{
													jQuery('.imagependantleft').append('<img src='+imgSrc+' id='+imgSrcId+'>');
												}
												}
											}
											/*Custom Work End*/
                                            for (var i = 0; i < option.length; i++) {
                                                if (option[i].value == vleId) {
                                                    $('#select_' + opId).val(option[i].value).change();
                                                    //option[i].selected = true;
                                                    break;
                                                }
                                            }
											$('.chains-size select').val(jQuery('select option:first').val()).trigger('change');
                                        });
                                    }
                                }
                            }
                            elm.change($.proxy(this.reloadSelect, this, elm, optionId));
                            this.reloadSelect(elm, optionId);

                        } else {
                            var options = elm[0].options;
                            for (var i = 0, len = options.length; i < len; ++i) {
                                if (options[i].value) {
                                    valueId = options[i].value;
                                    if (this.thumbnail[valueId]) {
                                        this.preloadedImageThumb[valueId] = new Image();
                                        this.preloadedImageThumb[valueId].src = this.thumbnail[valueId];
                                        this.preloadedDescptn[valueId] = this.descptn[valueId];
                                    }
                                }
                            }

                            if (this.layout[optionId] == 'above_option') {

                                parentField.find('.control').prepend(this.opImagesBox); // add images box div
                                parentField.find('.image-tplt').append(this.opImgTplt({
                                    id: optionId,
                                    title: this.title[valueId],
                                    display: 'none',
                                    descptn: this.descptn[valueId],
                                    dataurl: this.fullimage[valueId],
                                    image: this.spacer
                                }));
                            }

                            if (this.layout[optionId] == 'before_option') {
                                parentField.find('.control').prepend(this.opImagesBox); // add images box div
                                parentField.find('.image-tplt').append(this.opImgTplt({
                                    id: optionId,
                                    title: this.title[valueId],
                                    display: 'none',
                                    descptn: this.descptn[valueId],
                                    dataurl: this.fullimage[valueId],
                                    image: this.spacer
                                }));

                                parentField.find('.image-tplt').addClass('left');
                                parentField.find('.control select').addClass('right');
                            }

                            if (this.layout[optionId] == 'below_option') {
								$('.chains-size select option:first-child').css('display','block');		
                                if (parentField.find('div.control .btnbox').length) {
                                    parentField.find('div.control .btnbox').before(this.opImagesBox);
                                } else {
                                    parentField.find('div.control').append(this.opImagesBox);
                                }

                                parentField.find('.image-tplt').append(this.opImgTplt({
                                    id: optionId,
                                    title: this.title[valueId],
                                    display: 'none',
                                    descptn: this.descptn[valueId],
                                    dataurl: this.fullimage[valueId],
                                    image: this.spacer
                                }));
								
                            }
                            elm.change($.proxy(this.observeSelectOne, this, elm, optionId));
                            this.observeSelectOne(elm, optionId);
                        }

                        /******/
                        break;
                    }
                    case "radio": {
                        valueId = elm.val();
                        if (elm.attr('value') != '') {
                            if (re_isNewOption) {
                                isNewOption = true;
                                re_isNewOption = false;
                            }

                            if (this.thumbnail[valueId]) {
                                this.preloadedImageThumb[valueId] = new Image();
                                this.preloadedImageThumb[valueId].src = this.thumbnail[valueId];
                            }

                            if (this.descptn[valueId]) {
                                this.preloadedDescptn[valueId] = this.descptn[valueId];
                            }

                            if (this.layout[optionId] == 'grid' || this.layout[optionId] == 'list') {
                                if (this.thumbnail[valueId]) {
                                    elm.parent().prepend(this.opImagesBox); // add images box div
                                    elm.parent().find('.image-tplt').append(this.opImgTplt({
                                        id: 'value_' + valueId,
                                        title: this.title[valueId],
                                        display: 'block',
                                        descptn: this.descptn[valueId],
                                        dataurl: this.fullimage[valueId],
                                        image: this.thumbnail[valueId]
                                    }));

                                    if (this.descptn[valueId]) {
                                        $('#descptn_value_' + valueId).show();
                                    } else {
                                        $('#descptn_value_' + valueId).hide();
                                    }

                                    $('#option_image_value_' + valueId).parent().parent().removeClass('hide-imgbox');
                                    $('#option_image_value_' + valueId).closest('.field').click(function () {
                                        $(this).find('[type="radio"]').prop("checked", true);
                                        $(this).find('[type="radio"]').change();
                                        $(this).parent().find('.active').removeClass('active');
                                        $(this).addClass('active');
                                    });
                                }
                                isNewOption = false;
                            } else {

                                if (isNewOption) {

                                    if (this.layout[optionId] == 'above_option') {
                                        parentField.find('.control').prepend(this.opImagesBox); // add images box div
                                        parentField.find('.image-tplt').append(this.opImgTplt({
                                            id: optionId,
                                            display: 'none',
                                            title: this.title[valueId],
                                            descptn: this.descptn[valueId],
                                            dataurl: this.fullimage[valueId],
                                            image: this.spacer
                                        }));
                                    }

                                    if (this.layout[optionId] == 'before_option') {
                                        parentField.find('.control').prepend(this.opImagesBox); // add images box div
                                        parentField.find('.image-tplt').append(this.opImgTplt({
                                            id: optionId,
                                            display: 'none',
                                            title: this.title[valueId],
                                            descptn: this.descptn[valueId],
                                            dataurl: this.fullimage[valueId],
                                            image: this.spacer
                                        }));
                                        parentField.find('.image-tplt').addClass('left');
                                        parentField.find('.control .options-list').addClass('right');
                                    }

                                    if (this.layout[optionId] == 'below_option' && this.thumbnail[valueId]) {
                                        if (parentField.find('div.control .btnbox').length) {
                                            parentField.find('div.control .btnbox').before(this.opImagesBox);
                                        } else {
                                            parentField.find('div.control').append(this.opImagesBox);
                                        }
                                        parentField.find('.image-tplt').append(this.opImgTplt({
                                            id: optionId,
                                            title: this.title[valueId],
                                            display: 'none',
                                            descptn: this.descptn[valueId],
                                            dataurl: this.fullimage[valueId],
                                            image: this.spacer
                                        }));
                                    }
                                    isNewOption = false;
                                }
                                elm.click($.proxy(this.observeRadio, this, elm, optionId, valueId));
                            }
                        } else { // if radio button not required in admin will show none
                            elm.click($.proxy(this.observeRadio, this, elm, optionId, valueId));
                            isNewOption = false;
                            var re_isNewOption = true;
                        }

                        if (elm[0].checked) {
                            elm.trigger('click');
                        }

                        /******/
                        break;
                    }
                    case "checkbox": {
                        valueId = elm.val();
                        if (this.thumbnail[valueId]) {
							 if (this.layout[optionId] == 'grid' || this.layout[optionId] == 'list') {
								
                                elm.parent().prepend(this.opImagesBox); // add images box div
                                elm.parent().find('.image-tplt').append(this.opImgTplt({
                                    id: 'value_' + valueId,
                                    title: this.title[valueId],
                                    display: 'block',
                                    descptn: this.descptn[valueId],
                                    dataurl: this.fullimage[valueId],
                                    image: this.thumbnail[valueId]
                                }));
								if (this.classs[optionId]) // add user custom class
								{
									parentField.addClass(this.classs[optionId]);
								}
                                if (this.descptn[valueId]) {
                                    $('#descptn_value_' + valueId).show();
                                } else {
                                    $('#descptn_value_' + valueId).hide();
                                }

										
								$('.checkbox').click(function() {
								  if ($(this).is(':checked')) {
									//console.log('yes');
									var checkedId = $(this).attr('id');
									var labelText = $('.charms-category .choice label[for="' + checkedId + '"] span').text();
									
								  }
								  else{
									//console.log('nos');  
									var checkedId = $(this).attr('id');
									if($('.charms-category .choice label[for="' + checkedId + '"]')) {
										var labelText = $('.charms-category .choice label[for="' + checkedId + '"] span').text();
										if($('.charms-pendants').find('[type="checkbox"]').attr('id',checkedId)){
											var checkBoxes = $('.charms-pendants').find('[type="checkbox"]').attr('id',checkedId);
											checkBoxes.prop("checked", !checkBoxes.prop("checked"));
											 $('#' + checkedId).parent().removeClass('active');
										}
									}
									
								  }
								});
								
                                $('#option_image_value_' + valueId).parent().parent().removeClass('hide-imgbox');
								
                                 $('#option_image_value_' + valueId).closest('.choice').click(function (e) {
									var numberofimages = $('.imagependantleft img').length;
									var optionListId = $(this).parent().attr('id');
									if($(window).width() > 980){
										if(optionListId=='options-66-list'){
											var width = 'auto';
											var height ='75px';
										}
										else if(optionListId=='options-67-list'){
											var width = 'auto';
											var height ='75px';
										}
										else{
											var width = 'auto';
											var height ='75px';
										}
									}
									else{
										if(optionListId=='options-66-list'){
											var width = 'auto';
											var height ='75px';
										}
										else if(optionListId=='options-67-list'){
											var width = 'auto';
											var height ='75px';
										}
										else{
											var width = 'auto';
											var height ='75px';
										}
									}
                                    var checkBoxes = $(this).find('[type="checkbox"]');
                                    checkBoxes.prop("checked", !checkBoxes.prop("checked"));
									checkBoxes.change();
									var imagesrcUrl = [];
									
									var widthwindow = $('.imagependantleft').width();
									var halfwindow = parseInt(widthwindow,10)/2;
									var halfimage = parseInt(halfwindow)-parseInt(20);
									var heightwindow = $('.nechImage').height();
									
									var imageheight =  parseInt(heightwindow)-parseInt(10);
									
									if ($(window).width() > 1026){
										var fromleft = '278.28px';
										var fromtop = '488px';
									}
									else if ($(window).width() > 980 && $(window).width() <= 1025){
											var fromleft = '240px';
											var fromtop = '488px';
									}	
									else if ($(window).width() > 767 && $(window).width() <= 979){
										var imageheight =  parseInt(heightwindow)+parseInt(15);
											var fromleft = halfimage+'px';
											var fromtop = imageheight+'px';
											console.log('numberofimagesssss'+numberofimages);
									}										
									else{
										var fromleft = halfimage+'px';
										var fromtop = imageheight+'px';		
									}
									console.log('numberofimages'+numberofimages);
                                    if (checkBoxes.is(':checked')) {
										var imgalready = $('.imagependantleft img');
										var imageLengths = imgalready.length;
										var totalImages = parseInt(imageLengths)+parseInt(1);
										console.log(totalImages+'totalImages');
										$(this).addClass('active');
										if(totalImages<=5){
											if ($(window).width() > 1026){
												var leftvalue = '278.25'; 
											}										
											else if ($(window).width() > 980 && $(window).width() <= 1025){
												var leftvalue = '240';
											}
											else{
												var leftvalue = halfimage; 	
											}
											
											var imgSrc = $(this).find('.custom-image a').attr('href');
											var imgSrcId = $(this).find('.custom-image a').attr('id');										
											
											var imageLengths = imgalready.length;
											var totalImages = parseInt(imageLengths)+parseInt(1);	
											if(numberofimages>0){
												var imglastLeft = jQuery('.imagependantleft img:first-child').attr('data-left');
												var imglastTop = jQuery('.imagependantleft img:first-child').attr('data-top');
												if(numberofimages % 2 === 0){	
													var fromleftside = '20';
													if ($(window).width() > 1026){
														var top = '10';
														var leftside = fromleftside * numberofimages;
														var totalleftside = parseInt(imglastLeft) - parseInt(fromleftside);												
														var topside = top * numberofimages;
														
														var toplength = parseInt(topside);
														if(totalImages=='3'){ 
															var totaltopside = parseInt(imglastTop)- parseInt(toplength)+parseInt(15);	
														}
														else if(totalImages=='5'){ 
															var totaltopside = parseInt(imglastTop)- parseInt(toplength)+parseInt(28);	
															
														}
														else{
															var totaltopside = parseInt(imglastTop)- parseInt(toplength);
														}
													}
													else if ($(window).width() > 980 && $(window).width() <= 1025){
														var top = '10';
														var leftside = fromleftside * numberofimages;
														var totalleftside = parseInt(imglastLeft) - parseInt(fromleftside);												
														var topside = top * numberofimages;
														
														var toplength = parseInt(topside);
														if(totalImages=='4'){ 
															var totaltopside = parseInt(imglastTop)- parseInt(toplength)+parseInt(12);	
														}
														else if(totalImages=='3'){ 
															var totaltopside = parseInt(imglastTop)- parseInt(toplength)+parseInt(15);	
														}
														else if(totalImages=='5'){ 
															var totaltopside = parseInt(imglastTop)- parseInt(toplength)+parseInt(31);	
														}
														else{
															var totaltopside = parseInt(imglastTop)- parseInt(toplength);
														}
													}
													else if ($(window).width() > 767 && $(window).width() <= 979){
														var top = '5';
														var leftside = fromleftside * numberofimages;
														var totalleftside = parseInt(imglastLeft) - parseInt(fromleftside);												
														var topside = top * numberofimages;
														console.log('asddddddddddddd');
															var toplength = parseInt(topside);
															if(totalImages=='5'){ 
																var totaltopside = parseInt(imglastTop)- parseInt(toplength)+parseInt(10);	
																console.log('344444444444444asddddddddddddd');
															}
															else if(totalImages=='3'){ 
																var totaltopside = parseInt(imglastTop)- parseInt(toplength)+parseInt(6);	
															}
															else{
																var totaltopside = parseInt(imglastTop)- parseInt(toplength);
															}
													}
													else if ($(window).width()<411){
														var top = '5';
														var leftside = fromleftside * numberofimages;
														var totalleftside = parseInt(imglastLeft) - parseInt(fromleftside);												
														var topside = top * numberofimages;
														
														var toplength = parseInt(topside);
														if(totalImages=='5'){ 
															var totaltopside = parseInt(imglastTop)- parseInt(toplength)+parseInt(11);	
														}
														else if(totalImages=='3'){ 
															var totaltopside = parseInt(imglastTop)- parseInt(toplength)+parseInt(9);	
														}
														else{
															var totaltopside = parseInt(imglastTop)-parseInt(toplength)+parseInt(16);
														}		
													}
													else if ($(window).width() >= 411 && $(window).width() <= 641){
														var top = '5';
														var leftside = fromleftside * numberofimages;
														var totalleftside = parseInt(imglastLeft) - parseInt(fromleftside);												
														var topside = top * numberofimages;
														
															var toplength = parseInt(topside);
														if(totalImages=='3'){ 
															var totaltopside = parseInt(imglastTop)- parseInt(toplength)+parseInt(7);	
														}
														else if(totalImages=='5'){ 
															var totaltopside = parseInt(imglastTop)- parseInt(toplength)+parseInt(12);	
														}
														else{
															var totaltopside = parseInt(imglastTop)- parseInt(toplength);
														}
													}													
													else{
														var top = '5';
														var leftside = fromleftside * numberofimages;
														var totalleftside = parseInt(imglastLeft) - parseInt(fromleftside);												
														var topside = top * numberofimages;
														
														var toplength = parseInt(topside);
															var totaltopside = parseInt(imglastTop)- parseInt(toplength);
														
													}
													
													jQuery('<img src='+imgSrc+' id='+imgSrcId+' class="hizz" data-left='+totalleftside+' data-leftcurrent='+totalleftside+'  data-top='+totaltopside+' data-topcurrent='+totaltopside+' style="position:absolute;left:'+totalleftside+'px;width:'+width+';height:'+height+';top:'+totaltopside+'px;">').prependTo('.imagependantleft');
													console.log('totaltopsidewithoutaray2'+totaltopside);
													if(totalImages=='5'){
															console.log('totalImages'+totalImages);
															jQuery(".imagependantleft img:first-child").css({"transform": "rotate(45deg)"});
														}	
													
												}
												else{												
													var fromleftside = '20';
													if ($(window).width() > 1026){
														var leftvalue = '278.25'; 
														var top = '10';
														
														var leftside = fromleftside * numberofimages;
														
														var topside = top * numberofimages;
														
														var toplength = parseInt(topside);
														if(totalImages=='2'){ 
															var totaltopside = parseInt(imglastTop)- parseInt(toplength)+parseInt(5);	
														}
														else if(totalImages=='4'){ 
															var totaltopside = parseInt(imglastTop)- parseInt(toplength)+parseInt(22);	
														}
														else{
															var totaltopside = parseInt(imglastTop)- parseInt(toplength);
														}														
														var totalleftside = parseInt(imglastLeft) - parseInt(fromleftside);	
													}
													else if ($(window).width() > 980 && $(window).width() <= 1025){
														var leftvalue = '240'; 
														var top = '10';
														
														var leftside = fromleftside * numberofimages;
														
														var topside = top * numberofimages;
														
														var toplength = parseInt(topside);
														if(totalImages=='2'){ 
															var totaltopside = parseInt(imglastTop)- parseInt(toplength)+parseInt(10);	
														}
														else if(totalImages=='4'){ 
															var totaltopside = parseInt(imglastTop)- parseInt(toplength)+parseInt(25);	
														}
														else{
															var totaltopside = parseInt(imglastTop)- parseInt(toplength);
														}
														
														var totalleftside = parseInt(imglastLeft) - parseInt(fromleftside);	
													}
													else if ($(window).width() > 767 && $(window).width() <= 979){
														var leftvalue = halfimage;
														var top = '5';		

														var leftside = fromleftside * numberofimages;
														
														var topside = top * numberofimages;
														
														var toplength = parseInt(topside);
														if(totalImages=='4'){ 
															var totaltopside = parseInt(imglastTop)- parseInt(toplength)+parseInt(11);	
														}
														else{
															var totaltopside = parseInt(imglastTop)- parseInt(toplength)+parseInt(5);	
														}
														var totalleftside = parseInt(imglastLeft) - parseInt(fromleftside);	
													}
													else if ($(window).width()<411){
														var leftvalue = halfimage;
														var top = '5';		

														var leftside = fromleftside * numberofimages;
														
														var topside = top * numberofimages;
														
														var toplength = parseInt(topside);
														if(totalImages=='4'){ 
															var totaltopside = parseInt(imglastTop)- parseInt(toplength)+parseInt(6);	
														}
														else if(totalImages=='2'){ 
															var totaltopside = parseInt(imglastTop)- parseInt(toplength)+parseInt(4);	
														}
														else{
															var totaltopside = parseInt(imglastTop)- parseInt(toplength);	
														}
														var totalleftside = parseInt(imglastLeft) - parseInt(fromleftside);			
													}
													else if ($(window).width() >=411 && $(window).width() <= 641){
														var leftvalue = halfimage;
														var top = '5';		

														var leftside = fromleftside * numberofimages;
														
														var topside = top * numberofimages;
														
														var toplength = parseInt(topside);
														if(totalImages=='4'){ 
															var totaltopside = parseInt(imglastTop)- parseInt(toplength)+parseInt(8);	
														}
														else if(totalImages=='2'){ 
															var totaltopside = parseInt(imglastTop)- parseInt(toplength)+parseInt(4);	
														}
														else{
															var totaltopside = parseInt(imglastTop)- parseInt(toplength);	
														}
														var totalleftside = parseInt(imglastLeft) - parseInt(fromleftside);														
													}	
													else{
														var leftvalue = halfimage;
														var top = '5';		

														var leftside = fromleftside * numberofimages;
														
														var topside = top * numberofimages;
														
														var toplength = parseInt(topside);
														if(totalImages=='4'){ 
															var totaltopside = parseInt(imglastTop)- parseInt(toplength)-parseInt(20);	
														}
														else{
															var totaltopside = parseInt(imglastTop)- parseInt(toplength);	
														}
														var totalleftside = parseInt(imglastLeft) - parseInt(fromleftside);														
													}											
													jQuery('<img src='+imgSrc+' id='+imgSrcId+' class="hizz" data-left='+totalleftside+' data-leftcurrent='+totalleftside+' data-top='+totaltopside+' data-topcurrent='+totaltopside+' style="position:absolute;left:'+totalleftside+'px;width:'+width+';height:'+height+';top:'+totaltopside+'px;">').prependTo('.imagependantleft');		
													console.log('totaltopsidewithoutaray'+totaltopside);												
												}	
													if(totalImages=='5'){
															console.log('totalImages'+totalImages);
															jQuery(".imagependantleft img:first-child").css({"transform": "rotate(45deg)"});
													}	
											}
											else{
												jQuery('.imagependantleft').append('<img src='+imgSrc+' id='+imgSrcId+' class="hizz" data-left='+leftvalue+' data-leftcurrent='+leftvalue+' data-top='+fromtop+' data-topcurrent='+fromtop+' style="position:absolute;width:'+width+';height:'+height+';left:'+fromleft+'; top:'+fromtop+'">');		
											}
										}
										else{
											 alert("You can only add 5 charms for output. But you can add many charm in the cart.");
										}
										for (var i = 0; i < imgalready.length; i++) {
												var srcUrlImage = imgalready[i];
												imagesrcUrl.push(srcUrlImage.id);
												var imageLengths = imgalready.length;
												var totalImages = parseInt(imageLengths)+parseInt(1);
												var imglastLeft = jQuery('.imagependantleft img:first-child').attr('data-left');
												var imglastTop = jQuery('.imagependantleft img:first-child').attr('data-top');
												if(totalImages<=5){
													if(numberofimages % 2 === 0){	
														console.log('hee'+imagesrcUrl.length+'imglastLeft'+imglastLeft+'imgalready.length'+imgalready.length);
														
														$.each(imagesrcUrl, function(key,detail){														
															var jkey = key+1;
															var kkey = key;
															
															if ($(window).width() > 1026){
																	var leftvalue = '278.25'; 
																	var fromtopside = '20';
																	var fromleftside = '40';															
																
																var leftside = fromleftside * jkey;
																
																if(totalImages=='3'){
																	var topsidekey = fromtopside * jkey;
																	if(jkey=='2' && kkey=='1'){
																		var topside = parseInt(topsidekey)-parseInt(33);
																		console.log('jkey2'+jkey+'kkey'+kkey);
																	}
																	else{																	
																		var topside = parseInt(topsidekey)-parseInt(10);;
																			console.log('nojkey'+'kkey'+kkey);
																	}
																}
																else if(totalImages=='5'){
																	var topsidekey = fromtopside * jkey;
																	if(jkey=='3' && kkey=='2'){
																		var topside = parseInt(topsidekey)-parseInt(36);
																	}
																	else if(jkey=='4' && kkey=='3'){
																		var topside = parseInt(topsidekey)-parseInt(68);
																		jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(-45deg)"});
																	}
																	else if(jkey=='1' && kkey=='0'){
																		var topside = parseInt(topsidekey);
																	}
																	else{																	
																		var topside = parseInt(topsidekey)-parseInt(12);
																	}
																}
																else if(totalImages=='2'){
																	if(kkey=='0' && jkey == '1'){
																		var topsidekey = fromtopside * jkey;
																		var topside = parseInt(topsidekey)-parseInt(5)
																	}
																	
																}
																else{
																	var topside = fromtopside * jkey;
																	console.log('totalImages'+totalImages);
																} 
															}
															else if ($(window).width() > 980 && $(window).width() <=1025){
																var leftvalue = '278.25'; 
																var fromtopside = '20';
																var fromleftside = '40';															
															
																var leftside = fromleftside * jkey;
																
																if(totalImages=='3'){
																	var topsidekey = fromtopside * jkey;
																	if(jkey=='2' && kkey=='1'){
																		var topside = parseInt(topsidekey)-parseInt(45);
																		console.log('jkey2'+jkey+'kkey'+kkey);
																	}
																	else{																	
																		var topside = parseInt(topsidekey)-parseInt(20);
																			console.log('nojkey'+'kkey'+kkey);
																	}
																}
																else if(totalImages=='5'){
																	var topsidekey = fromtopside * jkey;
																	if(jkey=='3' && kkey=='2'){
																		var topside = parseInt(topsidekey)-parseInt(51);
																	}
																	else if(jkey=='4' && kkey=='3'){
																		var topside = parseInt(topsidekey)-parseInt(87);
																		jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(-45deg)"});
																	}
																	else if(jkey=='1' && kkey=='0'){
																		var topside = parseInt(topsidekey)-parseInt(7);
																	}
																	else{																	
																		var topside = parseInt(topsidekey)-parseInt(24);
																	}
																}
																else{
																	var topside = fromtopside * jkey;
																	console.log('totalImages'+totalImages);
																} 
															}
															else if ($(window).width() > 767 && $(window).width() <=979){
																var leftvalue = '278.25'; 
																var fromtopside = '20';
																var fromleftside = '40';															
															
																var leftside = fromleftside * jkey;
																
																if(totalImages=='3'){
																	var topsidekey = fromtopside * jkey;
																	if(jkey=='2' && kkey=='1'){
																		var topside = parseInt(topsidekey)-parseInt(52);
																		console.log('jkey2'+jkey+'kkey'+kkey);
																	}
																	else{																	
																		var topside = parseInt(topsidekey)-parseInt(20);
																			console.log('nojkey'+'kkey'+kkey);
																	}
																}
																else if(totalImages=='5'){
																	var topsidekey = fromtopside * jkey;
																	if(jkey=='3' && kkey=='2'){
																		var topside = parseInt(topsidekey)-parseInt(58);
																	}
																	else if(jkey=='4' && kkey=='3'){
																		var topside = parseInt(topsidekey)-parseInt(96);
																		jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(-45deg)"});
																	}
																	else if(jkey=='1' && kkey=='0'){
																		var topside = parseInt(topsidekey)-parseInt(6);
																	}
																	else{																	
																		var topside = parseInt(topsidekey)-parseInt(26);
																	}
																}
																else{
																	var topside = fromtopside * jkey;
																	console.log('totalImages'+totalImages);
																} 
															}
															else if ($(window).width() >= 411 && $(window).width() <=641){
																var leftvalue = halfimage; 
																var fromtopside = '10';
																var fromleftside = '30';
																
																var leftside = fromleftside * jkey;
																
																if(totalImages=='3'){
																	var topsidekey = fromtopside * jkey;
																	if(jkey=='2' && kkey=='1'){
																		var topside = parseInt(topsidekey)-parseInt(22);
																		console.log('jkey2'+jkey+'kkey'+kkey);
																	}
																	else{																	
																		var topside = parseInt(topsidekey)-parseInt(7);
																			console.log('nojkey'+'kkey'+kkey);
																	}
																}
																else if(totalImages=='5'){
																	var topsidekey = fromtopside * jkey;
																	if(jkey=='3' && kkey=='2'){
																		var topside = parseInt(topsidekey)-parseInt(17);
																	}
																	else if(jkey=='4' && kkey=='3'){
																		var topside = parseInt(topsidekey)-parseInt(36);
																		jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(-45deg)"});
																	}
																	else if(jkey=='1' && kkey=='0'){
																		var topside = parseInt(topsidekey)+parseInt(3);
																		jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																	}
																	else{																	
																		var topside = parseInt(topsidekey)-parseInt(2);
																	}
																}
																else{
																	var topside = fromtopside * jkey;
																	console.log('totalImages'+totalImages);
																}
															}															
															else{
																var leftvalue = halfimage; 
																var fromtopside = '10';
																var fromleftside = '30';
																
																var leftside = fromleftside * jkey;
																
																if(totalImages=='3'){
																	var topsidekey = fromtopside * jkey;
																	if(jkey=='2' && kkey=='1'){
																		var topside = parseInt(topsidekey)-parseInt(25);
																		console.log('jkey2'+jkey+'kkey'+kkey);
																	}
																	else{																	
																		var topside = parseInt(topsidekey)-parseInt(5);
																			console.log('nojkey'+'kkey'+kkey);
																	}
																}
																else if(totalImages=='5'){
																	var topsidekey = fromtopside * jkey;
																	if(jkey=='3' && kkey=='2'){
																		var topside = parseInt(topsidekey)-parseInt(14);
																	}
																	else if(jkey=='4' && kkey=='3'){
																		var topside = parseInt(topsidekey)-parseInt(34);
																		jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(-45deg)"});
																	}
																	else if(jkey=='1' && kkey=='0'){
																		var topside = parseInt(topsidekey)+parseInt(3);
																		jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																	}
																	else{																	
																		var topside = parseInt(topsidekey)-parseInt(1);
																	}
																}
																else{
																	var topside = fromtopside * jkey;
																	console.log('totalImages'+totalImages);
																}
															} 		
															var totalleftside = parseInt(leftside) + parseInt(imglastLeft);
															var totaltopside = parseInt(imglastTop) + parseInt(topside);
															console.log('yes'+detail+'key-'+jkey+'totaltopside'+totaltopside);
															jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
															jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
															jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);
														});
													}
													else{		
														var imageLengths = imgalready.length;
														var totalImages = parseInt(imageLengths)+parseInt(1);
 														
														$.each(imagesrcUrl, function(key,detail){														
															var jkey = key+1;
															var kkey = key;
															if ($(window).width() > 1026){
																var fromleftside = '40';														
																var fromtopside = '10';															
																var leftside = fromleftside * jkey;
																if(totalImages=='4'){
																	var topsidekey = fromtopside * jkey;
																	if(jkey=='3'){
																		var topside = parseInt(topsidekey)-parseInt(20);
																	}
																	else if(jkey=='2'){
																		var topside = parseInt(topsidekey)-parseInt(2);
																	}
																	else if(jkey=='1'){
																		var topside = parseInt(topsidekey)+parseInt(3);
																	}
																	else{																	
																		var topside = parseInt(topsidekey)+parseInt(30);
																	}
																}
																else if(totalImages=='2'){
																	if(kkey=='0' && jkey == '1'){
																		var topsidekey = fromtopside * jkey;	
																		var topside = parseInt(topsidekey)-parseInt(5);
																	}
																}
																else{
																	var topside = fromtopside * jkey;
																	console.log('totalImages'+totalImages);
																}
															}
															else if ($(window).width() > 980 && $(window).width() < 1025){
																var fromleftside = '40';														
																var fromtopside = '10';															
																var leftside = fromleftside * jkey;
																if(totalImages=='4'){
																	var topsidekey = fromtopside * jkey;
																	if(jkey=='3'){
																		var topside = parseInt(topsidekey)-parseInt(40);
																	}
																	else if(jkey=='2'){
																		var topside = parseInt(topsidekey)-parseInt(15);
																	}
																	else{																	
																		var topside = parseInt(topsidekey)-parseInt(2);
																	}
																}
																else if(totalImages=='2'){
																	var topsidekey = fromtopside * jkey;
																	var topside = parseInt(topsidekey)-parseInt(15);
																}
																else{
																	var topside = fromtopside * jkey;
																	console.log('totalImages'+totalImages);
																}
															}
															else if ($(window).width() > 767 && $(window).width() < 979){
																var fromleftside = '40';														
																var fromtopside = '10';															
																var leftside = fromleftside * jkey;
																if(totalImages=='4'){
																	var topsidekey = fromtopside * jkey;
																	if(jkey=='3'){
																		var topside = parseInt(topsidekey)-parseInt(48);
																	}
																	else if(jkey=='2'){
																		var topside = parseInt(topsidekey)-parseInt(20);
																	}
																	else{																	
																		var topside = parseInt(topsidekey)-parseInt(3);
																	}
																}
																else if(totalImages=='2'){
																	var topsidekey = fromtopside * jkey;
																	var topside = parseInt(topsidekey)-parseInt(17);
																}
																else{
																	var topside = fromtopside * jkey;
																	console.log('totalImages'+totalImages);
																}
															}
															else if ($(window).width() >= 411 && $(window).width() < 641){
																var fromleftside = '30';														
																var fromtopside = '10';	
																var leftside = fromleftside * jkey;
																if(totalImages=='4'){
																	var topsidekey = fromtopside * jkey;
																	if(jkey=='3'){
																		var topside = parseInt(topsidekey)-parseInt(30);
																	}
																	else if(jkey=='2'){
																		var topside = parseInt(topsidekey)-parseInt(10);
																	}
																	else{																	
																		var topside = parseInt(topsidekey);
																	}
																}
																else if(totalImages=='2'){
																	var topsidekey = fromtopside * jkey;						
																		var topside = parseInt(topsidekey)-parseInt(13);
																}
																else{
																	var topside = fromtopside * jkey;
																	console.log('totalImages'+totalImages);
																}
															}
															else{
																var fromleftside = '30';														
																var fromtopside = '10';	
																var leftside = fromleftside * jkey;
																if(totalImages=='4'){
																	var topsidekey = fromtopside * jkey;
																	if(jkey=='3'){
																		var topside = parseInt(topsidekey)-parseInt(30);
																	}
																	else if(jkey=='2'){
																		var topside = parseInt(topsidekey)-parseInt(10);
																	}
																	else{																	
																		var topside = parseInt(topsidekey);
																	}
																}
																else if(totalImages=='2'){
																	var topsidekey = fromtopside * jkey;						
																		var topside = parseInt(topsidekey)-parseInt(10);
																}
																else{
																	var topside = fromtopside * jkey;
																	console.log('totalImages'+totalImages);
																}
															}
															var totalleftside = parseInt(leftside) + parseInt(imglastLeft);
															var totaltopside = parseInt(imglastTop) + parseInt(topside);
															console.log('no'+detail+'key-'+jkey+'totaltopside'+totaltopside);
															jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
															jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
															jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);
														});
													}
												}
												else{
													
												}
												
										}									
									}
									else {
										
                                        $(this).removeClass('active');
										var imgSrcId = $(this).find('.custom-image a').attr('id');	
										var lengthId = $('.imagependantleft img').length;
										var imgalready = $('.imagependantleft img');
										for (var i = 0; i < imgalready.length; i++) {
												var srcUrlImage = imgalready[i];
												imagesrcUrl.push(srcUrlImage.id);
													console.log(imagesrcUrl);
										}
										var imglength = imgalready.length;
										console.log('hellowww'+imglength);
										if(jQuery.inArray(imgSrcId,imagesrcUrl)!== -1){
											for (var i = 0; i < imgalready.length; i++) {
												var srcUrlImage = imgalready[i];
												var srcUrlImageId = srcUrlImage.id;
												if(srcUrlImage.id == imgSrcId){	
													var removedImgIndex = $('.imagependantleft img#'+imgSrcId).index();
													$.each(imagesrcUrl, function(key,detail){
														var kdata = key+1;
														var kkey = key;	
														var srcUrlDataLeft = $(".imagependantleft img#"+srcUrlImageId).attr('data-leftcurrent');
														var srcUrlDataTop = $(".imagependantleft img#"+srcUrlImageId).attr('data-topcurrent');
														var imglastLeft = jQuery('.imagependantleft img:first-child').attr('data-left');
														var imglastTop = jQuery('.imagependantleft img:first-child').attr('data-top');
														
														var totalImages = parseInt(numberofimages)-parseInt(1);
														if(totalImages % 2 === 0){
															if ($(window).width() > 1026){
																var fromleftside = 20;																				
																if(key<removedImgIndex){
																	console.log("removedImgIndexI"+key);
																	var fromtopside = 20;																
																	var jkey = key+1;
																	var kkey = key;	
																	var leftside = fromleftside * jkey;
																	var leftfromside = fromleftside * kkey;
																	var topside = fromtopside * jkey;
																	var topsidekey = fromtopside * kkey;
																	var totalleft = $(".imagependantleft img#"+detail).attr('data-leftcurrent');
																	var totaltop = $(".imagependantleft img#"+detail).attr('data-topcurrent');
																	
																	var totalleftside = parseInt(totalleft)+parseInt(leftside)-parseInt(leftfromside);
																	if(totalImages=='4'){
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='1'&& kkey=='0'){
																			var topside = parseInt(topsidekey)+parseInt(12);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																		}
																		else if(jkey=='2'&& kkey=='1'){
																			var topside = parseInt(topsidekey)+parseInt(5);
																		}
																		else if(jkey=='3'&& kkey=='2'){
																			var topside = parseInt(topsidekey)+parseInt(2);
																		}
																		else if(jkey=='4'&& kkey=='3'){
																			var topside = parseInt(topsidekey)-parseInt(4);
																		}
																		else{																	
																			var topside = parseInt(topsidekey);
																			}
																	}																					
																	else if(totalImages=='2'){
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='1'&& kkey=='0'){
																			var topside = parseInt(topsidekey)+parseInt(5);
																		}
																		else if(jkey=='2'&& kkey=='1'){ 
																			var topside = parseInt(topsidekey);
																		}
																		else{
																			var topside = parseInt(topsidekey)+parseInt(15);
																		}
																	}
																	else{
																		var topside = parseInt(topsidekey)+parseInt(30);
																	}
																	
																	
																	var totaltopside = parseInt(totaltop) + parseInt(topside)-parseInt(topsidekey);																						
																	jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
																	jQuery(".imagependantleft img#"+detail).attr("data-left",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-top",totaltopside);
																	jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);
																}
																else if(key==removedImgIndex){
																}
																else{	
																		var fromtopside = 10;
																		if(key>=2){
																			var kkey = key-2;
																			var jkey = key-1;	
																		}
																		else if(key==0){
																			var jkey = key;	
																			var kkey = key;
																		}
																		else{
																			var kkey = key-1;
																			var jkey = key-1;	
																		}
																		var jkeyadd = key+1;
																		var leftside = fromleftside * jkeyadd;	
																		var leftsidefrom = fromleftside * key;	
																		var topsidekey = fromtopside * kkey;										
																		var totalleft = $(".imagependantleft img#"+detail).attr('data-leftcurrent');
																		var totaltop = $(".imagependantleft img#"+detail).attr('data-topcurrent');
																		var totalleftside = parseInt(totalleft)+parseInt(leftsidefrom)-parseInt(leftside);
																		if(totalImages=='4'){
																		var topsidekey = fromtopside * jkey;
																			if(jkey=='0'&& kkey=='0'){
																				var topside = parseInt(topsidekey)-parseInt(8);
																			}
																			else if(jkey=='1'&& kkey=='0'){
																				var topside = parseInt(topsidekey)-parseInt(13);
																			}
																			else if(jkey=='2'&& kkey=='1'){							
																				var topside = parseInt(topsidekey)-parseInt(14);
																			}
																			else if(jkey=='3'&& kkey=='2'){							
																				var topside = parseInt(topsidekey)-parseInt(22);
																				jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																			}
																			else{
																				var topside = parseInt(topsidekey)-parseInt(20);
																			}
																		}
																		else if(totalImages=='2'){
																			var topsidekey = fromtopside * jkey;
																			if(jkey=='1'&& kkey=='0'){
																				var topside = parseInt(topsidekey)-parseInt(7);
																			}
																			else{
																				var topside = parseInt(topsidekey)-parseInt(5);
																			}
																		}
																		else{
																			var topside = parseInt(topsidekey);
																		}
																		var totaltopside = parseInt(totaltop)+parseInt(topside);
																		jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
																		jQuery(".imagependantleft img#"+detail).attr("data-left",totalleftside);
																		jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
																		jQuery(".imagependantleft img#"+detail).attr("data-top",totaltopside);
																		jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);																						
																}
															}	
															else if ($(window).width() > 980 && $(window).width() <=1025){
																var fromleftside = 20;																				
																if(key<removedImgIndex){
																	var fromtopside = 10;																
																	var jkey = key+1;
																	var kkey = key;	
																	var leftside = fromleftside * jkey;
																	var leftfromside = fromleftside * kkey;
																	var topside = fromtopside * jkey;
																	var topsidekey = fromtopside * kkey;
																	var totalleft = $(".imagependantleft img#"+detail).attr('data-leftcurrent');
																	var totaltop = $(".imagependantleft img#"+detail).attr('data-topcurrent');
																	
																	var totalleftside = parseInt(totalleft)+parseInt(leftside)-parseInt(leftfromside);
																	if(totalImages=='4'){
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='1'&& kkey=='0'){
																			var topside = parseInt(topsidekey)+parseInt(13);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																		}
																		else if(jkey=='2'&& kkey=='1'){
																			var topside = parseInt(topsidekey)+parseInt(8);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																		}
																		else if(jkey=='3'&& kkey=='2'){
																			var topside = parseInt(topsidekey)-parseInt(5);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																		}
																		else if(jkey=='4'&& kkey=='3'){
																			var topside = parseInt(topsidekey)-parseInt(17);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																		}
																		else{																	
																			var topside = parseInt(topsidekey);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																			}
																	}
																	else if(totalImages=='2'){
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='1'&& kkey=='0'){
																			var topside = parseInt(topsidekey)+parseInt(5);
																		}
																		else if(jkey=='2'&& kkey=='1'){ 
																			var topside = parseInt(topsidekey);
																		}
																		else{
																			var topside = parseInt(topsidekey)+parseInt(20);
																		}
																	}
																	else{
																		var topside = parseInt(topsidekey)+parseInt(25);
																	}
																	var totaltopside = parseInt(totaltop) + parseInt(topside)-parseInt(topsidekey);																						
																	jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
																	jQuery(".imagependantleft img#"+detail).attr("data-left",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-top",totaltopside);
																	jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);
																}
																else if(key==removedImgIndex){
																}
																else{
																	var fromtopside = 10;
																	if(key>=2){
																		var kkey = key-2;
																		var jkey = key-1;	
																	}
																	else if(key==0){
																		var jkey = key;	
																		var kkey = key;
																	}
																	else{
																		var kkey = key-1;
																		var jkey = key-1;	
																	}
																	var jkeyadd = key+1;
																	var leftside = fromleftside * jkeyadd;	
																	var leftsidefrom = fromleftside * key;	
																	var topsidekey = fromtopside * kkey;										
																	var totalleft = $(".imagependantleft img#"+detail).attr('data-leftcurrent');
																	var totaltop = $(".imagependantleft img#"+detail).attr('data-topcurrent');
																	var totalleftside = parseInt(totalleft)+parseInt(leftsidefrom)-parseInt(leftside);
																	if(totalImages=='4'){
																	var topsidekey = fromtopside * jkey;
																		if(jkey=='0'&& kkey=='0'){
																			var topside = parseInt(topsidekey)-parseInt(4);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																		}
																		else if(jkey=='1'&& kkey=='0'){
																			var topside = parseInt(topsidekey)-parseInt(9);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																		}
																		else if(jkey=='2'&& kkey=='1'){
																			var topside = parseInt(topsidekey)-parseInt(15);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																		}
																		else if(jkey=='3'&& kkey=='2'){
																			var topside = parseInt(topsidekey)-parseInt(24);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																		}
																		else{
																			var topside = parseInt(topsidekey);
																		}
																	}
																	else if(totalImages=='2'){
																		var totalleftside = parseInt(totalleft)+parseInt(leftsidefrom)-parseInt(leftside);
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='1'&& kkey=='0'){
																			var topside = parseInt(topsidekey)-parseInt(5);
																		}
																		else{
																			var topside = parseInt(topsidekey)+parseInt(5);
																		}
																	}
																	else{
																		var topside = parseInt(topsidekey)-parseInt(5);
																	}
																	var totaltopside = parseInt(totaltop)+parseInt(topside);
																	jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
																	jQuery(".imagependantleft img#"+detail).attr("data-left",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-top",totaltopside);
																	jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);																						
																}
															}
															else if ($(window).width() > 767 && $(window).width() <=979){
																var fromleftside = 20;																				
																if(key<removedImgIndex){
																	var fromtopside = 10;																
																	var jkey = key+1;
																	var kkey = key;	
																	var leftside = fromleftside * jkey;
																	var leftfromside = fromleftside * kkey;
																	var topside = fromtopside * jkey;
																	var topsidekey = fromtopside * kkey;
																	var totalleft = $(".imagependantleft img#"+detail).attr('data-leftcurrent');
																	var totaltop = $(".imagependantleft img#"+detail).attr('data-topcurrent');
																	
																	var totalleftside = parseInt(totalleft)+parseInt(leftside)-parseInt(leftfromside);
																	if(totalImages=='4'){
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='1'&& kkey=='0'){
																			var topside = parseInt(topsidekey)+parseInt(10);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																		}
																		else if(jkey=='2'&& kkey=='1'){
																			var topside = parseInt(topsidekey)+parseInt(3);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																		}
																		else if(jkey=='3'&& kkey=='2'){
																			var topside = parseInt(topsidekey)-parseInt(4);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																		}
																		else if(jkey=='4'&& kkey=='3'){
																			var topside = parseInt(topsidekey)-parseInt(10);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																		}
																		else{																	
																			var topside = parseInt(topsidekey);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																			}
																	}
																	else if(totalImages=='2'){
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='1'&& kkey=='0'){
																			var topside = parseInt(topsidekey)+parseInt(4);
																		}
																		else if(jkey=='2'&& kkey=='1'){ 
																			var topside = parseInt(topsidekey)-parseInt(3);
																		}
																		else{
																			var topside = parseInt(topsidekey)+parseInt(20);
																		}
																	}
																	else{
																		var topside = parseInt(topsidekey)+parseInt(25);
																	}
																	var totaltopside = parseInt(totaltop) + parseInt(topside)-parseInt(topsidekey);																						
																	jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
																	jQuery(".imagependantleft img#"+detail).attr("data-left",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-top",totaltopside);
																	jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);
																}
																else if(key==removedImgIndex){
																}
																else{
																	var fromtopside = 10;
																	if(key>=2){
																		var kkey = key-2;
																		var jkey = key-1;	
																	}
																	else if(key==0){
																		var jkey = key;	
																		var kkey = key;
																	}
																	else{
																		var kkey = key-1;
																		var jkey = key-1;	
																	}
																	var jkeyadd = key+1;
																	var leftside = fromleftside * jkeyadd;	
																	var leftsidefrom = fromleftside * key;	
																	var topsidekey = fromtopside * kkey;										
																	var totalleft = $(".imagependantleft img#"+detail).attr('data-leftcurrent');
																	var totaltop = $(".imagependantleft img#"+detail).attr('data-topcurrent');
																	var totalleftside = parseInt(totalleft)+parseInt(leftsidefrom)-parseInt(leftside);
																	if(totalImages=='4'){
																	var topsidekey = fromtopside * jkey;
																		if(jkey=='0'&& kkey=='0'){
																			var topside = parseInt(topsidekey)-parseInt(4);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																		}
																		else if(jkey=='1'&& kkey=='0'){
																			var topside = parseInt(topsidekey)-parseInt(7);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																		}
																		else if(jkey=='2'&& kkey=='1'){
																			var topside = parseInt(topsidekey)-parseInt(12);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																		}
																		else if(jkey=='3'&& kkey=='2'){
																			var topside = parseInt(topsidekey)-parseInt(22);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																		}
																		else{
																			var topside = parseInt(topsidekey);
																		}
																	}
																	else if(totalImages=='2'){
																		var totalleftside = parseInt(totalleft)+parseInt(leftsidefrom)-parseInt(leftside);
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='1'&& kkey=='0'){
																			var topside = parseInt(topsidekey)-parseInt(1);
																		}
																		else{
																			var topside = parseInt(topsidekey)+parseInt(4);
																		}
																	}
																	else{
																		var topside = parseInt(topsidekey)-parseInt(5);
																	}
																	var totaltopside = parseInt(totaltop)+parseInt(topside);
																	jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
																	jQuery(".imagependantleft img#"+detail).attr("data-left",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-top",totaltopside);
																	jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);																						
																}
															}
															else if ($(window).width() >= 411 && $(window).width() <=461){
																var fromleftside = 20;																				
																if(key<removedImgIndex){
																	var fromtopside = 5;																
																	var jkey = key+1;
																	var kkey = key;	
																	var leftside = fromleftside * jkey;
																	var leftfromside = fromleftside * kkey;
																	var topside = fromtopside * jkey;
																	var topsidekey = fromtopside * kkey;
																	var totalleft = $(".imagependantleft img#"+detail).attr('data-leftcurrent');
																	var totaltop = $(".imagependantleft img#"+detail).attr('data-topcurrent');
																	
																	var totalleftside = parseInt(totalleft)+parseInt(leftside)-parseInt(leftfromside);
																	if(totalImages=='4'){
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='1'&& kkey=='0'){
																			var topside = parseInt(topsidekey)+parseInt(20);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																		}
																		else if(jkey=='2'&& kkey=='1'){
																			var topside = parseInt(topsidekey)+parseInt(14);
																		}
																		else if(jkey=='3'&& kkey=='2'){
																			var topside = parseInt(topsidekey)-parseInt(2);
																		}
																		else if(jkey=='4'&& kkey=='3'){
																			var topside = parseInt(topsidekey)-parseInt(13);
																		}
																		else{																	
																			var topside = parseInt(topsidekey);
																			}
																	}
																	else if(totalImages=='2'){
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='1'&& kkey=='0'){
																			var topside = parseInt(topsidekey)+parseInt(10);
																		}
																		else if(jkey=='2'&& kkey=='1'){ 
																			var topside = parseInt(topsidekey)-parseInt(6);
																		}
																		else{
																			var topside = parseInt(topsidekey)+parseInt(20);
																		}
																	}
																	else{
																		var topside = parseInt(topsidekey)+parseInt(25);
																	}
																	var totaltopside = parseInt(totaltop) + parseInt(topside)-parseInt(topsidekey);																						
																	jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
																	jQuery(".imagependantleft img#"+detail).attr("data-left",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-top",totaltopside);
																	jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);
																}
																else if(key==removedImgIndex){
																}
																else{
																	var fromtopside = 5;
																	if(key>=2){
																		var kkey = key-2;
																		var jkey = key-1;	
																	}
																	else if(key==0){
																		var jkey = key;	
																		var kkey = key;
																	}
																	else{
																		var kkey = key-1;
																		var jkey = key-1;	
																	}
																	var jkeyadd = key+1;
																	var leftside = fromleftside * jkeyadd;	
																	var leftsidefrom = fromleftside * key;	
																	var topsidekey = fromtopside * kkey;										
																	var totalleft = $(".imagependantleft img#"+detail).attr('data-leftcurrent');
																	var totaltop = $(".imagependantleft img#"+detail).attr('data-topcurrent');
																	var totalleftside = parseInt(totalleft)+parseInt(leftsidefrom)-parseInt(leftside)+parseInt(10);
																	if(totalImages=='4'){
																	var topsidekey = fromtopside * jkey;
																		if(jkey=='0'&& kkey=='0'){
																			var topside = parseInt(topsidekey)-parseInt(14);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																		}
																		else if(jkey=='1'&& kkey=='0'){
																			var topside = parseInt(topsidekey)-parseInt(10);
																		}
																		else if(jkey=='2'&& kkey=='1'){															
																			var topside = parseInt(topsidekey)-parseInt(7);
																		}
																		else if(jkey=='3'&& kkey=='2'){															
																			var topside = parseInt(topsidekey)-parseInt(7);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																		}
																		else{
																			var topside = parseInt(topsidekey)-parseInt(20);
																		}
																	}
																	else if(totalImages=='2'){
																		var totalleftside = parseInt(totalleft)+parseInt(leftsidefrom)-parseInt(leftside)+parseInt(10);
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='1'&& kkey=='0'){
																			var topside = parseInt(topsidekey)-parseInt(3);
																		}
																		else{
																			var topside = parseInt(topsidekey)-parseInt(6);
																		}
																	}
																	else{
																		var topside = parseInt(topsidekey)-parseInt(5);
																	}
																	var totaltopside = parseInt(totaltop)+parseInt(topside);
																	jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
																	jQuery(".imagependantleft img#"+detail).attr("data-left",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-top",totaltopside);
																	jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);																						
																}
															}
															else if ($(window).width()<411){
																var fromleftside = 20;																				
																if(key<removedImgIndex){
																	var fromtopside = 5;																
																	var jkey = key+1;
																	var kkey = key;	
																	var leftside = fromleftside * jkey;
																	var leftfromside = fromleftside * kkey;
																	var topside = fromtopside * jkey;
																	var topsidekey = fromtopside * kkey;
																	var totalleft = $(".imagependantleft img#"+detail).attr('data-leftcurrent');
																	var totaltop = $(".imagependantleft img#"+detail).attr('data-topcurrent');
																	
																	var totalleftside = parseInt(totalleft)+parseInt(leftside)-parseInt(leftfromside);
																	if(totalImages=='4'){
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='1'&& kkey=='0'){
																			var topside = parseInt(topsidekey)+parseInt(9);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																		}
																		else if(jkey=='2'&& kkey=='1'){
																			var topside = parseInt(topsidekey)+parseInt(6);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																		}
																		else if(jkey=='3'&& kkey=='2'){
																			var topside = parseInt(topsidekey);
																		}
																		else if(jkey=='4'&& kkey=='3'){
																			var topside = parseInt(topsidekey)-parseInt(7);
																		}
																		else{																	
																			var topside = parseInt(topsidekey);
																			}
																	}
																	else if(totalImages=='2'){
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='1'&& kkey=='0'){
																			var topside = parseInt(topsidekey)+parseInt(1);
																		}
																		else if(jkey=='2'&& kkey=='1'){ 
																			var topside = parseInt(topsidekey)-parseInt(4);
																		}
																		else{
																			var topside = parseInt(topsidekey)+parseInt(20);
																		}
																	}
																	else{
																		var topside = parseInt(topsidekey)+parseInt(25);
																	}
																	var totaltopside = parseInt(totaltop) + parseInt(topside)-parseInt(topsidekey);																						
																	jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
																	jQuery(".imagependantleft img#"+detail).attr("data-left",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-top",totaltopside);
																	jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);
																}
																else if(key==removedImgIndex){
																}
																else{
																	var fromtopside = 5;
																	if(key>=2){
																		var kkey = key-2;
																		var jkey = key-1;	
																	}
																	else if(key==0){
																		var jkey = key;	
																		var kkey = key;
																	}
																	else{
																		var kkey = key-1;
																		var jkey = key-1;	
																	}
																	var jkeyadd = key+1;
																	var leftside = fromleftside * jkeyadd;	
																	var leftsidefrom = fromleftside * key;	
																	var topsidekey = fromtopside * kkey;										
																	var totalleft = $(".imagependantleft img#"+detail).attr('data-leftcurrent');
																	var totaltop = $(".imagependantleft img#"+detail).attr('data-topcurrent');
																	var totalleftside = parseInt(totalleft)+parseInt(leftsidefrom)-parseInt(leftside)+parseInt(10);
																	if(totalImages=='4'){
																		var totalleftside = parseInt(totalleft)+parseInt(leftsidefrom)-parseInt(leftside)+parseInt(10);
																	var topsidekey = fromtopside * jkey;
																		if(jkey=='0'&& kkey=='0'){
																			var topside = parseInt(topsidekey)-parseInt(4);
																		}
																		else if(jkey=='1'&& kkey=='0'){
																			var topside = parseInt(topsidekey)-parseInt(5);
																		}
																		else if(jkey=='2'&& kkey=='1'){															
																			var topside = parseInt(topsidekey)-parseInt(7);
																		}
																		else if(jkey=='3'&& kkey=='2'){															
																			var topside = parseInt(topsidekey)-parseInt(12);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																		}
																		else{
																			var topside = parseInt(topsidekey)-parseInt(20);
																		}
																	}
																	else if(totalImages=='2'){
																		var totalleftside = parseInt(totalleft)+parseInt(leftsidefrom)-parseInt(leftside)+parseInt(10);
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='1'&& kkey=='0'){
																			var topside = parseInt(topsidekey)+parseInt(1);
																		}
																		else{
																			var topside = parseInt(topsidekey)-parseInt(4);
																		}
																	}
																	else{
																		var topside = parseInt(topsidekey)-parseInt(5);
																	}
																	var totaltopside = parseInt(totaltop)+parseInt(topside);
																	jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
																	jQuery(".imagependantleft img#"+detail).attr("data-left",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-top",totaltopside);
																	jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);																						
																}
															}
															else{
																var fromleftside = 20;																				
																if(key<removedImgIndex){
																	var fromtopside = 5;																
																	var jkey = key+1;
																	var kkey = key;	
																	var leftside = fromleftside * jkey;
																	var leftfromside = fromleftside * kkey;
																	var topside = fromtopside * jkey;
																	var topsidekey = fromtopside * kkey;
																	var totalleft = $(".imagependantleft img#"+detail).attr('data-leftcurrent');
																	var totaltop = $(".imagependantleft img#"+detail).attr('data-topcurrent');
																	
																	var totalleftside = parseInt(totalleft)+parseInt(leftside)-parseInt(leftfromside);
																	if(totalImages=='4'){
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='1'&& kkey=='0'){
																			var topside = parseInt(topsidekey)+parseInt(20);
																			jQuery(".imagependantleft img#"+detail).css({"transform": "rotate(0deg)"});
																		}
																		else if(jkey=='2'&& kkey=='1'){
																			var topside = parseInt(topsidekey)+parseInt(20);
																		}
																		else if(jkey=='3'&& kkey=='2'){
																			var topside = parseInt(topsidekey)+parseInt(12);
																		}
																		else if(jkey=='4'&& kkey=='3'){
																			var topside = parseInt(topsidekey)-parseInt(8);
																		}
																		else{																	
																			var topside = parseInt(topsidekey);
																			}
																	}
																	else if(totalImages=='2'){
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='1'&& kkey=='0'){
																			var topside = parseInt(topsidekey)+parseInt(10);
																		}
																		else if(jkey=='2'&& kkey=='1'){ 
																			var topside = parseInt(topsidekey);
																		}
																		else{
																			var topside = parseInt(topsidekey)+parseInt(20);
																		}
																	}
																	else{
																		var topside = parseInt(topsidekey)+parseInt(25);
																	}
																	var totaltopside = parseInt(totaltop) + parseInt(topside)-parseInt(topsidekey);																						
																	jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
																	jQuery(".imagependantleft img#"+detail).attr("data-left",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-top",totaltopside);
																	jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);
																}
																else if(key==removedImgIndex){
																}
																else{
																	var fromtopside = 5;
																	if(key>=2){
																		var kkey = key-2;
																		var jkey = key-1;	
																	}
																	else if(key==0){
																		var jkey = key;	
																		var kkey = key;
																	}
																	else{
																		var kkey = key-1;
																		var jkey = key-1;	
																	}
																	var jkeyadd = key+1;
																	var leftside = fromleftside * jkeyadd;	
																	var leftsidefrom = fromleftside * key;	
																	var topsidekey = fromtopside * kkey;										
																	var totalleft = $(".imagependantleft img#"+detail).attr('data-leftcurrent');
																	var totaltop = $(".imagependantleft img#"+detail).attr('data-topcurrent');
																	var totalleftside = parseInt(totalleft)+parseInt(leftsidefrom)-parseInt(leftside)+parseInt(10);
																	if(totalImages=='4'){
																	var topsidekey = fromtopside * jkey;
																		if(jkey=='0'&& kkey=='0'){
																			var topside = parseInt(topsidekey)-parseInt(26);
																		}
																		else if(jkey=='1'&& kkey=='0'){
																			var topside = parseInt(topsidekey)-parseInt(3);
																		}
																		else if(jkey=='2'&& kkey=='1'){															
																			var topside = parseInt(topsidekey)+parseInt(2);
																		}
																		else if(jkey=='3'&& kkey=='2'){															
																			var topside = parseInt(topsidekey)+parseInt(10);
																		}
																		else{
																			var topside = parseInt(topsidekey)-parseInt(20);
																		}
																	}
																	else if(totalImages=='2'){
																		var totalleftside = parseInt(totalleft)+parseInt(leftsidefrom)-parseInt(leftside)+parseInt(10);
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='1'&& kkey=='0'){
																			var topside = parseInt(topsidekey)+parseInt(5);
																		}
																		else{
																			var topside = parseInt(topsidekey);
																		}
																	}
																	else{
																		var topside = parseInt(topsidekey)-parseInt(5);
																	}
																	var totaltopside = parseInt(totaltop)+parseInt(topside);
																	jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
																	jQuery(".imagependantleft img#"+detail).attr("data-left",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-top",totaltopside);
																	jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);																						
																}
															}
																				console.log('even');
														}	
														else{
															if ($(window).width() > 1026){
																console.log('odd');
																var fromleftside = 20;	
																if(key<removedImgIndex){
																	var fromtopside = 30;																
																	var jkey = key+1;
																	var kkey = key;	
																	var leftside = fromleftside * jkey;
																	var topside = fromtopside * jkey;
																	var topsidekey = fromtopside * kkey;
																	
																	var totalleft = $(".imagependantleft img#"+detail).attr('data-leftcurrent');
																	var totalleftside =parseInt(totalleft)+parseInt(fromleftside);
																	var totaltop = $(".imagependantleft img#"+detail).attr('data-topcurrent');
																	if(totalImages=='3'){
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='1' && kkey=='0'){  
																			var topside = parseInt(topsidekey)+parseInt(8);
																			console.log('jkey2'+jkey+'kkey'+kkey);
																		}
																		else if(jkey=='2' && kkey=='1'){
																			var topside = parseInt(topsidekey)+parseInt(5);
																			console.log('jkey2'+jkey+'kkey'+kkey);
																		}
																		else{																	
																			var topside = parseInt(topsidekey)-parseInt(3);
																				console.log('nojkey'+'kkey'+kkey);
																		}
																	}
																	else{
																		var topside = parseInt(topsidekey)+parseInt(5)
																	}
																	var totaltopside = parseInt(totaltop) + parseInt(topside)-parseInt(topsidekey);																						
																	jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
																	jQuery(".imagependantleft img#"+detail).attr("data-left",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-top",totaltopside);
																	jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);																						
																}
																else if(key==removedImgIndex){
																}
																else{													
																	var fromtopside = 20;										
																	if(key>=2){
																		var kkey = key-2;
																		var jkey = key-1;	
																	}
																	else if(key==0){
																		var jkey = key;	
																		var kkey = key;
																	}
																	else{
																		var kkey = key-1;
																		var jkey = key-1;	
																	}
																	var leftside = fromleftside * jkey;	
																	var topsidekey = fromtopside * kkey;
																	var totalleft = $(".imagependantleft img#"+detail).attr('data-leftcurrent');
																	var totalleftside =parseInt(totalleft)-parseInt(fromleftside);	
																	var totaltop = $(".imagependantleft img#"+detail).attr('data-topcurrent');		
																	if(totalImages=='3'){
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='2' && kkey=='1'){
																			var topside = parseInt(topsidekey)+parseInt(7);
																		}
																		else if(jkey=='3' && kkey=='2'){
																			var topside = parseInt(topsidekey)+parseInt(18);
																		}
																		else if(jkey=='0' && kkey=='0'){
																			var topside = parseInt(topsidekey)-parseInt(5);
																		}
																		else{																	
																			var topside = parseInt(topsidekey);
																		}
																	}
																	else{
																		var topside = parseInt(topsidekey);
																	}
																	var totaltopside = parseInt(totaltop)+parseInt(topside)-parseInt(topsidekey);
																	
																	jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
																	jQuery(".imagependantleft img#"+detail).attr("data-left",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-top",totaltopside);
																	jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);
																}
															}
															else if ($(window).width() > 980 && $(window).width() <=1025){
																var fromleftside = 20;	
																if(key<removedImgIndex){
																	var fromtopside = 10;																
																	var jkey = key+1;
																	var kkey = key;	
																	var leftside = fromleftside * jkey;
																	var topside = fromtopside * jkey;
																	var topsidekey = fromtopside * kkey;
																	
																	var totalleft = $(".imagependantleft img#"+detail).attr('data-leftcurrent');
																	var totalleftside =parseInt(totalleft)+parseInt(fromleftside);
																	var totaltop = $(".imagependantleft img#"+detail).attr('data-topcurrent');
																	if(totalImages=='3'){
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='1' && kkey=='0'){  
																			var topside = parseInt(topsidekey)+parseInt(5);
																		}
																		else if(jkey=='2' && kkey=='1'){
																			var topside = parseInt(topsidekey)-parseInt(3);
																		}
																		else{																	
																			var topside = parseInt(topsidekey)-parseInt(5);
																		}
																	}		
																	else{
																		var topside = parseInt(topsidekey)+parseInt(10);
																	}
																	var totaltopside = parseInt(totaltop) + parseInt(topside)-parseInt(topsidekey);																						
																	jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
																	jQuery(".imagependantleft img#"+detail).attr("data-left",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-top",totaltopside);
																	jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);																						
																}
																else if(key==removedImgIndex){
																
																}
																else{
																		console.log("removedImgIndexII"+key);													
																		var fromtopside = 10;									
																		if(key>=2){
																			var kkey = key-2;
																			var jkey = key-1;	
																		}
																		else if(key==0){
																			var jkey = key;	
																			var kkey = key;
																		}
																		else{
																			var kkey = key-1;
																			var jkey = key-1;	
																		}
																		var leftside = fromleftside * jkey;	
																		var topsidekey = fromtopside * kkey;
																		var totalleft = $(".imagependantleft img#"+detail).attr('data-leftcurrent');
																		var totalleftside =parseInt(totalleft)-parseInt(fromleftside);
																		var totaltop = $(".imagependantleft img#"+detail).attr('data-topcurrent');		
																		if(totalImages=='3'){
																			var topsidekey = fromtopside * jkey;
																			if(jkey=='2' && kkey=='1'){
																				var topside = parseInt(topsidekey)+parseInt(10);
																			}
																			
																			else if(jkey=='3' && kkey=='2'){
																				var topside = parseInt(topsidekey)+parseInt(10);
																			}
																			else if(jkey=='0' && kkey=='0'){
																				var topside = parseInt(topsidekey)-parseInt(3);
																			}
																			else{																	
																				var topside = parseInt(topsidekey);
																			}
																		}
																		else{
																			var totalleftside =parseInt(totalleft)-parseInt(fromleftside);	
																			var topside = parseInt(topsidekey)+parseInt(5);
																		}
																		var totaltopside = parseInt(totaltop)+parseInt(topside)-parseInt(topsidekey);
																		
																		jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
																		jQuery(".imagependantleft img#"+detail).attr("data-left",totalleftside);
																		jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
																		jQuery(".imagependantleft img#"+detail).attr("data-top",totaltopside);
																		jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);
																	}
															}
															else if ($(window).width() > 767 && $(window).width() <=979){
																var fromleftside = 20;	
																if(key<removedImgIndex){
																	var fromtopside = 10;																
																	var jkey = key+1;
																	var kkey = key;	
																	var leftside = fromleftside * jkey;
																	var topside = fromtopside * jkey;
																	var topsidekey = fromtopside * kkey;
																	
																	var totalleft = $(".imagependantleft img#"+detail).attr('data-leftcurrent');
																	var totalleftside =parseInt(totalleft)+parseInt(fromleftside);
																	var totaltop = $(".imagependantleft img#"+detail).attr('data-topcurrent');
																	if(totalImages=='3'){
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='1' && kkey=='0'){  
																			var topside = parseInt(topsidekey)+parseInt(4);
																		}
																		else if(jkey=='2' && kkey=='1'){
																			var topside = parseInt(topsidekey)-parseInt(3);
																		}
																		else{																	
																			var topside = parseInt(topsidekey)-parseInt(8);
																		}
																	}	
																	else{
																		var topside = parseInt(topsidekey);
																	}
																	var totaltopside = parseInt(totaltop) + parseInt(topside)-parseInt(topsidekey);																						
																	jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
																	jQuery(".imagependantleft img#"+detail).attr("data-left",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-top",totaltopside);
																	jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);																						
																}
																else if(key==removedImgIndex){
																
																}
																else{
																		console.log("removedImgIndexII"+key);													
																		var fromtopside = 10;									
																		if(key>=2){
																			var kkey = key-2;
																			var jkey = key-1;	
																		}
																		else if(key==0){
																			var jkey = key;	
																			var kkey = key;
																		}
																		else{
																			var kkey = key-1;
																			var jkey = key-1;	
																		}
																		var leftside = fromleftside * jkey;	
																		var topsidekey = fromtopside * kkey;
																		var totalleft = $(".imagependantleft img#"+detail).attr('data-leftcurrent');
																		var totalleftside =parseInt(totalleft)-parseInt(fromleftside);
																		var totaltop = $(".imagependantleft img#"+detail).attr('data-topcurrent');		
																		if(totalImages=='3'){
																			var topsidekey = fromtopside * jkey;
																			if(jkey=='2' && kkey=='1'){
																				var topside = parseInt(topsidekey)+parseInt(10);
																			}
																			
																			else if(jkey=='3' && kkey=='2'){
																				var topside = parseInt(topsidekey);
																			}
																			else if(jkey=='0' && kkey=='0'){
																				var topside = parseInt(topsidekey)-parseInt(3);
																			}
																			else{																	
																				var topside = parseInt(topsidekey)+parseInt(4);
																			}
																		}
																		else{
																			var totalleftside =parseInt(totalleft)-parseInt(fromleftside);	
																			var topside = parseInt(topsidekey)+parseInt(7);
																		}
																		var totaltopside = parseInt(totaltop)+parseInt(topside)-parseInt(topsidekey);
																		
																		jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
																		jQuery(".imagependantleft img#"+detail).attr("data-left",totalleftside);
																		jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
																		jQuery(".imagependantleft img#"+detail).attr("data-top",totaltopside);
																		jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);
																	}
															}
															else if ($(window).width() >= 411 && $(window).width() <=461){
																var fromleftside = 20;	
																if(key<removedImgIndex){
																	var fromtopside = 5;																
																	var jkey = key+1;
																	var kkey = key;	
																	var leftside = fromleftside * jkey;
																	var topside = fromtopside * jkey;
																	var topsidekey = fromtopside * kkey;
																	
																	var totalleft = $(".imagependantleft img#"+detail).attr('data-leftcurrent');
																	var totalleftside =parseInt(totalleft)+parseInt(fromleftside);
																	var totaltop = $(".imagependantleft img#"+detail).attr('data-topcurrent');
																	if(totalImages=='3'){
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='1' && kkey=='0'){  
																			var topside = parseInt(topsidekey)+parseInt(19);
																		}
																		else if(jkey=='2' && kkey=='1'){
																			var topside = parseInt(topsidekey)+parseInt(7);
																		}
																		else{																	
																			var topside = parseInt(topsidekey)-parseInt(4);
																		}
																	}		
																	else{
																		var topside = parseInt(topsidekey)+parseInt(5);
																	}
																	var totaltopside = parseInt(totaltop) + parseInt(topside)-parseInt(topsidekey);																						
																	jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
																	jQuery(".imagependantleft img#"+detail).attr("data-left",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-top",totaltopside);
																	jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);																						
																}
																else if(key==removedImgIndex){
																
																}
																else{													
																	var fromtopside = 10;
																																	
																	if(key>=2){
																		var kkey = key-2;
																		var jkey = key-1;	
																	}
																	else if(key==0){
																		var jkey = key;	
																		var kkey = key;
																	}
																	else{
																		var kkey = key-1;
																		var jkey = key-1;	
																	}
																	var leftside = fromleftside * jkey;	
																	var topsidekey = fromtopside * kkey;
																	var totalleft = $(".imagependantleft img#"+detail).attr('data-leftcurrent');
																	var totalleftside =parseInt(totalleft)-parseInt(fromleftside)+parseInt(10);
																	var totaltop = $(".imagependantleft img#"+detail).attr('data-topcurrent');		
																	if(totalImages=='3'){
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='2' && kkey=='1'){
																			var topside = parseInt(topsidekey)+parseInt(12);
																		}
																		else if(jkey=='3' && kkey=='2'){
																			var topside = parseInt(topsidekey)+parseInt(18);
																		}
																		else if(jkey=='0' && kkey=='0'){
																			var topside = parseInt(topsidekey)-parseInt(9);
																		}
																		else{																	
																			var topside = parseInt(topsidekey)+parseInt(4);
																		}
																	}
																	else{
																		var totalleftside =parseInt(totalleft)-parseInt(fromleftside);	
																		var topside = parseInt(topsidekey)+parseInt(5);
																	}
																	var totaltopside = parseInt(totaltop)+parseInt(topside)-parseInt(topsidekey);
																	
																	jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
																	jQuery(".imagependantleft img#"+detail).attr("data-left",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-top",totaltopside);
																	jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);
																}
															}
															else if ($(window).width()<411){
														
																var fromleftside = 20;	
																if(key<removedImgIndex){
																	var fromtopside = 5;																
																	var jkey = key+1;
																	var kkey = key;	
																	var leftside = fromleftside * jkey;
																	var topside = fromtopside * jkey;
																	var topsidekey = fromtopside * kkey;
																	
																	var totalleft = $(".imagependantleft img#"+detail).attr('data-leftcurrent');
																	var totalleftside =parseInt(totalleft)+parseInt(fromleftside);
																	var totaltop = $(".imagependantleft img#"+detail).attr('data-topcurrent');
																	if(totalImages=='3'){
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='1' && kkey=='0'){  
																			var topside = parseInt(topsidekey)+parseInt(9);
																		}
																		else if(jkey=='2' && kkey=='1'){
																			var topside = parseInt(topsidekey)+parseInt(4);
																		}
																		else{																	
																			var topside = parseInt(topsidekey)-parseInt(6);
																		}
																	}		
																	else{
																		var topside = parseInt(topsidekey)+parseInt(1);
																	}
																	var totaltopside = parseInt(totaltop) + parseInt(topside)-parseInt(topsidekey);																						
																	jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
																	jQuery(".imagependantleft img#"+detail).attr("data-left",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-top",totaltopside);
																	jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);																						
																}
																else if(key==removedImgIndex){
																
																}
																else{													
																	var fromtopside = 10;																											
																	if(key>=2){
																		var kkey = key-2;
																		var jkey = key-1;	
																	}
																	else if(key==0){
																		var jkey = key;	
																		var kkey = key;
																	}
																	else{
																		var kkey = key-1;
																		var jkey = key-1;	
																	}
																	var leftside = fromleftside * jkey;	
																	var topsidekey = fromtopside * kkey;
																	var totalleft = $(".imagependantleft img#"+detail).attr('data-leftcurrent');
																	var totalleftside =parseInt(totalleft)-parseInt(fromleftside)+parseInt(10);
																	var totaltop = $(".imagependantleft img#"+detail).attr('data-topcurrent');		
																	if(totalImages=='3'){
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='2' && kkey=='1'){
																			var topside = parseInt(topsidekey)+parseInt(4);
																		}
																		else if(jkey=='3' && kkey=='2'){
																			var topside = parseInt(topsidekey)+parseInt(13);
																		}
																		else if(jkey=='0' && kkey=='0'){
																			var topside = parseInt(topsidekey)-parseInt(1);
																		}
																		else{																	
																			var topside = parseInt(topsidekey)+parseInt(4);
																		}
																	}
																	else{
																		var totalleftside =parseInt(totalleft)-parseInt(fromleftside)+parseInt(10);
																		var topside = parseInt(topsidekey)+parseInt(1);
																	}
																	var totaltopside = parseInt(totaltop)+parseInt(topside)-parseInt(topsidekey);
																	
																	jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
																	jQuery(".imagependantleft img#"+detail).attr("data-left",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-top",totaltopside);
																	jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);
																}	
															}
															else{
																var fromleftside = 20;	
																if(key<removedImgIndex){
																	var fromtopside = 5;																
																	var jkey = key+1;
																	var kkey = key;	
																	var leftside = fromleftside * jkey;
																	var topside = fromtopside * jkey;
																	var topsidekey = fromtopside * kkey;
																	
																	var totalleft = $(".imagependantleft img#"+detail).attr('data-leftcurrent');
																	var totalleftside =parseInt(totalleft)+parseInt(fromleftside);
																	var totaltop = $(".imagependantleft img#"+detail).attr('data-topcurrent');
																	if(totalImages=='3'){
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='1' && kkey=='0'){  
																			var topside = parseInt(topsidekey)+parseInt(35);
																		}
																		else if(jkey=='2' && kkey=='1'){
																			var topside = parseInt(topsidekey)+parseInt(9);
																		}
																		else{																	
																			var topside = parseInt(topsidekey)-parseInt(11);
																		}
																	}			
																	else{
																		var topside = parseInt(topsidekey)+parseInt(5);
																	}
																	var totaltopside = parseInt(totaltop) + parseInt(topside)-parseInt(topsidekey);																						
																	jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
																	jQuery(".imagependantleft img#"+detail).attr("data-left",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-top",totaltopside);
																	jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);																						
																}
																else if(key==removedImgIndex){
																
																}
																else{													
																	var fromtopside = 10;
																																	
																	if(key>=2){
																		var kkey = key-2;
																		var jkey = key-1;	
																	}
																	else if(key==0){
																		var jkey = key;	
																		var kkey = key;
																	}
																	else{
																		var kkey = key-1;
																		var jkey = key-1;	
																	}
																	var leftside = fromleftside * jkey;	
																	var topsidekey = fromtopside * kkey;
																	var totalleft = $(".imagependantleft img#"+detail).attr('data-leftcurrent');
																	var totalleftside =parseInt(totalleft)-parseInt(fromleftside)+parseInt(10);
																	var totaltop = $(".imagependantleft img#"+detail).attr('data-topcurrent');		
																	if(totalImages=='3'){
																		var topsidekey = fromtopside * jkey;
																		if(jkey=='2' && kkey=='1'){
																			var topside = parseInt(topsidekey)+parseInt(9);
																		}
																		else if(jkey=='3' && kkey=='2'){
																			var topside = parseInt(topsidekey)+parseInt(18);
																		}
																		else if(jkey=='0' && kkey=='0'){
																			var topside = parseInt(topsidekey)-parseInt(11);
																		}
																		else{																	
																			var topside = parseInt(topsidekey)-parseInt(1);
																		}
																	}
																	else{
																		var totalleftside =parseInt(totalleft)-parseInt(fromleftside);	
																		var topside = parseInt(topsidekey)-parseInt(5);
																	}
																	var totaltopside = parseInt(totaltop)+parseInt(topside)-parseInt(topsidekey);
																	
																	jQuery(".imagependantleft img#"+detail).css({"left":totalleftside+"px","top":totaltopside+"px"});
																	jQuery(".imagependantleft img#"+detail).attr("data-left",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-leftcurrent",totalleftside);
																	jQuery(".imagependantleft img#"+detail).attr("data-top",totaltopside);
																	jQuery(".imagependantleft img#"+detail).attr("data-topcurrent",totaltopside);
																}
															}
														}		
													});
													$("img#"+imgSrcId).remove();
												}
											}	
										}
									}
									
                                });

                                isNewOption = false;

                            } else {

                                if (this.layout[optionId] == 'above_option') {
                                    parentField.find('.control').prepend(this.opImagesBox); // add images box div
                                    var imageTmplt = this.opImgTplt({
                                        id: 'value_' + valueId,
                                        title: this.title[valueId],
                                        display: 'none',
                                        descptn: this.descptn[valueId],
                                        dataurl: this.fullimage[valueId],
                                        image: this.thumbnail[valueId]
                                    });
                                }

                                if (this.layout[optionId] == 'below_option') {
                                    if (parentField.find('div.control .btnbox').length) {
                                        parentField.find('div.control .btnbox').before(this.opImagesBox);
                                    } else {
                                        parentField.find('div.control').append(this.opImagesBox);
                                    }
                                    var imageTmplt = this.opImgTplt({
                                        id: 'value_' + valueId,
                                        title: this.title[valueId],
                                        display: 'none',
                                        descptn: this.descptn[valueId],
                                        dataurl: this.fullimage[valueId],
                                        image: this.thumbnail[valueId]
                                    });
                                }

                                if (isNewOption) {
                                    parentField.find('.image-tplt').append(imageTmplt);
                                    isNewOption = false;
                                } else {
                                    $('#option_image_value_' + prevVId).parent().parent().after(imageTmplt);
                                }
                                prevVId = valueId;
                                elm.click($.proxy(this.observeCheckbox, this, elm, valueId));
                            }
                        }

                        if (elm[0].checked) {
                            this.observeCheckbox(elm, valueId);
                        }

                        /******/
                        break;
                    }
                    case "select-multiple": {

                        var options = elm[0].options;
                        for (var i = 0, len = options.length; i < len; ++i) {
                            if (options[i].value) {
                                valueId = options[i].value;
                                if (this.thumbnail[valueId]) {
                                    var imageHtml = this.opImgTplt({
                                        id: 'value_' + valueId,
                                        title: this.title[valueId],
                                        display: 'none',
                                        descptn: this.descptn[valueId],
                                        dataurl: this.fullimage[valueId],
                                        image: this.thumbnail[valueId]
                                    });

                                    if (isNewOption) {

                                        if (this.layout[optionId] == 'above_option') {
                                            parentField.find('.control').prepend(this.opImagesBox);
                                        }

                                        if (this.layout[optionId] == 'below_option') {
                                            if (parentField.find('div.control .btnbox').length) {
                                                parentField.find('div.control .btnbox').before(this.opImagesBox);
                                            } else {
                                                parentField.find('div.control').append(this.opImagesBox);
                                            }
                                        }

                                        parentField.addClass('option-image');
                                        parentField.addClass(this.classs[optionId]);

                                        parentField.find('.image-tplt').append(imageHtml);
                                        isNewOption = false;
                                    } else {
                                        $('#option_image_value_' + prevVId).parent().parent().after(imageHtml);
                                    }
                                    prevVId = valueId;
                                }
                            }
                        }
                        elm.change($.proxy(this.observeSelectMultiple, this, elm));
                        this.observeSelectMultiple(elm);

                        /******/
                        break;
                    }
                } // end switch
            }
            ;
        },
        reloadSelect: function (element, optionId) {
            var valueId = element.val();
            if (valueId == '' || !this.thumbnail[valueId]) {
                element.parent().parent().find('.active').removeClass('active');
            } else {
                $('#option_image_value_' + valueId).closest('.image-tplt').children('.active').removeClass('active');
                $('#option_image_value_' + valueId).parent().parent().addClass('active');
            }
        },

        observeRadio: function (element, optionId, valueId) {
            this.updateImage(optionId, valueId);
        },

        observeCheckbox: function (element, valueId) {
            if (element[0].checked) {
                if (this.descptn[valueId]) {
                    $('#descptn_value_' + valueId).show();
                } else {
                    $('#descptn_value_' + valueId).hide();
                }
                $('#option_image_value_' + valueId).show();
                $('#option_image_value_' + valueId).parent().parent().addClass('show-imgbox');
                $('#option_image_value_' + valueId).parent().parent().removeClass('hide-imgbox');
            } else {
                $('#option_image_value_' + valueId).hide();
                $('#option_image_value_' + valueId).parent().parent().addClass('hide-imgbox');
                $('#option_image_value_' + valueId).parent().parent().removeClass('show-imgbox');
            }
        },

        observeSelectOne: function (element, optionId) {
            var valueId = element.val();
            this.updateImage(optionId, valueId);
        },

        observeSelectMultiple: function (element) {
            var vId;
            var options = element[0].options;
            for (var i = 0; i < options.length; ++i) {
                if (options[i].value) {
                    vId = options[i].value;
                    if (options[i].selected) {
                        if (this.descptn[vId]) {
                            $('#descptn_value_' + vId).show();
                        } else {
                            $('#descptn_value_' + vId).hide();
                        }
                        $('#option_image_value_' + vId).show();
                        $('#option_image_value_' + vId).parent().parent().addClass('show-imgbox');
                        $('#option_image_value_' + vId).parent().parent().removeClass('hide-imgbox');
                    } else {
                        $('#option_image_value_' + vId).hide();
                        $('#option_image_value_' + vId).parent().parent().addClass('hide-imgbox');
                        $('#option_image_value_' + vId).parent().parent().removeClass('show-imgbox');
                    }
                }
            }
        },

        updateImage: function (optionId, valueId) {
            var image = $('#option_image_' + optionId);
            if (image.length == 0)
                return;

            if (valueId != '' && this.thumbnail[valueId]) {
                image[0].src = this.preloadedImageThumb[valueId].src;
                image.show();
                image.parent().parent().addClass('show-imgbox');
                image.parent().parent().removeClass('hide-imgbox');

                var full_image = $('#full_image_' + optionId);
                full_image.attr('href', this.fullimage[valueId]);
                full_image.attr('title', this.descptn[valueId]);

                var op_title_ = $('#op_title_' + optionId);
                op_title_.text(this.title[valueId]);

                var op_descptn = $('#descptn_' + optionId);
                if (this.descptn[valueId]) {
                    op_descptn.attr('title', this.descptn[valueId]);
                    op_descptn.show();
                } else {
                    op_descptn.attr('title', '');
                    op_descptn.hide();
                }


            } else {
                image.parent().parent().addClass('hide-imgbox');
                image.parent().parent().removeClass('show-imgbox');
                image.hide();
            }
        },

    });
    return $.mage.optionImages;
});
