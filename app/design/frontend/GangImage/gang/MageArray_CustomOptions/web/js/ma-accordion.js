define([
    'jquery',
    'mage/template',
    'jquery/ui'
], function($, nbImageTplt, modal) {
	
	"use strict";
    $.widget('mage.maAccordion', {
		oldOption: [],
		validElements: [],
		_create: function() {
			this.maButtonBox = nbImageTplt('[data-template=magearray-button-box]');
			this.setAccordion();
			this.onAddToCartButton();
		},
		setAccordion: function() {
			var firstElm, lastElm;
			var optionElements = $('.product-custom-option');
			for (var n = 0; n < optionElements.length; n++) {
				
				var elm = $(optionElements[n]);	
                var optionId = parseInt(elm.attr('name').match(/\d+/)[0]);
			
				if (!this.oldOption[optionId]) 
				{
					this.oldOption[optionId] = {};
					var parentField = elm[0].type == 'radio' || elm[0].type == 'checkbox' ? elm.closest('.options-list').closest('.field') : elm.closest('.field');
					var isNewOption = true;
				}
					
				if(n == 0) // first element
				{ firstElm = true; } else { firstElm = false;}
				
				if(n == parseInt(optionElements.length - 1)) // last element
				{ lastElm = true; } else { lastElm = false;}
				
				if($(elm).is(':radio, :checkbox'))
				{
					if (isNewOption) {
						this.firstFunction(elm, parentField, optionId, firstElm);
						isNewOption = false;
					}
					elm.click($.proxy(this.nextVisibal, this, parentField, '', ''));
				} else {
					if (isNewOption) {
						
						this.firstFunction(elm, parentField, optionId, firstElm);
						
						isNewOption = false;
						elm.change($.proxy(this.nextVisibal, this, parentField, '', ''));
						if(elm[0].type == 'textarea')
						{
							//elm.click($.proxy(this.nextVisibal, this, parentField, '', ''));
						}
					}
				}
				
				if(firstElm) { $('#prev-elm-'+optionId).hide(); }
				if(lastElm) { $('#next-elm-'+optionId).hide(); }
			}
		},
		firstFunction: function(elm, parentField, optionId, firstElm) {
			var maTitle = parentField.children('label');
			maTitle.addClass('ma-title');
			
			var divControl = parentField.children('div.control');
			divControl.addClass('ma-contain');
			divControl.append(this.maButtonBox({opId : optionId}));
			
			if(elm[0].type == 'file') { maTitle.removeAttr('for'); }
			
			if(!firstElm)
			{
				divControl.hide();
			}
			
			var prevEmlBtn = $('#prev-elm-'+optionId);
			prevEmlBtn.click($.proxy(this.goPrevElement, this, parentField));
			
			var nextEmlBtn = $('#next-elm-'+optionId);
			nextEmlBtn.click($.proxy(this.goNextElement, this, elm, parentField));
			
			maTitle.click($.proxy(this.onClickEvent, this, maTitle));
			this.validElements.push(parentField);			
		},
		goPrevElement: function(element) {
			$.each(element.prevAll(), function(index, element)
			{ 
				if($(element).css("display") === "block")
				{ 
					$(element).children('label').trigger("click");
					return false;
				}
			});
		},
		goNextElement: function(elm, parentField) {
			if(elm.closest('form').valid())
			{
				this.nextVisibal(parentField, '', 'display');
			}
		},
		onClickEvent: function(maTitle) {
			var elmt = maTitle.parent().find('div.control');
			if(maTitle.parent().hasClass('charms-category')){
				var elmtp = maTitle.parent().find('div.control p');
				var elmtlength = elmtp.length;
				if(elmtlength>0){
					
				}
				else{
				 jQuery('.charms-category .options-list').prepend('<p>Select one or multiple categories</p>');	
				}
			}
			if(maTitle.parent().hasClass('charms-pendants')){
				jQuery('.charms-pendants').find('.choice').css("display","inline-block"); 
			}
			if(elmt.css("display") !== "block")
			{
				maTitle.parent().parent().find('div.control').slideUp(); // hide all elements
				elmt.slideDown();
				this.canShowMyNextButton(elmt.parent());
			}			
			/*if(maTitle.parent().hasClass('charmsselectvalue')){
				console.log('maTitle');
				maTitle.parent().find('div.control').css("display","none"); 
				jQuery('.charmslist').find('.control').css("display","block"); 
				jQuery('.charmslist').find('div.control').slideDown();
			}*/
			if(maTitle.parent().hasClass('customizedName')){
				var customNameId =	jQuery('.customizedName input').attr('id');
					$('#'+customNameId).keyup(function(){
						var textinput = $('#'+customNameId).val();
						 var keyuplength = this.value.length;
						 if(keyuplength<=9){
						 $("#personalised-nametext").text(textinput);
						 }
					});
			}
			if(maTitle.parent().hasClass('customizedCase')){
				var customFinisedId =	jQuery('.customizedCase select').attr('id');
				$('#'+customFinisedId).change(function(){
					var selectedval = $(this).val();
					var selectText = $("#"+customFinisedId+" option[value='"+selectedval+"']").text();
					
					if(selectText=='Upper Case'){
						$('#personalised-nametext').css('text-transform','capitalize');	
					}
					else{
						$('#personalised-nametext').css('text-transform','lowercase');
					}
					
				});
			}
			if(maTitle.parent().hasClass('customizedFinisedColor')){
				var customFinisedId =	jQuery('.customizedFinisedColor select').attr('id');
				$('#'+customFinisedId).change(function(){
					var selectedval = $(this).val();
					var selectText = $("#"+customFinisedId+" option[value='"+selectedval+"']").text();
					
					if(selectText=='Sterling Silver'){
						$('#personalised-nametext').css('color','#C0C0C0');	
					}
					else{
						$('#personalised-nametext').css('color','#f8c656');
					}
					
				});
			}
		},
		/* nextVisibalDiv: function() {
			var MA = this;
			$.each(MA.validElements, function(index, element)
			{
				var parentElm = element[0].type == 'radio' || element[0].type == 'checkbox' ? element.closest('.options-list').closest('.field') : element.closest('.field');
				if($(parentElm).css("display") === "block")
				{
					MA.nextVisibal(parentElm,'', '');
				}
			});
		}, */
		prevVisibal: function(element) {
			var prevElm = element.prevAll();
			if(prevElm.length === 0)
			{
				return false;
			}
			return true;
		},
		nextVisibal: function(parentElm, nullData, display = '') {
			
			var MA = this;
			var anyElement = false;
			var nextBtnlg = false;
			
			$.each(parentElm.nextAll(), function(index, element)
			{ 
				if($(element).css("display") == "block")
				{ 
					if(display == 'display') // call goNextElement function
					{ // if find any next element. it will show
						anyElement = element;
					} 
					
					nextBtnlg = $(element);
					
					return false;
				}
			});
			
			if(anyElement)
			{ // if get any next element. it will show
				display = false;
				$(anyElement).children('label').trigger("click");
				
					MA.canShowMyNextButton(anyElement);
				
			}
			
			if(nextBtnlg.length)
			{ // if get any next element. it will show
				$(parentElm).find('.btnbox .action-next').show();
			} else {
				$(parentElm).find('.btnbox .action-next').hide();
			}
		},
		canShowMyNextButton: function(parentElm) {
			//parentElm = parentElm.prev();
			
			var anyNextOne = false; 
			$.each($(parentElm).nextAll(), function(index, element)
			{
				if($(element).css("display") === "block")
				{
					anyNextOne = true;
					return false;
				} else {
					anyNextOne = false; 
				}
			});
			
			if(anyNextOne)
			{
				$(parentElm).find('.btnbox .action-next').show();
			} else {
				$(parentElm).find('.btnbox .action-next').hide();
			}
		},
		returnNextVisibal: function(parentElm) {
			var MA = this;
			var noAny = false;
			
			$.each(parentElm.nextAll(), function(index, element)
			{
				if($(element).css("display") === "block")
				{
					noAny = element;
					return false;
				} 
			});
			
			if(noAny)
			{
				parentElm.find('.btnbox').show();
			} else {
				parentElm.find('.btnbox').hide();
			}
		},
		onAddToCartButton: function(maTitle) {
			var MA = this;
			var canAddToCart = true;
			
			$('#product-addtocart-button').click(function(event){
				var checkboxgroup =$('.charmslist').find('.checkbox').attr('group');
				var checkboxgroupradio=$('.charmslist').find('.radio').attr('group');
				$.each(MA.validElements, function(index, element)
				{	
					element.parent().find('div.control').hide();
					element.find('div.control').show();
					if(element.closest('form').valid())
					{
						if(checkboxgroup=='2_charm'){
							var numberOfChecked = $('input:checkbox:checked').length;
							if(numberOfChecked<2){
								alert('Please pick 2 charms.')
								canAddToCart = false;
								return false;
							}
							
							if(parseInt(MA.validElements.length - 1) != index)
							{
								element.find('div.control').hide();
							}
							else{
									element.parent().find('div.control').slideUp();
									canAddToCart = true;
							}
						}
						else if(checkboxgroup=='3_charm'){
							var numberOfChecked = $('input:checkbox:checked').length;
							if(numberOfChecked<3){
								alert('Please pick 3 charms.')
								canAddToCart = false;
								return false;
							}
							
							if(parseInt(MA.validElements.length - 1) != index)
							{
								element.find('div.control').hide();
							}
							else{
									element.parent().find('div.control').slideUp();
									canAddToCart = true;
							}
						}
						else{
							if(checkboxgroupradio=='1_charm'){
								var numberOfCheckedRadio = $('input:radio:checked').length;
								if(numberOfCheckedRadio<1){
									alert('Please pick 1 charm.')
									canAddToCart = false;
									return false;
								}
								
								if(parseInt(MA.validElements.length - 1) != index)
								{
									element.find('div.control').hide();
								}
								else{
									element.parent().find('div.control').slideUp();
									canAddToCart = true;
								}
							}
							else if(checkboxgroupradio=='rose_charm'){
								var numberOfCheckedRadio = $('input:radio:checked').length;
								if(numberOfCheckedRadio<1){
									alert('Please pick initial')
									canAddToCart = false;
									return false;
								}
								
								if(parseInt(MA.validElements.length - 1) != index)
								{
									element.find('div.control').hide();
								}
								else{
									element.parent().find('div.control').slideUp();
									canAddToCart = true;
								}
							}
							else if(checkboxgroupradio=='rosedate_charm'){
								var numberOfCheckedRadio = $('input:radio:checked').length;
								if(numberOfCheckedRadio<1){
									alert('Please pick Date')
									canAddToCart = false;
									return false;
								}
								
								if(parseInt(MA.validElements.length - 1) != index)
								{
									element.find('div.control').hide();
								}
								else{
									element.parent().find('div.control').slideUp();
									canAddToCart = true;
								}
							}
						else{	
							if(parseInt(MA.validElements.length - 1) != index)
							{
								element.find('div.control').hide();
							}
							canAddToCart = true;
						}
					}
					}
					else {
						if(element.css("display") !== "block")
						{
							element.parent().find('div.control').slideUp(); // hide all elements
							console.log("a45da");
						}
						element.find('div.control').slideDown();
						console.log("adaqwe");
						canAddToCart = false;
						return false;
					}
				});
				
				if(!canAddToCart)
				{
					event.preventDefault();
				}
			});
		}
	});
    return $.mage.maAccordion;
});